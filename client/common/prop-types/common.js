import PropTypes from 'prop-types';

export const photoDataShape = PropTypes.shape({
  id: PropTypes.string,
  name: PropTypes.string,
  location: PropTypes.string,
  thumbnailLocation: PropTypes.string,
  mediaType: PropTypes.string,
});

export const customerSummaryShape = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  email: PropTypes.string,
  avatarData: photoDataShape,
});
