import PropTypes from 'prop-types';

/* eslint-disable import/prefer-default-export */
export const oneChangePeriodShape = PropTypes.shape({
  id: PropTypes.string.isRequired,
  startedAt: PropTypes.string.isRequired,
  finishedAt: PropTypes.string.isRequired,
  oneChange: PropTypes.string.isRequired,
});
