import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Snackbar from 'material-ui/Snackbar';
import { Link } from 'react-router';
import metabolicTheme from '../theme';
import SmallPaper from './SmallPaper';
import Logo from './Logo';
import Footer from './Footer';

const styles = {
  logoWrapper: {
    textAlign: 'center',
    marginTop: '20px',
  },
  logo: {
    display: 'inline-block',
  },
};

export default function PublicLayout(props) {
  return (
    <MuiThemeProvider muiTheme={metabolicTheme}>
      <div>
        <main>
          <div style={styles.logoWrapper}>
            <Link to="/" style={styles.logo}><Logo size={50} /></Link>
          </div>
          <SmallPaper>{props.children}</SmallPaper>
        </main>
        <Footer />
        <Snackbar
          open={props.notification.open || false}
          message={props.notification.message || ''}
          autoHideDuration={props.notification.duration || 4000}
          onRequestClose={props.onNotificationClose}
        />
      </div>
    </MuiThemeProvider>
  );
}

PublicLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  notification: PropTypes.shape({
    message: PropTypes.string,
    open: PropTypes.bool,
    duration: PropTypes.number,
  }).isRequired,
  onNotificationClose: PropTypes.func.isRequired,
};
