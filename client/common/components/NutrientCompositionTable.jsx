import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import map from 'lodash/map';
import prettyNumber from '../lib/numbers';
import { textCentered } from '../theme';

const styles = {
  textCentered,
};

const NutrientCompositionTable = ({ nutrients, calories }) => (
  <div>
    <h2 style={styles.textCentered}>Nutrient Composition</h2>
    <Table selectable={false}>
      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
        <TableRow>
          <TableHeaderColumn>Nutrient</TableHeaderColumn>
          <TableHeaderColumn>Amount</TableHeaderColumn>
          <TableHeaderColumn>%</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {map(nutrients, ({ amount, percentage }, key) => (
          <TableRow key={key}>
            <TableRowColumn>{key}</TableRowColumn>
            <TableRowColumn>{prettyNumber(amount)}</TableRowColumn>
            <TableRowColumn>{prettyNumber(percentage)}</TableRowColumn>
          </TableRow>
        ))}
      </TableBody>
    </Table>
    <h3 style={styles.textCentered}>Calories: {prettyNumber(calories)}</h3>
  </div>
);

const nutrient = PropTypes.shape({
  amount: PropTypes.number,
  percentage: PropTypes.number,
});

NutrientCompositionTable.propTypes = {
  nutrients: PropTypes.shape({ proteins: nutrient, fats: nutrient, carbs: nutrient }).isRequired,
  calories: PropTypes.number.isRequired,
};

export default NutrientCompositionTable;
