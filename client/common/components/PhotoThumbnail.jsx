import React, { PureComponent } from 'react';
import injectSheet from 'react-jss';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const styles = {
  imageThumbnail: {
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
};

const THUMBNAIL_SIZE = 40;

class PhotoThumbnail extends PureComponent {
  constructor(props) {
    super(props);

    this.backgroundProperties = this.backgroundProperties.bind(this);
  }

  componentDidMount() {
    this.image = new Image();
    this.image.src = this.props.src;
    this.image.onload = this.props.onLoad;
  }

  componentWillUnmount() {
    this.image.onload = null;
    this.image = null;
  }

  backgroundProperties() {
    return {
      backgroundImage: `url(${this.props.src})`,
      width: `${this.props.size || THUMBNAIL_SIZE}px`,
      height: `${this.props.size || THUMBNAIL_SIZE}px`,
      borderRadius: `${this.props.square ? 'none' : '50%'}`,
    };
  }

  render() {
    return (
      <div
        className={classNames(
          this.props.classes.imageThumbnail,
          this.props.thumbnailDisplayClass,
        )}
        style={this.backgroundProperties()}
      />
    );
  }
}

PhotoThumbnail.propTypes = {
  src: PropTypes.string.isRequired,
  onLoad: PropTypes.func,
  thumbnailDisplayClass: PropTypes.string,
  size: PropTypes.number,
  square: PropTypes.bool,
  classes: PropTypes.shape({
    imageThumbnail: PropTypes.string.isRequired,
  }).isRequired,
};

PhotoThumbnail.defaultProps = {
  onLoad: null,
  size: undefined,
  square: false,
  thumbnailDisplayClass: undefined,
};

export default injectSheet(styles)(PhotoThumbnail);
