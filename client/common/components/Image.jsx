import React, { PureComponent } from 'react';
import injectSheet from 'react-jss';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import omit from 'lodash/omit';
import Avatar from 'material-ui/Avatar';
import Food from '../svg/Food';
import PhotoThumbnail from '../components/PhotoThumbnail';

const styles = {
  hidden: {
    display: 'none !important',
  },
  '@keyframes loading': {
    from: { opacity: 0.33 },
    '50%': { opacity: 1 },
    to: { opacity: 0.33 },
  },
  glowing: {
    animation: 'loading 2s infinite',
  },
};

class ImageContainer extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { loading: true };

    this.setLoaded = this.setLoaded.bind(this);
    this.propsToPass = this.propsToPass.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
  }

  setLoaded() {
    this.setState({ loading: false });
  }

  propsToPass() {
    return omit(this.props, 'sheet', 'classes', 'thumbnail', 'square', 'size', 'loadingIcon');
  }

  renderIcon() {
    const Icon = this.props.loadingIcon || Food;
    const className = classNames(this.props.className, this.props.classes.glowing);
    return (
      <Icon
        {...(this.props.thumbnail ? omit(this.propsToPass(), 'style') : this.propsToPass())}
        className={className}
      />
    );
  }

  render() {
    if (this.props.thumbnail) {
      return (
        <span style={this.props.style}>
          {this.state.loading && (this.props.square
            ? this.renderIcon()
            : <Avatar icon={this.renderIcon()} size={this.props.size} />)
          }
          <PhotoThumbnail
            src={this.props.src}
            size={this.props.size}
            square={this.props.square}
            onLoad={this.setLoaded}
            thumbnailDisplayClass={this.state.loading ? this.props.classes.hidden : undefined}
          />
        </span>
      );
    }

    /* eslint-disable jsx-a11y/img-has-alt */
    return (
      <span className={this.props.className}>
        {this.state.loading && this.renderIcon()}
        <img
          {...this.propsToPass()}
          onLoad={this.setLoaded}
          className={this.state.loading ? this.props.classes.hidden : this.props.className}
        />
      </span>
    );
  }
}

ImageContainer.propTypes = {
  src: PropTypes.string.isRequired,
  thumbnail: PropTypes.bool,
  loadingIcon: PropTypes.func,
  className: PropTypes.string,
  size: PropTypes.number,
  square: PropTypes.bool,
  style: PropTypes.shape({}),
  classes: PropTypes.shape({
    hidden: PropTypes.string.isRequired,
    glowing: PropTypes.string.isRequired,
  }).isRequired,
};

ImageContainer.defaultProps = {
  thumbnail: undefined,
  loadingIcon: undefined,
  size: undefined,
  square: false,
  className: undefined,
  style: undefined,
};

export default injectSheet(styles)(ImageContainer);
