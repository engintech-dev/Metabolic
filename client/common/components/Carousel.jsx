import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import injectSheet from 'react-jss';
import { ViewPager, Frame, Track, View } from 'react-view-pager';
import get from 'lodash/get';
import noop from 'lodash/noop';
import FlatButton from 'material-ui/FlatButton';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import ArrowForward from 'material-ui/svg-icons/navigation/arrow-forward';
import uuid from 'uuid/v4';
import classNames from 'classnames';
import Image from './Image';
import LightBox from './LightBox';
import { photoDataShape } from '../prop-types/common';
import { textCentered } from '../theme';

const styles = {
  frame: {
    width: '100%',
    maxWidth: '500px',
    margin: '0 auto',
  },
  pagerControls: {
    display: 'flex',
    justifyContent: 'center',
  },
  container: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewContainer: {
    marginBottom: '54px',
  },
  image: {
    height: '320px',
    width: '480px',
    objectFit: 'contain',
    cursor: 'pointer',
  },
  indexIndicator: {
    position: 'absolute',
    bottom: '0',
    width: '100%',
    maxWidth: '500px',
  },
  textCentered,
};

class EntryReviewCarousel extends Component {
  constructor(props) {
    super(props);

    this.state = { lightBoxOpen: false, lightBoxStartingIndex: 0, scrolling: false };

    this.onCloseLightBox = this.onCloseLightBox.bind(this);
    this.setTrack = this.setTrack.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.handleRest = this.handleRest.bind(this);
    this.onPrevious = this.onPrevious.bind(this);
    this.onNext = this.onNext.bind(this);
    this.renderIndexIndicator = this.renderIndexIndicator.bind(this);
    this.renderImages = this.renderImages.bind(this);
    this.renderText = this.renderText.bind(this);
  }

  componentDidMount() {
    /**
     * According to https://reactjs.org/blog/2015/12/16/ismounted-antipattern.html,
     * this is not the best solution but for WEB-382 it is because the root problem
     * should be fixed in react-view-pager library.
     */
    const { handleAllImagesReviewedCheck } = this.props;
    handleAllImagesReviewedCheck(false);
    this.active = true;
  }

  componentWillUnmount() {
    this.active = false;
  }

  onPrevious() {
    this.track.prev();
  }

  onNext() {
    const { handleAllImagesReviewedCheck } = this.props;
    this.track.next();
    const currentIndex = get(this.track, 'context.pager.currentIndex', 0);
    if (currentIndex === this.contentLength() - 1) {
      handleAllImagesReviewedCheck(true);
    }
  }

  onOpenLightBox(lightBoxStartingIndex) {
    if (!this.state.scrolling) {
      this.setState({ lightBoxOpen: true, lightBoxStartingIndex });
    }
  }

  onCloseLightBox() {
    this.setState({ lightBoxOpen: false });
  }

  setTrack(track) {
    this.track = track;
  }

  handleScroll() {
    if (this.active && !this.state.scrolling) {
      this.setState({ scrolling: true });
    }
  }

  handleRest() {
    this.setState({ scrolling: false });
  }

  contentLength() {
    return this.props.content.length || this.props.content.size;
  }

  renderIndexIndicator(currentIndex) {
    if (this.contentLength() === 1) {
      return null;
    }

    return (
      <p
        className={classNames(
          this.props.classes.textCentered,
          this.props.classes.indexIndicator,
        )}
      >
        {`${currentIndex + 1}/${this.contentLength()}`}
      </p>
    );
  }

  renderImages() {
    return this.props.content.map((photoData, index) => (
      <View
        key={photoData.id}
        className={classNames(
          this.props.classes.container,
          this.props.classes.viewContainer,
        )}
      >
        <Image
          src={photoData.location}
          alt="Entry Attachment"
          onMouseUp={() => this.onOpenLightBox(index)}
          className={this.props.classes.image}
          color={this.props.loadingPlaceholderColor}
        />
        {this.renderIndexIndicator(index)}
      </View>
    ));
  }

  renderText() {
    return this.props.content.map((singleText, index) => (
      <View
        key={uuid()}
        className={classNames(
          this.props.classes.container,
          this.props.classes.viewContainer,
        )}
      >
        <p className={this.props.classes.textCentered}>{singleText}</p>
        {this.renderIndexIndicator(index)}
      </View>
    ));
  }

  render() {
    const { classes, content } = this.props;
    const image = get(content[0] || content.first(), 'location', false);
    const currentIndex = get(this.track, 'context.pager.currentIndex', 0);

    return (
      <div>
        <ViewPager tag="main">
          <Frame className={classes.frame}>
            <Track
              ref={this.setTrack}
              viewsToShow={1}
              onScroll={this.handleScroll}
              onRest={this.handleRest}
              className={classes.container}
            >
              {image ? this.renderImages() : this.renderText()}
            </Track>
          </Frame>
          {this.contentLength() > 1 &&
            <nav className={classes.pagerControls}>
              <FlatButton
                label="Previous"
                labelPosition="after"
                icon={<ArrowBack />}
                disabled={currentIndex === 0}
                primary
                onClick={this.onPrevious}
              />

              <FlatButton
                label="Next"
                labelPosition="before"
                icon={<ArrowForward />}
                disabled={currentIndex === this.contentLength() - 1}
                primary
                onClick={this.onNext}
              />
            </nav>}
        </ViewPager>
        {image && this.state.lightBoxOpen &&
          <LightBox
            images={content.map(contentImage => contentImage.location)}
            startingIndex={this.state.lightBoxStartingIndex}
            handleClose={this.onCloseLightBox}
          />}
      </div>
    );
  }
}

EntryReviewCarousel.propTypes = {
  handleAllImagesReviewedCheck: PropTypes.func,
  content: PropTypes.oneOfType([
    ImmutablePropTypes.listOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(photoDataShape),
  ]).isRequired,
  loadingPlaceholderColor: PropTypes.string,
  classes: PropTypes.shape({
    frame: PropTypes.string,
    pagerControls: PropTypes.string,
    container: PropTypes.string,
    viewContainer: PropTypes.string,
    image: PropTypes.string,
    indexIndicator: PropTypes.string,
    textCentered: PropTypes.string,
  }).isRequired,
};

EntryReviewCarousel.defaultProps = {
  loadingPlaceholderColor: '#ffffff',
  handleAllImagesReviewedCheck: noop,
};

export default injectSheet(styles)(EntryReviewCarousel);
