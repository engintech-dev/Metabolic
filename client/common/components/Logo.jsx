import React from 'react';
import PropTypes from 'prop-types';

const leftColor = '#1ECCF7';
const rightColor = '#279EF7';

export default function Logo(props) {
  return (
    <div>
      <svg
        width={props.size}
        height={props.size}
        opacity={1}
        viewBox="0 0 22.7 19"
      >
        <g>
          <defs>
            <linearGradient
              id="logograd"
              x1="15%"
              y1="0"
              x2="85%"
              y2="0"
            >
              <stop offset="0" style={{ stopColor: leftColor }} />
              <stop offset="0.5" style={{ stopColor: rightColor }} />
              <stop offset="1" style={{ stopColor: leftColor }} />
            </linearGradient>
          </defs>
          <polygon
            points="
              16.6,5.3
              18.4,11.7
              20.6,11.7
              17.3,0
              6.8,16.9
              2.8,16.9
              4.3,11.7
              2.1,11.7
              0,19
              8,19
            "
            fill="url(#logograd)"
          />
          <polygon
            points="
              18.313461538461538,11.4
              19.9,16.9
              15.8,16.9
              12.6,11.7
              11.3,9.6
              5.3,0
              2.013461538461538,12
              4.213461538461538,12
              6.1,5.3
              10.1,11.7
              11.3,13.7
              14.7,19
              22.7,19
              20.513461538461538,11.4
            "
            fill={leftColor}
          />
        </g>
      </svg>
    </div>
  );
}

Logo.propTypes = {
  size: PropTypes.number,
};

Logo.defaultProps = {
  size: 100,
};
