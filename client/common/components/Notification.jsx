import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import * as colors from 'material-ui/styles/colors';
import keyMirror from 'keymirror';
import classNames from 'classnames';

const TYPES = keyMirror({
  error: null,
  warning: null,
  info: null,
});

const styles = {
  notification: {
    background: colors.grey600,
    color: 'white',
    padding: '20px',
    borderRadius: '2px',
  },
  [TYPES.error]: {
    background: colors.red700,
  },
  [TYPES.warning]: {
    background: colors.orange600,
  },
  [TYPES.info]: {
    background: colors.blue500,
  },
};

function Notification({ type, message, classes }) {
  if (!message) {
    return null;
  }
  const finalType = TYPES[type] ? type : TYPES.error;
  return (
    <div className={classNames(classes.notification, classes[finalType])}>
      {message}
    </div>
  );
}

Notification.propTypes = {
  type: PropTypes.string,
  message: PropTypes.node,
  classes: PropTypes.shape({}).isRequired,
};

Notification.defaultProps = {
  type: 'info',
  message: null,
};

export default injectSheet(styles)(Notification);
