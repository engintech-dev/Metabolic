import each from 'lodash/each';
import { SubmissionError } from 'redux-form';
import ApiError from '../services/api-error';

export default function apiValidationErrorsTranslator(error) {
  if (!(error instanceof ApiError)) {
    return Promise.reject(error);
  }
  const errorResult = {};
  errorResult._error = error; // eslint-disable-line no-underscore-dangle
  each(error.details && error.details.messages, (messages, key) => {
    errorResult[key] = messages && messages[0];
  });
  return Promise.reject(new SubmissionError(errorResult));
}
