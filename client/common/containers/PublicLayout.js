import { connect } from 'react-redux';
import PublicLayout from '../components/PublicLayout';
import { getNotification } from '../store/selectors/ui/notification';
import { hideNotification } from '../store/actions/ui/notification';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps.route.componentProps,
  notification: getNotification(state),
});

const mapDispatchToProps = {
  onNotificationClose: () => hideNotification(),
};

export default connect(mapStateToProps, mapDispatchToProps)(PublicLayout);
