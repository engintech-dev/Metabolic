import ACTION_TYPES from './types';

const { CURRENT_USER } = ACTION_TYPES;

export function setCurrentUser(payload) {
  return { type: CURRENT_USER.SET_CURRENT_USER, payload };
}

export function unsetCurrentUser() {
  return { type: CURRENT_USER.UNSET_CURRENT_USER };
}

export default {
  setCurrentUser,
  unsetCurrentUser,
};
