import types from '../types';

export function setSignInRedirection(location) {
  return { type: types.SET_SIGN_IN_REDIRECTION, location };
}

export function clearSignInRedirection() {
  return { type: types.CLEAR_SIGN_IN_REDIRECTION };
}

export default {
  setSignInRedirection,
  clearSignInRedirection,
};
