import types from '../types';

export function showNotification(payload) {
  return { type: types.SHOW_NOTIFICATION, payload: { ...payload, open: true } };
}

export function hideNotification() {
  return { type: types.HIDE_NOTIFICATION };
}

export default {
  showNotification,
  hideNotification,
};
