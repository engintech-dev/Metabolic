export function getNotification(state) {
  return state.ui.notifications;
}

export default {
  getNotification,
};
