import { schema } from 'normalizr';
import omit from 'lodash/omit';

// eslint-disable-next-line import/prefer-default-export
export const customerSchema = new schema.Entity('customers');

export const customerSummarySchema = new schema.Entity('customerSummaries');

export const agentSummarySchema = new schema.Entity('agentSummaries');

export const entrySchema = new schema.Entity('entries', {
  agentSummary: agentSummarySchema,
  userSummary: customerSummarySchema,
}, {
  processStrategy: (value) => {
    const newValue = omit(value, 'agent', 'user');
    newValue.agentSummary = value.agent;
    newValue.userSummary = value.user;
    return newValue;
  },
});

export const oneChangePeriodSchema = new schema.Entity('oneChangePeriods');
