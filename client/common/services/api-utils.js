import ApiError from './api-error';

export const BASE_PATH = '/api';

export function buildUrl(path) {
  return `${BASE_PATH}/${path}`;
}

export async function jsonFetch(url, options) {
  const finalOptions = options;
  finalOptions.headers = finalOptions.headers || {};

  if (!(finalOptions.body instanceof FormData)) {
    finalOptions.headers['Content-Type'] = finalOptions.headers['Content-Type'] || 'application/json';
  }

  const response = await fetch(url, finalOptions);
  if (response.status === 204) { // no content
    return undefined;
  }
  const json = await response.json();
  if (response.ok) {
    return json;
  }
  throw new ApiError(json);
}

export function authenticatedJsonFetchWrapper(store, getAccessToken) {
  return async function authenticatedJsonFetch(url, options) {
    const finalOptions = options || {};
    finalOptions.headers = finalOptions.headers || {};
    finalOptions.headers.Authorization =
      finalOptions.headers.Authorization || getAccessToken(store.getState());
    return jsonFetch(url, finalOptions);
  };
}

/**
 * This function only has two nesting levels. It does NOT deeply convert JSON to FormData.
 */
export function simpleJSONToFormData(body) {
  const parsedBody = new FormData();

  const levelAppend = (key, value) => {
    if (typeof value === 'string' || typeof value === 'number') {
      parsedBody.append(key, value);
    } else if (Array.isArray(value) && value[0] instanceof File) {
      value.forEach((file) => { parsedBody.append(key, file); });
    } else if (Array.isArray(value)) {
      parsedBody.append(key, JSON.stringify(value));
    }
  };

  Object.keys(body).forEach((key) => {
    if (typeof body[key] === 'string' || typeof body[key] === 'number' || Array.isArray(body[key])) {
      levelAppend(key, body[key]);
    } else {
      Object.keys(body[key]).forEach((innerKey) => {
        levelAppend(`${key}[${innerKey}]`, body[key][innerKey]);
      });
    }
  });

  return parsedBody;
}
