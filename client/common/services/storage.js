import engine from 'store/src/store-engine';
import localStorage from 'store/storages/localStorage';
import cookieStorage from 'store/storages/cookieStorage';

const storages = [
  localStorage,
  cookieStorage,
];

const store = engine.createStore(storages);

export default {
  get(key) {
    return store.get(key);
  },
  set(key, value) {
    return store.set(key, value);
  },
  remove(key) {
    return store.remove(key);
  },
};
