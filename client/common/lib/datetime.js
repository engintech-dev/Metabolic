import moment from 'moment';

export function toISODateFormat(date) {
  return moment(date).format('YYYY-MM-DD');
}

export default {
  toISODateFormat,
};
