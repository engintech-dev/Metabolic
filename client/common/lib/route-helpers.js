const inflection = require('inflection');
const URI = require('urijs');
const UrlPattern = require('url-pattern');
const isEmpty = require('lodash/isEmpty');
const { push, replace } = require('react-router-redux');

function generateHelperName(name) {
  return `get${inflection.camelize(name)}Path`;
}

function generateHelper(pathPattern) {
  const pattern = new UrlPattern(pathPattern);
  return (params, query) => {
    if (!params && query) {
      params = query; // eslint-disable-line no-param-reassign
      query = {}; // eslint-disable-line no-param-reassign
    }
    const computedPath = pattern.stringify(params);
    if (isEmpty(pattern.match(computedPath))) {
      query = params; // eslint-disable-line no-param-reassign
    }
    return isEmpty(query) ? computedPath : URI(computedPath).query(query).toString();
  };
}

function navigationActionGenerator(action, baseUrl, helper) {
  return (...args) => action(`${baseUrl.replace(/\/+$/, '')}${helper(...args)}`);
}

function generateGoToHelperName(name) {
  return `goTo${inflection.camelize(name)}`;
}

function generateGoToHelper(baseUrl, helper) {
  return navigationActionGenerator(push, baseUrl, helper);
}

function generateReplaceToHelperName(name) {
  return `replaceTo${inflection.camelize(name)}`;
}

function generateReplaceToHelper(baseUrl, helper) {
  return navigationActionGenerator(replace, baseUrl, helper);
}

function generate(config) {
  const baseUrl = config.baseUrl || '';
  const routes = config.routes || [];
  return routes.reduce((helpers, route) => {
    const helper = generateHelper(route.path);
    return Object.assign(helpers, {
      [generateHelperName(route.name)]: helper,
      [generateGoToHelperName(route.name)]: generateGoToHelper(baseUrl, helper),
      [generateReplaceToHelperName(route.name)]: generateReplaceToHelper(baseUrl, helper),
    });
  }, {});
}

module.exports = {
  generateHelperName,
  generateHelper,
  generate,
};
