import keyMirror from 'keymirror';

export default function prettyNumber(number) {
  return new Intl.NumberFormat('en-US', { maximumFractionDigits: 2 }).format(number);
}

export const unitsSystems = keyMirror({
  english: null,
  metric: null,
});

export function prettyEnglishHeight(inches) {
  const foot = Math.floor(inches / 12);
  return `${foot}' ${inches - (foot * 12)}''`;
}

function inchesToCm(inches) {
  return inches * 2.54;
}

function poundsToKilograms(pounds) {
  return pounds * 0.453592;
}

export function computeBMI(mass, height, unitsSystem) {
  if (unitsSystem === unitsSystems.english) {
    return (poundsToKilograms(mass) / ((inchesToCm(height) / 100) ** 2)).toFixed(2);
  }

  return (mass / ((height / 100) ** 2)).toFixed(2);
}
