import { INSIGHTS_KEYS } from '../../../common/lib/insights';

const INSIGHTS_HUMAN_NAMES = {
  [INSIGHTS_KEYS.entriesCount]: 'Entries count',
  [INSIGHTS_KEYS.breakfast]: 'Breakfast',
  [INSIGHTS_KEYS.amSnack]: 'Morning snack',
  [INSIGHTS_KEYS.lunch]: 'Lunch',
  [INSIGHTS_KEYS.pmSnack]: 'Afternoon snack',
  [INSIGHTS_KEYS.dinner]: 'Dinner',
  [INSIGHTS_KEYS.caloriesEaten]: 'Under eating',
  [INSIGHTS_KEYS.lateEating]: 'Late eating',
};

export default INSIGHTS_HUMAN_NAMES;
