export default {
  responsiveMediaQueries: {
    mediumLarge: '(min-width: 601px)',
  },
};
