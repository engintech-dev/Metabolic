import { normalize } from 'normalizr';
import configureStore from '../../store';
import { setCurrentQueues, changesFromQueues } from '../../store/actions/data/entries';
import { queuesEvents } from '../events';
import { createListener } from './utils';
import { queueSchema, entrySummarySchema } from '../../normalizr';

const store = configureStore();

export default [
  createListener(
    queuesEvents.current,
    data => store.dispatch(setCurrentQueues(normalize(data.queues, queueSchema))),
  ),
  createListener(
    queuesEvents.changes,
    (data) => {
      if (data.entry) {
        store.dispatch(changesFromQueues(data, normalize(data.entry, entrySummarySchema)));
      } else {
        store.dispatch(changesFromQueues(data));
      }
    },
  ),
];
