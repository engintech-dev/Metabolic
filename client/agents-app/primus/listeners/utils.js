export const createListener = (event, listener) => ({ event, listener });

export const bindListeners = (primus, listeners) => {
  listeners.forEach(listenerData => primus.on(listenerData.event, listenerData.listener));
};
