/* global Primus */
import difference from 'lodash/difference';
import configureStore from '../store';
import {
  lifecycleEvents,
  queuesEvents,
  entriesEvents,
  metabolicChatEvents,
} from './events';
import listeners, { bindListeners } from './listeners';
import { getAccessToken } from '../../common/store/selectors/session';
import {
  getSubscribedToQueues,
  getSubscribedEntries,
  getSubscribedToMetabolicChatList,
  getSubscribedChats,
} from '../store/selectors/data/subscriptions';
import { getAppLoaded } from '../store/selectors/ui/app';
import { getIsSuperAgentModeEnabled, getAgentIdFilter } from '../store/selectors/ui/home';

function buildSubscriptionsState() {
  return {
    currentlySubscribedToChatList: false,
    currentlySubscribedToQueues: false,
    currentSubscribedEntries: [],
    currentSubscribedChats: [],
  };
}

export default function setupPrimusModule() {
  const store = configureStore();

  let primusInstance;
  let currentToken;
  let subscriptionsState = buildSubscriptionsState();

  function subscribeToLifecycleEvents() {
    bindListeners(primusInstance, listeners);
    primusInstance.on(lifecycleEvents.open, () => {
      subscriptionsState = buildSubscriptionsState();
      statusChangeHandler(); // eslint-disable-line no-use-before-define
    });

    primusInstance.on(lifecycleEvents.end, () => {
      /**
       * intentional server side disconnect (probably because of server exiting);
       * primus won't do automatic reconnect, so we need to start the reconnection process
       * (but let's give the server a short break first)
       */
      if (primusInstance.online) {
        setTimeout(() => primusInstance.recovery.reconnect(), 3000);
      }
    });
  }

  function getInstance(accessToken) {
    if (primusInstance && accessToken === currentToken) {
      return primusInstance;
    }

    if (primusInstance) {
      primusInstance.destroy();
    }

    const serverEndpoint = `//${window.location.host}/primus?access_token=${accessToken}`;
    primusInstance = Primus.connect(serverEndpoint, {
      reconnect: {
        max: 20000,
        min: 500,
        retries: 50,
        'reconnect timeout': 10000,
      },
    });

    currentToken = accessToken;
    subscribeToLifecycleEvents();

    return primusInstance;
  }

  function statusChangeHandler() {
    const state = store.getState();
    const loadedApp = getAppLoaded(state);

    if (loadedApp) {
      const accessToken = getAccessToken(state);

      if (!accessToken) {
        return;
      }

      const primus = getInstance(accessToken);

      const subscribedToQueues = getSubscribedToQueues(state);
      if (subscriptionsState.currentlySubscribedToQueues !== subscribedToQueues) {
        if (!subscribedToQueues) {
          primus.emit(queuesEvents.unsubscribe);
        } else {
          const superAgentSubscription = getIsSuperAgentModeEnabled(state);
          const agentId = getAgentIdFilter(state);
          const subscriptionData = superAgentSubscription ?
            { superAgent: true, agentId } : undefined;
          primus.emit(queuesEvents.subscribe, subscriptionData);
        }
        subscriptionsState.currentlySubscribedToQueues = subscribedToQueues;
      }

      const subscribedToChatList = getSubscribedToMetabolicChatList(state);
      if (subscriptionsState.currentlySubscribedToChatList !== subscribedToChatList) {
        primus.emit(subscribedToChatList
          ? metabolicChatEvents.list.subscribe
          : metabolicChatEvents.list.unsubscribe);
        subscriptionsState.currentlySubscribedToChatList = subscribedToChatList;
      }

      const subscribedEntries = getSubscribedEntries(state);
      const currentSubscribedEntries = subscriptionsState.currentSubscribedEntries;
      if (currentSubscribedEntries !== subscribedEntries) {
        const toSubscribe = difference(subscribedEntries, currentSubscribedEntries);
        const toUnsubscribe = difference(currentSubscribedEntries, subscribedEntries);
        toSubscribe.forEach(entryId => primus.emit(entriesEvents.subscribe, { entryId }));
        toUnsubscribe.forEach(entryId => primus.emit(entriesEvents.unsubscribe, { entryId }));
        subscriptionsState.currentSubscribedEntries = subscribedEntries;
      }

      const subscribedChats = getSubscribedChats(state);
      const { currentSubscribedChats } = subscriptionsState;
      if (subscribedChats !== currentSubscribedChats) {
        const toSubscribe = difference(subscribedChats, currentSubscribedChats);
        const toUnsubscribe = difference(currentSubscribedChats, subscribedChats);
        toSubscribe.forEach(id =>
          primus.emit(metabolicChatEvents.users.subscribe, { userId: id }));
        toUnsubscribe.forEach(id =>
          primus.emit(metabolicChatEvents.users.unsubscribe, { userId: id }));
        subscriptionsState.currentSubscribedChats = subscribedChats;
      }
    }
  }

  statusChangeHandler();
  store.subscribe(statusChangeHandler);
}
