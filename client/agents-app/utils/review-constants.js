export const reviewStates = {
  resolve: 'Resolve',
  needMoreInfo: 'Need More Information',
};

export const reviewTypes = {
  search: 'Search Foods',
  manual: 'Manual',
  na: 'N/A',
};

export const reviewActions = {
  evaluate: 'evaluate',
  answer: 'answer',
  requestInfo: 'requestInfo',
};
