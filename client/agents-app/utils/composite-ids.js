import moment from 'moment';

/* eslint-disable import/prefer-default-export */
export const customerDayId = (customerId, timezone, date) =>
  `${customerId}|${timezone}|${moment(date).format('YYYY-MM-DD')}`;
