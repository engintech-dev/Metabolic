import moment from 'moment';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import configureStore from '../store';
import { getAccessToken } from '../../common/store/selectors/session';
import { TYPE_OPTIONS } from '../components/EntryReview/FoodForm/FoodSearchForm';

import {
  buildUrl,
  jsonFetch,
  authenticatedJsonFetchWrapper,
  simpleJSONToFormData,
} from '../../common/services/api-utils';

const store = configureStore();

const authenticatedJsonFetch = authenticatedJsonFetchWrapper(store, getAccessToken);

export default {
  agents: {
    changePassword: (oldPassword, newPassword) =>
      authenticatedJsonFetch(buildUrl('users/change-password'), {
        method: 'POST',
        body: JSON.stringify({ oldPassword, newPassword }),
      }),
    checkAccessToken: accessToken => jsonFetch(buildUrl('agents/me'), {
      headers: {
        Authorization: accessToken,
      },
    }),
    me: () => authenticatedJsonFetch(buildUrl('agents/me')),
    requestPasswordReset: email => jsonFetch(buildUrl('users/reset'), {
      method: 'POST',
      body: JSON.stringify({ email }),
    }),
    resetPassword: (accessToken, newPassword) => jsonFetch(buildUrl('users/reset-password'), {
      method: 'POST',
      body: JSON.stringify({ newPassword }),
      headers: {
        Authorization: accessToken,
      },
    }),
    signIn: credentials => jsonFetch(buildUrl('agents/login'), {
      method: 'POST',
      body: JSON.stringify(credentials),
    }),
    signOut: () => authenticatedJsonFetch(buildUrl('users/logout'), {
      method: 'POST',
    }),
    claimEntry: id => authenticatedJsonFetch(buildUrl(`entries/${id}/claim`), {
      method: 'PUT',
    }),
    review: (entryId, body) => (
      authenticatedJsonFetch(buildUrl(`entries/${entryId}/review`), {
        method: 'PUT',
        body: isEmpty(get(body.message, 'photos')) ? JSON.stringify(body) : simpleJSONToFormData(body),
      })
    ),
    search: (query, type) => {
      let qString = `?q=${query}`;
      if (type !== TYPE_OPTIONS.all) {
        qString += `&type=${type}`;
      }

      return authenticatedJsonFetch(buildUrl(`food${qString}`), { method: 'GET' });
    },
    getEntry: id => authenticatedJsonFetch(buildUrl(`entries/${id}`)),
    getUser: id => authenticatedJsonFetch(buildUrl(`users/${id}`)),
    getCustomerDay: (id, date) => authenticatedJsonFetch(buildUrl(`/users/${id}/day/${moment(date).format('YYYY-MM-DD')}`)),
    getCustomerCurrentBudget: id => authenticatedJsonFetch(buildUrl(`/users/${id}/currentNutritionalBudget`)),
    getFood: (externalSource, externalId) => authenticatedJsonFetch(buildUrl(`food/${externalSource}/${externalId}`)),
    getFoodById: foodId => authenticatedJsonFetch(buildUrl(`food/${foodId}`)),
    updateFoodHumanName: (foodId, humanName) => authenticatedJsonFetch(buildUrl(`food/${foodId}/humanName`), {
      method: 'PUT',
      body: JSON.stringify({ humanName }),
    }),
    list: () => authenticatedJsonFetch(buildUrl('agents')),
    users: (query = '', limit = 20, skip = 0) => authenticatedJsonFetch(buildUrl(`users?filter=${JSON.stringify({ where: { name: { regexp: `/${query}/i` } }, skip, limit })}`)),
    customers: agentId => authenticatedJsonFetch(buildUrl(`agents/${agentId}/users`)),
    updateUserNotes: (userId, notes) => authenticatedJsonFetch(buildUrl(`users/${userId}/notes`), {
      method: 'PUT',
      body: JSON.stringify({ notes }),
    }),
    submitGoals: (customerId, internalGoals = [], goalsPresets = {}, otherGoal = {}) => (
      authenticatedJsonFetch(buildUrl(`users/${customerId}/agentGoals`), {
        method: 'PUT',
        body: JSON.stringify({ internalGoals, goalsPresets, otherGoal }),
      })
    ),
    addNutritionTips: (userId, message) => authenticatedJsonFetch(buildUrl(`users/${userId}/nutritionTips`), {
      method: 'POST',
      body: JSON.stringify({ message }),
    }),
    submitCalculatedNutritionalBudget: (id, nutritionalBudget) => authenticatedJsonFetch(buildUrl(`users/${id}/nutritionalBudgets`), {
      method: 'POST',
      body: JSON.stringify(nutritionalBudget),
    }),
    getNutritionTips: (userId, limit = 3) => authenticatedJsonFetch(buildUrl(`users/${userId}/nutritionTips?filter=${JSON.stringify({ limit, order: 'createdAt DESC' })}`)),
    spellCheck: text => authenticatedJsonFetch(buildUrl('spelling/check'), {
      method: 'POST',
      body: JSON.stringify({ text }),
    }),
    getSimilarEntries: id => authenticatedJsonFetch(buildUrl(`entries/${id}/similar`)),
  },
  users: {
    sendMetabolicMessage: (userId, body) => (
      authenticatedJsonFetch(buildUrl(`users/${userId}/metabolicChat/messages`), {
        method: 'POST',
        body: isEmpty(get(body, 'photos')) ? JSON.stringify(body) : simpleJSONToFormData(body),
      })
    ),
  },
};
