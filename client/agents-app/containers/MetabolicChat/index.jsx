import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { goBack } from 'react-router-redux';
import isEmpty from 'lodash/isEmpty';
import {
  addMetabolicChatSubscription,
  removeMetabolicChatSubscription,
} from '../../store/actions/data/subscriptions';
import {
  sendMessageDirectly,
  sendMessageWithSpellCheck,
} from '../../store/actions/data/metabolic-chat';
import { getCustomerFromApi } from '../../store/actions/data/customers';
import { goToCustomer } from '../../store/actions/ui/routes';
import { getCustomer } from '../../store/selectors/data/customers';
import { getIsSubmitting } from '../../store/selectors/ui/metabolic-chat';
import { getIsSpellChecking } from '../../store/selectors/ui/spell-check';
import { getComposerText, getComposerPhotos } from '../../store/selectors/forms/composer';
import MetabolicChat from '../../components/MetabolicChat';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import { origins } from '../../store/reducers/ui/spell-check';
import customPropTypes from '../../prop-types';

function promiseFn(props) {
  return {
    metabolicChatLoading: props.getCustomerFromApi(props.params.customerId),
  };
}

class MetabolicChatContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { spellCheckModalOpen: false };
    this.setSpellCheckModal = this.setSpellCheckModal.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    this.props.addMetabolicChatSubscription(this.props.params.customerId);
  }

  componentWillUnmount() {
    this.props.removeMetabolicChatSubscription(this.props.params.customerId);
  }

  onSubmit(submitDirectly) {
    const { params: { customerId }, composerText, composerPhotos } = this.props;
    const body = { text: composerText, photos: composerPhotos };

    if (submitDirectly) {
      this.props.sendMessageDirectly(customerId, body).then(() => this.setSpellCheckModal(false));
    } else {
      this.props.sendMessageWithSpellCheck(customerId, body).then(this.setSpellCheckModal);
    }
  }

  setSpellCheckModal(spellCheckModalOpen) {
    this.setState({ spellCheckModalOpen });
  }

  render() {
    return (
      <MetabolicChat
        {...this.props}
        spellCheckModalOpen={this.state.spellCheckModalOpen}
        handleCloseSpellCheckModal={() => this.setSpellCheckModal(false)}
        handleSubmit={this.onSubmit}
      />
    );
  }
}

MetabolicChatContainer.propTypes = {
  params: PropTypes.shape({ customerId: PropTypes.string.isRequired }).isRequired,
  composerText: PropTypes.string,
  composerPhotos: customPropTypes.files,
  addMetabolicChatSubscription: PropTypes.func.isRequired,
  removeMetabolicChatSubscription: PropTypes.func.isRequired,
  sendMessageDirectly: PropTypes.func.isRequired,
  sendMessageWithSpellCheck: PropTypes.func.isRequired,
};

MetabolicChatContainer.defaultProps = {
  composerText: undefined,
  composerPhotos: [],
};

function mapStateToProps(state, ownProps) {
  const props = { customerId: ownProps.params.customerId, origin: origins.composer };
  const isLoading = getIsSubmitting(state) || getIsSpellChecking(state, props);
  const composerText = getComposerText(state);
  const composerPhotos = getComposerPhotos(state);
  return {
    customer: getCustomer(state, props),
    composerText,
    composerPhotos,
    isLoading,
    isDisabled: isLoading || (!composerText && isEmpty(composerPhotos)),
  };
}

const mapDispatchToProps = {
  addMetabolicChatSubscription,
  removeMetabolicChatSubscription,
  sendMessageDirectly,
  sendMessageWithSpellCheck,
  getCustomerFromApi,
  goBack,
  goToCustomer,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, MetabolicChatContainer),
);
