import { connect } from 'react-redux';
import { signOut } from '../../store/actions/session';
import { getSignedIn } from '../../../common/store/selectors/session';
import TopNavBar from '../../components/AgentLayout/TopNavBar';
import { goToHome, goToChangePassword } from '../../store/actions/ui/routes';
import { toggleSuperAgentMode } from '../../store/actions/ui/home';
import { getIsSuperAgentModeEnabled } from '../../store/selectors/ui/home';
import { getCurrentUserSuperAgentModeCapability } from '../../store/selectors/current-user';

const mapStateToProps = state => ({
  signedIn: getSignedIn(state),
  superAgentAvailable: getCurrentUserSuperAgentModeCapability(state),
  isSuperAgentModeEnabled: getIsSuperAgentModeEnabled(state),
});
const mapDispatchToProps = { signOut, goToHome, goToChangePassword, toggleSuperAgentMode };

export default connect(mapStateToProps, mapDispatchToProps)(TopNavBar);
