import { connect } from 'react-redux';
import Layout from '../../components/AgentLayout';
import { getNotification } from '../../../common/store/selectors/ui/notification';
import { hideNotification } from '../../../common/store/actions/ui/notification';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps.route.componentProps,
  notification: getNotification(state),
});

const mapDispatchToProps = {
  onNotificationClose: () => hideNotification(),
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
