import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import round from 'lodash/round';
import NutritionalBudget from '../../components/CustomerProfile/NutritionalBudget';

function mapStateToProps(state, ownProps) {
  return {
    initialValues: {
      protein: round(ownProps.nutritionalBudget.proteins, 2),
      fat: round(ownProps.nutritionalBudget.fats, 2),
      carbs: round(ownProps.nutritionalBudget.carbs, 2),
      calories: round(ownProps.nutritionalBudget.calories, 2),
      recommendedWeight: round(ownProps.nutritionalBudget.recommendedWeight, 2),
      caloriesAdjustment: round(ownProps.nutritionalBudget.caloriesAdjustment, 2),
    },
  };
}

export default connect(mapStateToProps)(reduxForm({
  form: 'nutritionalBudget',
  enableReinitialize: true,
})(NutritionalBudget));
