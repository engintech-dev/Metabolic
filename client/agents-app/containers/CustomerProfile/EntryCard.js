import { connect } from 'react-redux';
import { goToReadOnlyEntry } from '../../store/actions/ui/routes';
import EntryCard from '../../components/CustomerProfile/EntryCard';

const mapDispatchToProps = { goToReadOnlyEntry };

export default connect(
  undefined,
  mapDispatchToProps,
)(EntryCard);
