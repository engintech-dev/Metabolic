import { connect } from 'react-redux';
import { getIsSuperAgentModeEnabled } from '../../store/selectors/ui/home';
import NutritionTips from '../../components/CustomerProfile/NutritionTips';
import { getNutritionTipsList, getIsLoadingNutritionTips } from '../../store/selectors/data/nutritionTips';
import { addNewNutritionTip, loadNutritionTips } from '../../store/actions/data/nutritionTips';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';

function promiseFn(props) {
  return {
    nutritionTipsLoaded: props.loadNutritionTips(props.customerId),
  };
}

function mapStateToProps(state) {
  return {
    nutritionTipsList: getNutritionTipsList(state),
    nutritionTipsLoading: getIsLoadingNutritionTips(state),
    superAgentModeEnabled: getIsSuperAgentModeEnabled(state),
  };
}

const mapDispatchToProps = { addNewNutritionTip, loadNutritionTips };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PromiseLoadingWrapper(promiseFn, NutritionTips));
