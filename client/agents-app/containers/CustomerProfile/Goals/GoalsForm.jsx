import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, getFormSyncErrors } from 'redux-form';
import keyBy from 'lodash/keyBy';
import isEmpty from 'lodash/isEmpty';
import { addInternalGoalToForm, handleSubmitGoals } from '../../../store/actions/data/customers';
import GoalsForm from '../../../components/CustomerProfile/Goals/GoalsForm';
import { getOtherUserGoalValues, getUserPresetGoalValues } from '../../../store/selectors/forms/goals';

class GoalsFormContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      staticId: this.props.internalGoalsActiveIds.length,
      internalGoalsActiveIds: this.props.internalGoalsActiveIds,
    };

    this.onInternalGoalAdded = this.onInternalGoalAdded.bind(this);
    this.onInternalGoalRemoved = this.onInternalGoalRemoved.bind(this);
    this.handleSubmitGoals = this.handleSubmitGoals.bind(this);
  }

  onInternalGoalAdded() {
    const staticId = String(this.state.staticId);
    this.props.addInternalGoalToForm(staticId);
    this.setState(oldState => ({
      staticId: oldState.staticId + 1,
      internalGoalsActiveIds: [...oldState.internalGoalsActiveIds, staticId],
    }));
  }

  onInternalGoalRemoved(removedId) {
    this.setState(oldState => ({
      internalGoalsActiveIds: oldState.internalGoalsActiveIds.filter(id => id !== removedId),
    }));
  }

  handleSubmitGoals() {
    this.props.handleSubmitGoals(
      this.props.customerId,
      this.state.internalGoalsActiveIds.map(id => this.props.internalGoals[id]),
      this.props.userPresetGoals,
      (!isEmpty(this.props.userGoal) && this.props.userGoal.otherGoal) || {},
    ).then(() => this.props.handleStopEditing());
  }

  render() {
    return (
      <GoalsForm
        {...this.props}
        internalGoalsActiveIds={this.state.internalGoalsActiveIds}
        handleInternalGoalAdded={this.onInternalGoalAdded}
        handleInternalGoalRemoved={this.onInternalGoalRemoved}
        handleSubmitGoals={this.handleSubmitGoals}
      />
    );
  }
}

GoalsFormContainer.propTypes = {
  customerId: PropTypes.string.isRequired,
  internalGoalsActiveIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  internalGoals: PropTypes.shape({}).isRequired,
  userGoal: PropTypes.shape({
    otherGoal: PropTypes.shape({
      status: PropTypes.string.isRequired,
    }),
  }).isRequired,
  userPresetGoals: PropTypes.shape({}).isRequired,
  addInternalGoalToForm: PropTypes.func.isRequired,
  handleStopEditing: PropTypes.func.isRequired,
  handleSubmitGoals: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  disabled: !!getFormSyncErrors('goals')(state),
  userGoal: getOtherUserGoalValues(state),
  userPresetGoals: getUserPresetGoalValues(state),
  initialValues: {
    internalGoals: ownProps.internalGoals,
    userGoal: { otherGoal: ownProps.customerOtherGoal },
    goals: keyBy(ownProps.customerPresetGoals, 'name'),
  },
});

const mapDispatchToProps = { handleSubmitGoals, addInternalGoalToForm };

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'goals',
})(GoalsFormContainer));
