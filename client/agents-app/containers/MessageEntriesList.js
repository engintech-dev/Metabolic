import { connect } from 'react-redux';
import MessageEntriesList from '../components/MessageEntriesList';
import LoadingWrapper from '../../common/hoc/LoadingWrapper';
import { goToEntryReview, goToReadOnlyEntry } from '../store/actions/ui/routes';
import { loadAgents } from '../store/actions/data/agents';
import { getIsSuperAgentModeEnabled } from '../store/selectors/ui/home';
import { getUserId } from '../../common/store/selectors/session';
import {
  getActiveEntries,
  getWaitingEntries,
  getInactiveEntries,
  getQueuesInitialized,
} from '../store/selectors/data/entries';

const mapStateToProps = state => ({
  loading: !getQueuesInitialized(state),
  activeEntries: getActiveEntries(state),
  waitingEntries: getWaitingEntries(state),
  inactiveEntries: getInactiveEntries(state),
  superAgentMode: getIsSuperAgentModeEnabled(state),
  currentUserId: getUserId(state),
});

const mapDispatchToProps = {
  goToReadOnlyEntry,
  goToEntryReview,
  loadAgents,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoadingWrapper(MessageEntriesList));
