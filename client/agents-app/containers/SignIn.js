import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { push } from 'react-router-redux';
import { routes, redirections, session } from '../store/actions';
import { getSignInRedirection } from '../../common/store/selectors/ui/redirections';
import SignIn from '../components/SignIn';
import apiErrorsTranslator from '../../common/forms/api-errors';

const FormComponent = reduxForm({
  form: 'signIn',
  onSubmit: (values, dispatch, props) => (
    dispatch(session.signIn(values))
      .then(() => {
        if (props.signInRedirection) {
          const { signInRedirection } = props;
          dispatch(redirections.clearSignInRedirection());
          dispatch(push(signInRedirection));
        } else {
          dispatch(routes.goToHome());
        }
      })
      .catch((error) => {
        if (error.code === 'LOGIN_FAILED') {
          // eslint-disable-next-line no-param-reassign
          error.humanMessage = 'Incorrect e-mail or password';
        }
        throw error;
      })
      .catch(apiErrorsTranslator)
  ),
})(SignIn);

function mapStateToProps(state) {
  return {
    signInRedirection: getSignInRedirection(state),
  };
}

export default connect(mapStateToProps)(FormComponent);
