import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSignedIn } from '../../common/store/selectors/session';
import { goToSignIn, goToHome } from '../store/actions/ui/routes';

class RootRedirection extends Component {
  componentWillMount() {
    if (this.props.signedIn) {
      this.props.goToHome();
    } else {
      this.props.goToSignIn();
    }
  }

  render() {
    return null;
  }
}

RootRedirection.propTypes = {
  signedIn: PropTypes.bool.isRequired,
  goToHome: PropTypes.func.isRequired,
  goToSignIn: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  signedIn: getSignedIn(state),
});

const mapDispatchToProps = { goToHome, goToSignIn };

export default connect(mapStateToProps, mapDispatchToProps)(RootRedirection);
