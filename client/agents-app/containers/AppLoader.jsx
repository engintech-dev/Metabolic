import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { assignCurrentUser } from '../store/actions/current-user';
import { markAppLoading, markAppLoaded } from '../store/actions/ui/app';
import { getAppLoaded } from '../store/selectors/ui/app';
import metabolicTheme from '../../common/theme';
import Loading from '../../common/components/Loading';

class AppLoaderContainer extends React.Component {
  componentWillMount() {
    this.props.markAppLoading();
    this.props.assignCurrentUser()
      .then(() => this.props.markAppLoaded());
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={metabolicTheme}>
        {this.props.appLoadedStatus ? this.props.children : <Loading />}
      </MuiThemeProvider>
    );
  }
}

AppLoaderContainer.propTypes = {
  appLoadedStatus: PropTypes.bool.isRequired,
  markAppLoading: PropTypes.func.isRequired,
  markAppLoaded: PropTypes.func.isRequired,
  assignCurrentUser: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

const mapStateToProps = state => ({
  appLoadedStatus: getAppLoaded(state),
});

const mapDispatchToProps = {
  markAppLoading,
  markAppLoaded,
  assignCurrentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppLoaderContainer);
