import { connect } from 'react-redux';
import { claimEntry } from '../store/actions/data/entries';
import ClaimToolbar from '../components/ClaimToolbar';
import { getUserId } from '../../common/store/selectors/session';
import { getIsSuperAgentModeEnabled } from '../store/selectors/ui/home';

const mapStateToProps = state => ({
  userId: getUserId(state),
  superAgentModeEnabled: getIsSuperAgentModeEnabled(state),
});

const mapDispatchToProps = {
  claimEntry,
};

export default connect(mapStateToProps, mapDispatchToProps)(ClaimToolbar);
