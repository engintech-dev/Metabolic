import { reduxForm } from 'redux-form';
import AnalyticsForm from '../../components/EntryReview/AnalyticsForm';

export default reduxForm({
  form: 'entryAnalytics',
})(AnalyticsForm);
