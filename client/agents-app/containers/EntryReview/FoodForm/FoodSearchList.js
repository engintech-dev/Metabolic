import { connect } from 'react-redux';
import { getCurrentFoodSearchResults } from '../../../store/selectors/data/reviews';
import FoodSearchList from '../../../components/EntryReview/FoodForm/FoodSearchList';

const mapStateToProps = (state, ownProps) => ({
  entryId: ownProps.entryId,
  foodSearchResults: getCurrentFoodSearchResults(state),
});

export default connect(mapStateToProps)(FoodSearchList);
