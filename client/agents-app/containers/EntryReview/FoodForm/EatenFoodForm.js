import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { removeFoodFromSearch, resetReview } from '../../../store/actions/data/reviews';
import { getEatenFoodInitialValues, getEatenFoodFormMassInformation } from '../../../store/selectors/data/reviews';
import EatenFoodForm from '../../../components/EntryReview/FoodForm/EatenFoodForm';

const mapStateToProps = (state, ownProps) => ({
  initialValues: getEatenFoodInitialValues(state, ownProps),
  massInformation: getEatenFoodFormMassInformation(state, ownProps),
});

const mapDispatchToProps = {
  onFoodRemove: removeFoodFromSearch,
  onUnmount: resetReview,
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'searchReview',
  enableReinitialize: true,
})(EatenFoodForm));
