import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { getCurrentSearchQuery, getCurrentSearchType } from '../../../store/selectors/forms/search-foods';
import { submitSearch } from '../../../store/actions/data/reviews';
import FoodSearchForm, { TYPE_OPTIONS } from '../../../components/EntryReview/FoodForm/FoodSearchForm';

const mapStateToProps = state => ({
  currentSearchValue: getCurrentSearchQuery(state),
  currentSearchType: getCurrentSearchType(state),
});

export default reduxForm({
  form: 'foodSearch',
  onSubmit: (values, dispatch) => dispatch(submitSearch(values.query, values.type)),
  initialValues: {
    type: TYPE_OPTIONS.standard,
  },
})(connect(mapStateToProps)(FoodSearchForm));
