import { connect } from 'react-redux';
import SimilarEntries from '../../../components/EntryReview/SimilarEntries';
import { loadSimilarEntries } from '../../../store/actions/data/entries';
import { getSimilarEntriesList } from '../../../store/selectors/data/entries';
import PromiseLoadingWrapper from '../../../../common/hoc/PromiseLoadingWrapper';

function promiseFn(props) {
  return {
    similarEntriesLoaded: props.loadSimilarEntries(props.entryId),
  };
}

function mapStateToProps(state, ownProps) {
  return {
    similarEntries: getSimilarEntriesList(state, ownProps),
  };
}

const mapDispatchToProps = { loadSimilarEntries };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PromiseLoadingWrapper(promiseFn, SimilarEntries));
