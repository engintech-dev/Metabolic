import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import { getEntryFromApi } from '../../../store/actions/data/entries';
import { addEntrySubscription, removeEntrySubscription } from '../../../store/actions/data/subscriptions';
import { goToCustomer } from '../../../store/actions/ui/routes';
import { getEntry } from '../../../store/selectors/data/entries';
import { getCustomerSummary } from '../../../store/selectors/data/customers';
import { getReviewType } from '../../../store/selectors/ui/reviews';
import ReadOnlyEntryReview from '../../../components/EntryReview/ReadOnly';
import PromiseLoadingWrapper from '../../../../common/hoc/PromiseLoadingWrapper';
import customPropTypes from '../../../prop-types';

class ReadOnlyEntryReviewContainer extends Component {
  componentWillMount() {
    this.props.addEntrySubscription(this.props.entry.id);
  }

  componentWillUnmount() {
    this.props.removeEntrySubscription(this.props.entry.id);
  }

  render() {
    return <ReadOnlyEntryReview {...this.props} />;
  }
}

ReadOnlyEntryReviewContainer.propTypes = {
  entry: customPropTypes.entry.isRequired,
  addEntrySubscription: PropTypes.func.isRequired,
  removeEntrySubscription: PropTypes.func.isRequired,
};

const promiseFn = props => ({
  entryLoading: props.getEntryFromApi(props.params.id),
});

const mapStateToProps = (state, ownProps) => {
  const entry = getEntry(state, { entryId: ownProps.params.id });

  return {
    entry,
    customer: entry && getCustomerSummary(state, { customerId: entry.userId }),
    currentReviewType: getReviewType(state),
  };
};

const mapDispatchToProps = {
  getEntryFromApi,
  addEntrySubscription,
  removeEntrySubscription,
  goToCustomer,
  goBack,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, ReadOnlyEntryReviewContainer),
);
