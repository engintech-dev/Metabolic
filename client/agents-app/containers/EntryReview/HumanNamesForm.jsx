import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import HumanNamesForm from '../../components/EntryReview/HumanNamesForm';
import getHumanNamesInitialValues from '../../store/selectors/forms/human-names';

function mapStateToProps(state, ownProps) {
  return {
    initialValues: getHumanNamesInitialValues(state, ownProps),
  };
}

export default connect(mapStateToProps)(reduxForm({
  form: 'humanNamesForm',
  enableReinitialize: true,
})(HumanNamesForm));
