import { reduxForm } from 'redux-form';
import { routes, notification } from '../store/actions';
import apiErrorsTranslator from '../../common/forms/api-errors';
import ChangePassword from '../components/ChangePassword';
import apiService from '../services/api';

export default reduxForm({
  form: 'changePassword',
  enableReinitialize: true,
  onSubmit: (values, dispatch) =>
    apiService.agents.changePassword(values.currentPassword, values.password)
      .then(() => dispatch(notification.showNotification({
        message: 'Password changed successfully!',
        duration: 7000,
      })))
      .then(() => dispatch(routes.goToHome()))
      .catch((error) => {
        if (error.code === 'INVALID_PASSWORD') {
          error.humanMessage = 'Your current password is incorrect'; //  eslint-disable-line no-param-reassign
        }
        throw error;
      })
      .catch(apiErrorsTranslator),
})(ChangePassword);
