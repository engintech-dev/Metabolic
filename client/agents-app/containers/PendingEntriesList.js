import { connect } from 'react-redux';
import { claimEntry } from '../store/actions/data/entries';
import { getPendingEntries, getQueuesInitialized } from '../store/selectors/data/entries';
import { getEntryBeingClaimed } from '../store/selectors/ui/entries';
import PendingEntriesList from '../components/PendingEntriesList';
import LoadingWrapper from '../../common/hoc/LoadingWrapper';

const mapStateToProps = state => ({
  loading: !getQueuesInitialized(state),
  pendingEntries: getPendingEntries(state),
  entryBeingClaimed: getEntryBeingClaimed(state),
});

const mapDispatchToProps = { claimEntry };

export default connect(mapStateToProps, mapDispatchToProps)(LoadingWrapper(PendingEntriesList));
