import React from 'react';
import { Provider } from 'react-redux';
import { storeShape } from 'react-redux/lib/utils/PropTypes';
import { Router } from 'react-router';
import { history } from 'react-router/lib/InternalPropTypes';

import routes from './routes';

export default function App(props) {
  return (
    <Provider store={props.store}>
      <Router history={props.history} routes={routes} />
    </Provider>
  );
}

App.propTypes = {
  store: storeShape.isRequired,
  history: history.isRequired,
};
