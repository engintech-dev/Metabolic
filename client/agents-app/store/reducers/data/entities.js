import mapValues from 'lodash/mapValues';
import { Map } from 'immutable';

const INITIAL_STATE = Map({
  agents: new Map(),
  agentSummaries: new Map(),
  entries: new Map(),
  entrySummaries: new Map(),
  customers: new Map(),
  customerSummaries: new Map(),
  messages: new Map(),
  customerDays: new Map(),
  food: new Map(),
  nutritionTips: new Map(),
  foodSearchResults: new Map(),
  metabolicChats: new Map(),
  metabolicMessages: new Map(),
});

export default function entitiesReducer(state = INITIAL_STATE, action) {
  // Actions like @@redux/INIT do not have a payload
  if (!action.payload || !action.payload.entities) {
    return state;
  }

  const { entities } = action.payload;
  return state.merge(
    mapValues(entities, (collection, collectionName) =>
      state.get(collectionName).merge(new Map(collection))));
}
