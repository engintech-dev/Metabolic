import { Map, Set } from 'immutable';
import ACTION_TYPES from '../../actions/types';

const SUBSCRIPTIONS_ACTION_TYPES = ACTION_TYPES.SUBSCRIPTIONS;

const INITIAL_STATE = Map({
  queues: false,
  entries: Set(),
  metabolicChatList: false,
  metabolicChats: Set(),
});

export default function queuesReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SUBSCRIPTIONS_ACTION_TYPES.SUBSCRIBE_TO_QUEUES: {
      return state.set('queues', true);
    }
    case SUBSCRIPTIONS_ACTION_TYPES.UNSUBSCRIBE_FROM_QUEUES: {
      return state.set('queues', false);
    }
    case SUBSCRIPTIONS_ACTION_TYPES.SUBSCRIBE_TO_ENTRY: {
      return state.set('entries', state.get('entries').add(action.payload.entryId));
    }
    case SUBSCRIPTIONS_ACTION_TYPES.UNSUBSCRIBE_FROM_ENTRY: {
      return state.set('entries', state.get('entries').remove(action.payload.entryId));
    }
    case SUBSCRIPTIONS_ACTION_TYPES.SUBSCRIBE_TO_METABOLIC_CHAT_LIST: {
      return state.set('metabolicChatList', true);
    }
    case SUBSCRIPTIONS_ACTION_TYPES.UNSUBSCRIBE_FROM_METABOLIC_CHAT_LIST: {
      return state.set('metabolicChatList', false);
    }
    case SUBSCRIPTIONS_ACTION_TYPES.SUBSCRIBE_TO_METABOLIC_CHAT: {
      return state.set('metabolicChats', state.get('metabolicChats').add(action.payload.chatId));
    }
    case SUBSCRIPTIONS_ACTION_TYPES.UNSUBSCRIBE_FROM_METABOLIC_CHAT: {
      return state.set('metabolicChats', state.get('metabolicChats').remove(action.payload.chatId));
    }
    default:
      return state;
  }
}
