import { Map, List } from 'immutable';
import ACTION_TYPES from '../../actions/types';

const AGENTS_ACTION_TYPES = ACTION_TYPES.AGENTS;

const INITIAL_STATE = Map({
  loading: false,
  list: List(),
});

export default function agentsReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case AGENTS_ACTION_TYPES.LOADING_AGENTS: {
      return state.merge({ loading: true });
    }
    case AGENTS_ACTION_TYPES.ADD_AGENTS: {
      return state.merge({
        list: action.payload.result,
      });
    }
    case AGENTS_ACTION_TYPES.LOADED_AGENTS: {
      return state.merge({ loading: false });
    }
    default:
      return state;
  }
}
