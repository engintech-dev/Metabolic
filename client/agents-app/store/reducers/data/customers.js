import { Map, Set } from 'immutable';
import ACTION_TYPES from '../../actions/types';

const CUSTOMERS_ACTION_TYPES = ACTION_TYPES.CUSTOMERS;

const INITIAL_STATE = Map({
  searchingUsers: false,
  lists: Map({
    engaged: Set(),
    others: Set(),
  }),
  submittingGoals: false,
});

export default function customersReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CUSTOMERS_ACTION_TYPES.ADD_CUSTOMERS: {
      const { result, customerList } = action.payload;
      return state.setIn(['lists', customerList], Set(result));
    }
    case CUSTOMERS_ACTION_TYPES.SEARCHING_USERS: {
      return state.merge({ searchingUsers: true });
    }
    case CUSTOMERS_ACTION_TYPES.SEARCHED_USERS: {
      return state.merge({ searchingUsers: false });
    }
    case CUSTOMERS_ACTION_TYPES.SET_CUSTOMER_DAY_LOADING: {
      return state.set('loading', action.payload.loading);
    }
    case CUSTOMERS_ACTION_TYPES.SET_CURRENT_CUSTOMER_DAY: {
      return state.set('currentCustomerDay', action.payload.result);
    }
    case CUSTOMERS_ACTION_TYPES.SET_SUBMITTING_GOALS: {
      return state.set('submittingGoals', action.payload.submitting);
    }
    case CUSTOMERS_ACTION_TYPES.SET_CUSTOMER_CURRENT_BUDGET: {
      return state.set('currentCustomerBudget', action.payload.result);
    }
    default:
      return state;
  }
}
