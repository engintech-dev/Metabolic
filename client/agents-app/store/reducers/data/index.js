import { combineReducers } from 'redux';
import entities from './entities';
import subscriptions from './subscriptions';
import entries from './entries';
import reviews from './reviews';
import agents from './agents';
import customers from './customers';
import nutritionTips from './nutritionTips';
import metabolicChats from './metabolic-chats';

export default combineReducers({
  entities,
  subscriptions,
  entries,
  reviews,
  agents,
  customers,
  nutritionTips,
  metabolicChats,
});
