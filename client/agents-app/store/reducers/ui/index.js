import { combineReducers } from 'redux';
import notifications from '../../../../common/store/reducers/ui/notification';
import redirections from '../../../../common/store/reducers/ui/redirections';
import home from './home';
import reviews from './reviews';
import spellCheck from './spell-check';
import app from './app';
import entries from './entries';
import metabolicChats from './metabolic-chats';

export default combineReducers({
  notifications,
  redirections,
  home,
  reviews,
  spellCheck,
  app,
  entries,
  metabolicChats,
});
