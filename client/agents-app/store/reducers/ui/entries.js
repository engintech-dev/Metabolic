import { Map } from 'immutable';
import ACTION_TYPES from '../../actions/types';

const ENTRIES_ACTION_TYPES = ACTION_TYPES.ENTRIES;

const INITIAL_STATE = Map({
  entryBeingClaimed: undefined,
});

export default function entriesReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ENTRIES_ACTION_TYPES.SET_ENTRY_BEING_CLAIMED: {
      return state.set('entryBeingClaimed', action.payload.entryId);
    }
    case ENTRIES_ACTION_TYPES.REMOVE_ENTRY_BEING_CLAIMED: {
      return state.set('entryBeingClaimed', undefined);
    }
    default:
      return state;
  }
}
