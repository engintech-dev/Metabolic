import { Map } from 'immutable';
import ACTION_TYPES from '../../actions/types';

const METABOLIC_CHAT_ACTION_TYPES = ACTION_TYPES.METABOLIC_CHATS;

const INITIAL_STATE = Map({
  submitting: false,
});

export default function reviewsReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case METABOLIC_CHAT_ACTION_TYPES.USER.SET_SUBMITTING: {
      return state.set('submitting', action.payload.submitting);
    }
    default:
      return state;
  }
}
