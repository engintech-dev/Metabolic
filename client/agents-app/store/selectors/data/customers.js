import { createSelector } from 'reselect';
import getEntities from './entities';
import { customerLists } from '../../../utils/customer-constants';

export default function customersSelector(state) {
  return state.data.customers;
}

export const getCustomerEntities = createSelector(
  getEntities,
  entities => entities.get('customers'),
);

export const getCustomerSummaryEntities = createSelector(
  getEntities,
  entities => entities.get('customerSummaries'),
);

export const getCustomerDayEntities = createSelector(
  getEntities,
  entities => entities.get('customerDays'),
);

export function getCustomerId(state, props) {
  return props.customerId;
}

export const getCustomer = createSelector(
  getCustomerId,
  getCustomerEntities,
  (customerId, customerEntities) => customerEntities.get(customerId),
);

export const getIsSubmittingGoals = createSelector(
  customersSelector,
  customers => customers.get('submittingGoals'),
);

export const getCustomerSummary = createSelector(
  getCustomerId,
  getCustomerSummaryEntities,
  (customerId, customerSummaryEntities) => customerSummaryEntities.get(customerId),
);

export const getIsLoading = createSelector(
  customersSelector,
  customers => customers.get('loading'),
);

export const getCurrentCustomerDay = createSelector(
  customersSelector,
  getCustomerDayEntities,
  (customers, customerDayEntities) => {
    const currentCustomerDayId = customers.get('currentCustomerDay');
    return currentCustomerDayId ? customerDayEntities.get(currentCustomerDayId) : undefined;
  },
);

export const getCustomersEntities = createSelector(
  getEntities,
  entities => entities.get('customers'),
);

export const getCustomersList = createSelector(
  customersSelector,
  getCustomerSummaryEntities,
  (customers, customerSummaryEntities) =>
    customers.getIn(['lists', customerLists.engaged])
      .map(id => customerSummaryEntities.get(id)),
);

export const getOtherCustomersList = createSelector(
  customersSelector,
  getCustomerSummaryEntities,
  (customers, customerSummaryEntity) =>
    customers.getIn(['lists', customerLists.others])
      .map(id => customerSummaryEntity.get(id)),
);

export const getIsSearchingUsers = createSelector(
  customersSelector,
  customer => customer.get('searchingUsers'),
);

export const getCurrentNutritionalBudget = createSelector(
  customersSelector,
  customer => customer.get('currentCustomerBudget'),
);
