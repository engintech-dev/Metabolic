import { createSelector } from 'reselect';
import getEntities from './entities';

export default function getAgents(state) {
  return state.data.agents;
}

export const getAgentsEntities = createSelector(
  getEntities,
  entities => entities.get('agents'),
);

export const getAgentSummaryEntities = createSelector(
  getEntities,
  entities => entities.get('agentSummaries'),
);

export const getAgentsList = createSelector(
  getAgents,
  getAgentSummaryEntities,
  (agents, agentSummariesEntities) =>
    agents.get('list').map(id => agentSummariesEntities.get(id)),
);

export const getAreAgentsLoaded = createSelector(
  getAgentsList,
  agents => !agents.isEmpty(),
);
