import { createSelector } from 'reselect';
import { List } from 'immutable';
import getEntities from './entities';
import { getCustomerSummaryEntities, getCustomerId } from './customers';

export default function getMetabolicChats(state) {
  return state.data.metabolicChats;
}

export const getMetabolicChatEntities = createSelector(
  getEntities,
  entities => entities.get('metabolicChats'),
);

export const getMetabolicMessageEntities = createSelector(
  getEntities,
  entities => entities.get('metabolicMessages'),
);

export const getMetabolicChatListInitialized = createSelector(
  getMetabolicChats,
  metabolicChats => metabolicChats.get('chatListInitialized'),
);

export const getOrderedChats = createSelector(
  getMetabolicChats,
  metabolicChats => metabolicChats.get('orderedChats'),
);

export const getOrderedMetabolicChatsEntities = createSelector(
  getOrderedChats,
  getMetabolicChatEntities,
  getCustomerSummaryEntities,
  (orderedChats, metabolicChatEntities, customerSummaryEntities) =>
    orderedChats.map((chatId) => {
      const metabolicChat = metabolicChatEntities.get(chatId);
      return {
        ...metabolicChat,
        customerSummary: customerSummaryEntities.get(metabolicChat.customerSummary),
      };
    }),
);

export const getMessagesByCustomer = createSelector(
  getMetabolicChats,
  metabolicChats => metabolicChats.get('messagesByCustomer'),
);

export const getMetabolicMessagesForUser = createSelector(
  getCustomerId,
  getMessagesByCustomer,
  getMetabolicMessageEntities,
  (customerId, messagesByCustomer, metabolicMessageEntities) => {
    const messageIdsForCustomer = messagesByCustomer.get(customerId) || List();
    return messageIdsForCustomer.map(messageId => metabolicMessageEntities.get(messageId));
  },
);
