import { createSelector } from 'reselect';
import { getFormValues } from 'redux-form';

export default function getComposerValues(state) {
  return getFormValues('composer')(state);
}

export const getComposerText = createSelector(
  getComposerValues,
  values => (values && values.text) || '',
);

export const getComposerPhotos = createSelector(
  getComposerValues,
  values => (values && values.photos) || [],
);
