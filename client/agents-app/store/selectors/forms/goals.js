import { getFormValues } from 'redux-form';
import { createSelector } from 'reselect';

export default function getGoalsValues(state) {
  return getFormValues('goals')(state) || {};
}

export const getInternalGoalsValues = createSelector(
  getGoalsValues,
  goalsValues => goalsValues.internalGoals || {},
);

export const getOtherUserGoalValues = createSelector(
  getGoalsValues,
  goalsValues => goalsValues.userGoal || {},
);

export const getUserPresetGoalValues = createSelector(
  getGoalsValues,
  goalsValues => goalsValues.goals || {},
);
