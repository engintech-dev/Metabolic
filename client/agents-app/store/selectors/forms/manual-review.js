import { createSelector } from 'reselect';
import { getFormValues, getFormSyncErrors } from 'redux-form';
import { caloriesPerGram } from '../../../../common/lib/nutrients';

const {
  proteins: proteinCalories,
  fats: fatCalories,
  carbs: carbCalories,
} = caloriesPerGram;

export default function getManualReviewValues(state) {
  return getFormValues('manualReview')(state) || {};
}

export function getHasManualErrors(state) {
  return !!getFormSyncErrors('manualReview')(state);
}

const toInt = value => (
  parseInt(value, 10)
);

export const getManualNutrients = createSelector(
  getManualReviewValues,
  values => ({
    proteins: values.proteins ? toInt(values.proteins) : 0,
    fats: values.fats ? toInt(values.fats) : 0,
    carbs: values.carbs ? toInt(values.carbs) : 0,
  }),
);

export const getManualCalories = createSelector(
  getManualReviewValues,
  values => (values.calories ? toInt(values.calories) : 0),
);

function caloriesFor(nutrientGrams, calories) {
  return nutrientGrams * calories;
}

export const getManualTotalCalories = createSelector(
  getManualNutrients,
  ({ proteins, fats, carbs }) =>
    caloriesFor(proteins, proteinCalories) +
    caloriesFor(fats, fatCalories) +
    caloriesFor(carbs, carbCalories),
);

// Rounded to one decimal point
export const getPercentage = (calories, total) => (
  total === 0 ? 0 : ((Math.round((calories / total) * 100 * 10)) / 10)
);

export const getManualPercentages = createSelector(
  getManualNutrients,
  getManualTotalCalories,
  ({ proteins, fats, carbs }, total) => ({
    proteins: getPercentage(caloriesFor(proteins, proteinCalories), total),
    fats: getPercentage(caloriesFor(fats, fatCalories), total),
    carbs: getPercentage(caloriesFor(carbs, carbCalories), total),
  }),
);

export const getNutritionalData = createSelector(
  getManualNutrients,
  getManualCalories,
  (manualNutrients, calories) => ({ ...manualNutrients, calories }),
);
