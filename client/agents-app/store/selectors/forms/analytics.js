import { createSelector } from 'reselect';
import { getFormValues, getFormSyncErrors } from 'redux-form';
import isEmpty from 'lodash/isEmpty';

export default function getAnalyticsValues(state) {
  return getFormValues('entryAnalytics')(state);
}

export function getHasAnalyticsErrors(state) {
  return !!getFormSyncErrors('entryAnalytics')(state);
}

export const getHasAnalyticsFormErrors = createSelector(
  getAnalyticsValues,
  getHasAnalyticsErrors,
  (values, formErrors) => isEmpty(values) || formErrors,
);
