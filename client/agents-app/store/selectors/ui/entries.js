import { createSelector } from 'reselect';

export default function entriesSelector(state) {
  return state.ui.entries;
}

export const getEntryBeingClaimed = createSelector(
  entriesSelector,
  entries => entries.get('entryBeingClaimed'),
);
