import { reset } from 'redux-form';
import { addFoodFromApi } from '../data/reviews';
import ACTION_TYPES from '../types';
import api from '../../../services/api';
import { notification } from '../../actions';
import { goToHome } from './routes';
import { setSpellChecking, setSpellingErrors } from './spell-check';
import { origins } from '../../reducers/ui/spell-check';

const REVIEWS_ACTION_TYPES = ACTION_TYPES.REVIEWS;

export function setReviewState(state) {
  return {
    type: REVIEWS_ACTION_TYPES.SET_REVIEW_STATE,
    payload: { state },
  };
}

export function setReviewType(type) {
  return {
    type: REVIEWS_ACTION_TYPES.SET_REVIEW_TYPE,
    payload: { type },
  };
}

function reviewSubmission(submitting) {
  return {
    type: REVIEWS_ACTION_TYPES.REVIEW_SUBMISSION,
    payload: { submitting },
  };
}

export function preloadFood(entryId, foodIdsToLoad) {
  return dispatch =>
    Promise.all(foodIdsToLoad.map(foodId => api.agents.getFoodById(foodId)))
      .then(food => dispatch(addFoodFromApi(entryId, food)))
      .catch(() => dispatch(notification.showNotification({
        message: 'There was an error while retrieving this food.',
        duration: 7000,
      })));
}

export function submitReviewDirectly(entryId, data) {
  return (dispatch) => {
    dispatch(reviewSubmission(true));
    return api.agents.review(entryId, data)
      .then(() => {
        dispatch(reset('manualReview'));
        dispatch(reset('searchReview'));
        dispatch(reset('composer'));
        dispatch(reviewSubmission(false));
        dispatch(goToHome());
        dispatch(notification.showNotification({
          message: 'Review submitted successfully.',
          duration: 7000,
        }));
      })
      .catch(() => {
        dispatch(reviewSubmission(false));
        dispatch(notification.showNotification({
          message: 'There was an error while reviewing this entry.',
          duration: 7000,
        }));
      });
  };
}

export function submitReviewWithSpellCheck(text, entryId, data) {
  return (dispatch) => {
    dispatch(setSpellChecking(true, origins.composer));
    return api.agents.spellCheck(text)
      .then((response) => {
        dispatch(setSpellChecking(false, origins.composer));
        dispatch(setSpellingErrors(response.errors, origins.composer));

        if (!response.errors.length) {
          dispatch(submitReviewDirectly(entryId, data));
        }

        return Boolean(response.errors.length);
      })
      .catch(() => {
        dispatch(setSpellChecking(false, origins.composer));
        dispatch(submitReviewDirectly(entryId, data));
        return false;
      });
  };
}

function submitHumanFoodNames(foods) {
  return Promise.all(foods.map(food => api.agents.updateFoodHumanName(food.id, food.humanName)));
}

export function submitReviewWithHumanFoodNamesAndSpellCheck(
  text,
  entryId,
  data,
  foodWithHumanNames,
) {
  return dispatch => (
    submitHumanFoodNames(foodWithHumanNames)
      .then(() => dispatch(submitReviewWithSpellCheck(text, entryId, data)))
  );
}

export function submitReviewWithHumanFoodNames(entryId, data, foodWithHumanNames) {
  return dispatch => (
    submitHumanFoodNames(foodWithHumanNames)
      .then(() => dispatch(submitReviewDirectly(entryId, data)))
  );
}
