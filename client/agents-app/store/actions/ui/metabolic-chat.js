import ACTION_TYPES from '../../actions/types';

const USER_ACTION_TYPES = ACTION_TYPES.METABOLIC_CHATS.USER;

export function setSubmitting(submitting) {
  return {
    type: USER_ACTION_TYPES.SET_SUBMITTING,
    payload: { submitting },
  };
}

export default { setSubmitting };
