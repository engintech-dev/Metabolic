import { push, replace } from 'react-router-redux';
import URI from 'urijs';

function pushToAgentSite(route) {
  return push(`/agents${route}`);
}

function replaceInAgentSite(route) {
  return replace(`/agents${route}`);
}

export function goToRoot() {
  return pushToAgentSite('/dashboard');
}

export function goToHome() {
  return pushToAgentSite('/home');
}

export function goToHomeTab(tab) {
  return pushToAgentSite(URI('/home').query({ tab }));
}

export function goToEntryReview(entryId, claimed) {
  if (claimed) {
    return push({
      pathname: `/agents/entries/${entryId}/review`,
      state: { claimed },
    });
  }

  return pushToAgentSite(`/entries/${entryId}/review`);
}

export function goToReadOnlyEntry(entryId, claimed) {
  if (claimed) {
    return push({
      pathname: `/agents/entries/${entryId}`,
      state: { claimed },
    });
  }

  return pushToAgentSite(`/entries/${entryId}`);
}

export function goToSignIn() {
  return pushToAgentSite('/sign-in');
}

export function goToChangePassword() {
  return pushToAgentSite('/change-password');
}

function getCustomerPath(customerId, customerDay) {
  return `/customers/${customerId}${customerDay ? `?day=${customerDay}` : ''}`;
}

export function replaceCustomerDay(customerId, customerDay) {
  return replaceInAgentSite(getCustomerPath(customerId, customerDay));
}

export function goToCustomer(id) {
  return pushToAgentSite(getCustomerPath(id));
}

export function goToMetabolicChat(id) {
  return pushToAgentSite(`/metabolic-chats/${id}`);
}

export default {
  goToRoot,
  goToHome,
  goToEntryReview,
  goToMetabolicChat,
  goToSignIn,
};
