import ACTION_TYPES from '../types';
import { subscribeToQueues, unsubscribeFromQueues } from '../data/subscriptions';
import storageService from '../../../../common/services/storage';
import { getIsSuperAgentModeEnabled } from '../../selectors/ui/home';

const HOME_ACTION_TYPES = ACTION_TYPES.HOME;

export function changeAgentFilter(agentId) {
  return (dispatch) => {
    dispatch({ type: HOME_ACTION_TYPES.CHANGE_AGENT_FILTER, payload: { agentId } });
    dispatch(unsubscribeFromQueues());
    dispatch(subscribeToQueues());
  };
}

export function toggleSuperAgentMode() {
  return (dispatch, getState) => {
    dispatch({ type: HOME_ACTION_TYPES.TOGGLE_SUPERAGENT_MODE });
    const superAgentModeEnabled = getIsSuperAgentModeEnabled(getState());
    const storedInfo = storageService.get('redux-store-agents');
    storageService.set('redux-store-agents', { ...storedInfo, superAgent: superAgentModeEnabled });
    dispatch(unsubscribeFromQueues());
    dispatch(subscribeToQueues());
  };
}

export default {
  toggleSuperAgentMode,
};
