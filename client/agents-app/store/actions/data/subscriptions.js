import ACTION_TYPES from '../../actions/types';

const SUBSCRIPTIONS_ACTION_TYPES = ACTION_TYPES.SUBSCRIPTIONS;

export function subscribeToQueues() {
  return {
    type: SUBSCRIPTIONS_ACTION_TYPES.SUBSCRIBE_TO_QUEUES,
  };
}

export function unsubscribeFromQueues() {
  return {
    type: SUBSCRIPTIONS_ACTION_TYPES.UNSUBSCRIBE_FROM_QUEUES,
  };
}

export function subscribeToMetabolicChatList() {
  return {
    type: SUBSCRIPTIONS_ACTION_TYPES.SUBSCRIBE_TO_METABOLIC_CHAT_LIST,
  };
}

export function unsubscribeFromMetabolicChatList() {
  return {
    type: SUBSCRIPTIONS_ACTION_TYPES.UNSUBSCRIBE_FROM_METABOLIC_CHAT_LIST,
  };
}

export function addEntrySubscription(entryId) {
  return {
    type: SUBSCRIPTIONS_ACTION_TYPES.SUBSCRIBE_TO_ENTRY,
    payload: {
      entryId,
    },
  };
}

export function removeEntrySubscription(entryId) {
  return {
    type: SUBSCRIPTIONS_ACTION_TYPES.UNSUBSCRIBE_FROM_ENTRY,
    payload: {
      entryId,
    },
  };
}

export function addMetabolicChatSubscription(chatId) {
  return {
    type: SUBSCRIPTIONS_ACTION_TYPES.SUBSCRIBE_TO_METABOLIC_CHAT,
    payload: {
      chatId,
    },
  };
}

export function removeMetabolicChatSubscription(chatId) {
  return {
    type: SUBSCRIPTIONS_ACTION_TYPES.UNSUBSCRIBE_FROM_METABOLIC_CHAT,
    payload: {
      chatId,
    },
  };
}

export default {
  subscribeToQueues,
  unsubscribeFromQueues,
};
