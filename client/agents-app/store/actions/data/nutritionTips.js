import { normalize } from 'normalizr';
import api from '../../../services/api';
import { nutritionTipSchema } from '../../../normalizr';
import { notification } from '../../actions';
import ACTION_TYPES from '../types';

const NUTRITION_TIPS_ACTION_TYPES = ACTION_TYPES.NUTRITION_TIPS;

export function loadNutritionTips(customerId) {
  return (dispatch) => {
    dispatch({ type: NUTRITION_TIPS_ACTION_TYPES.LOADING_NUTRITION_TIPS, payload: true });
    return api.agents.getNutritionTips(customerId)
      .then((nTips) => {
        dispatch({
          type: NUTRITION_TIPS_ACTION_TYPES.ADD_NUTRITION_TIPS,
          payload: { ...normalize(nTips, [nutritionTipSchema]) },
        });
        dispatch({ type: NUTRITION_TIPS_ACTION_TYPES.LOADING_NUTRITION_TIPS, payload: false });
      });
  };
}

export function addNewNutritionTip(customerId, message) {
  return dispatch =>
    api.agents.addNutritionTips(customerId, message)
      .then(() => {
        dispatch(notification.showNotification({
          message: 'Nutrition Tip saved successfully.',
          duration: 7000,
        }));
        dispatch(loadNutritionTips(customerId));
      })
      .catch(() => {
        dispatch(notification.showNotification({
          message: 'There was an error while saving this Nutrition Tip.',
          duration: 7000,
        }));
      });
}
