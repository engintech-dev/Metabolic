import { normalize } from 'normalizr';
import api from '../../../services/api';
import { agentSummarySchema } from '../../../normalizr';
import ACTION_TYPES from '../types';

const AGENTS_ACTION_TYPES = ACTION_TYPES.AGENTS;

function addAgents(normalizedAgents) {
  return {
    type: AGENTS_ACTION_TYPES.ADD_AGENTS,
    payload: normalizedAgents,
  };
}

export function loadAgents() {
  return (dispatch) => {
    dispatch({ type: AGENTS_ACTION_TYPES.LOADING_AGENTS });
    return api.agents.list()
      .then((agents) => {
        dispatch(addAgents(normalize(agents, [agentSummarySchema])));
        dispatch({ type: AGENTS_ACTION_TYPES.LOADED_AGENTS });
      });
  };
}

export default {
  loadAgents,
};
