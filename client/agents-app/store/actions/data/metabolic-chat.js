import { reset } from 'redux-form';
import ACTION_TYPES from '../../actions/types';
import { notification } from '../../actions';
import { setSubmitting } from '../ui/metabolic-chat';
import { setSpellChecking, setSpellingErrors } from '../ui/spell-check';
import { origins } from '../../reducers/ui/spell-check';
import api from '../../../services/api';

const LIST_ACTION_TYPES = ACTION_TYPES.METABOLIC_CHATS.LIST;
const USER_ACTION_TYPES = ACTION_TYPES.METABOLIC_CHATS.USER;

export function setChats(normalizedChats) {
  return {
    type: LIST_ACTION_TYPES.SET_CHATS,
    payload: normalizedChats,
  };
}

export function addChat(normalizedChat) {
  return {
    type: LIST_ACTION_TYPES.ADD_CHAT,
    payload: normalizedChat,
  };
}

export function setMessages(normalizedChatMessages) {
  return {
    type: USER_ACTION_TYPES.SET_MESSAGES,
    payload: normalizedChatMessages,
  };
}

export function addMessage(normalizedChatMessage) {
  return {
    type: USER_ACTION_TYPES.ADD_MESSAGE,
    payload: normalizedChatMessage,
  };
}

export function sendMessageDirectly(customerId, text, photos) {
  return (dispatch) => {
    dispatch(setSubmitting(true));
    return api.users.sendMetabolicMessage(customerId, text, photos)
      .then(() => {
        dispatch(setSubmitting(false));
        dispatch(reset('composer'));
      })
      .catch(() => {
        dispatch(setSubmitting(false));
        dispatch(notification.showNotification({
          message: 'There was an error while sending your message.',
          duration: 7000,
        }));
      });
  };
}

export function sendMessageWithSpellCheck(customerId, message) {
  return (dispatch) => {
    dispatch(setSpellChecking(true, origins.composer));
    return api.agents.spellCheck(message.text)
      .then((response) => {
        dispatch(setSpellChecking(false, origins.composer));
        dispatch(setSpellingErrors(response.errors, origins.composer));

        if (!response.errors.length) {
          dispatch(sendMessageDirectly(customerId, message));
        }

        return Boolean(response.errors.length);
      })
      .catch(() => {
        dispatch(setSpellChecking(false, origins.composer));
        dispatch(sendMessageDirectly(customerId, message));
        return false;
      });
  };
}

export default { setChats, addChat };
