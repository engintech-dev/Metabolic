import React, { Component } from 'react';
import injectSheet from 'react-jss';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { List } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import TextField from 'material-ui/TextField';
import CircularProgress from 'material-ui/CircularProgress';
import CustomerListItem from './CustomerListItem';
import customPropTypes from '../prop-types';
import { textCentered } from '../../common/theme';

const styles = {
  textCentered,
};

class CustomersList extends Component {
  constructor(props) {
    super(props);
    this.state = { textFilter: '' };
    this.searchProcess = null;
    this.renderCustomer = this.renderCustomer.bind(this);
  }

  changeSearchFilter(textFilter) {
    clearTimeout(this.searchProcess);
    this.setState({ textFilter });
    this.searchProcess = setTimeout(
      () => this.props.searchUsers(this.state.textFilter),
      250,
    );
  }

  renderCustomer(customer) {
    return (
      <CustomerListItem
        key={customer.id}
        customer={customer}
        onClick={() => this.props.onClick(customer.id)}
      />
    );
  }

  renderListItems(customers, listName, filter = '') {
    const filterToLowerCase = filter.toLowerCase();
    const matches = text => text.toLowerCase().indexOf(filterToLowerCase) >= 0;
    const customersToRender = filter
      ? customers.filter(({ name, email }) => matches(name) || matches(email))
      : customers;

    return isEmpty(customersToRender)
      ? <h3 className={this.props.classes.textCentered}>No {listName} Users Were Found.</h3>
      : customersToRender.map(this.renderCustomer);
  }

  render() {
    const { engagedList, othersList } = this.props;
    return (
      <div>
        <div className={this.props.classes.textCentered}>
          <TextField
            hintText="Search Customers"
            onChange={(event, newValue) => this.changeSearchFilter(newValue)}
          />
        </div>
        <List>
          <Subheader>Engaged</Subheader>
          {this.renderListItems(engagedList, 'Engaged', this.state.textFilter)}
          <Subheader>Others</Subheader>
          {this.props.searchingUsers ? <CircularProgress /> : this.renderListItems(othersList, 'Other')}
        </List>
      </div>
    );
  }
}

CustomersList.propTypes = {
  engagedList: customPropTypes.customerSummariesSet.isRequired,
  othersList: customPropTypes.customerSummariesSet.isRequired,
  searchUsers: PropTypes.func.isRequired,
  searchingUsers: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(CustomersList);
