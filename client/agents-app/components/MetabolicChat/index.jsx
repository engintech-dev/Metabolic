import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import BackIcon from 'material-ui/svg-icons/navigation/chevron-left';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import AccountIcon from 'material-ui/svg-icons/action/account-circle';
import Chat from '../../containers/Chat';
import { textCentered, centeredButtonWrapper } from '../../../common/theme';
import customPropTypes from '../../prop-types';
import SpellingErrors from '../../containers/SpellingErrors';
import { origins } from '../../store/reducers/ui/spell-check';

const styles = {
  textCentered,
  centeredButtonWrapper,
  headerActions: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingBottom: '10px',
  },
};

function renderSpellCheckActions(handleSubmit, handleCloseSpellCheckModal) {
  return [
    <RaisedButton label="Go Back And Fix" onClick={handleCloseSpellCheckModal} primary />,
    <FlatButton label="Submit Anyway" onClick={() => handleSubmit(true)} primary />,
  ];
}

function MetabolicChat({
  customer,
  isLoading,
  isDisabled,
  spellCheckModalOpen,
  composerText,
  handleSubmit,
  handleCloseSpellCheckModal,
  goToCustomer,
  goBack,
  classes,
}) {
  return (
    <div>
      <div className={classes.headerActions}>
        <FlatButton label="Back" icon={<BackIcon />} onClick={goBack} />
        <Chip onClick={() => goToCustomer(customer.id)}>
          {customer.avatarData
            ? <Avatar src={customer.avatarData.thumbnailLocation || customer.avatarData.location} />
            : <Avatar icon={<AccountIcon />} />}
          {customer.name}
        </Chip>
      </div>

      <h1 className={classes.textCentered}>Chat with {customer.name}</h1>

      <Chat customerId={customer.id} disabled={isLoading} />
      <div className={classes.centeredButtonWrapper}>
        <RaisedButton
          label="Submit"
          labelPosition="before"
          onClick={() => handleSubmit()}
          disabled={isDisabled}
          icon={isLoading ? <CircularProgress size={25} color="white" /> : null}
          fullWidth
          primary
        />
      </div>

      <Dialog
        title="Message Spell Check"
        actions={renderSpellCheckActions(handleSubmit, handleCloseSpellCheckModal)}
        open={spellCheckModalOpen}
        autoScrollBodyContent
        modal
      >
        {composerText && <SpellingErrors origin={origins.composer} originalText={composerText} />}
      </Dialog>
    </div>
  );
}

MetabolicChat.propTypes = {
  customer: customPropTypes.customer.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isDisabled: PropTypes.bool.isRequired,
  spellCheckModalOpen: PropTypes.bool.isRequired,
  composerText: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleCloseSpellCheckModal: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  goToCustomer: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
    centeredButtonWrapper: PropTypes.string.isRequired,
    headerActions: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(MetabolicChat);
