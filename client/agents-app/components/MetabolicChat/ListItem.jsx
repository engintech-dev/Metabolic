import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { ListItem } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import Forward from 'material-ui/svg-icons/navigation/chevron-right';
import Divider from 'material-ui/Divider';
import moment from 'moment';
import customPropTypes from '../../prop-types';
import UserAvatar from '../../../common/components/UserAvatar';

const styles = {
  secondaryText: {
    margin: 0,
    padding: 0,
  },
};

function MetabolicChatListItem({
  metabolicChat: { customerSummary, lastMessageAt },
  onClick,
  classes,
}) {
  const secondaryText = (
    <div>
      <p className={classes.secondaryText}>{customerSummary.email}</p>
      <p className={classes.secondaryText}>{lastMessageAt
        ? `Last message created ${moment(lastMessageAt).fromNow()}`
        : 'There are no messages yet in this chat.'}
      </p>
    </div>
  );

  const leftAvatar = <UserAvatar avatarData={customerSummary.avatarData} />;
  const rightIconButton = <IconButton><Forward /></IconButton>;

  return (
    <div>
      <Divider />
      <ListItem
        primaryText={customerSummary.name}
        secondaryText={secondaryText}
        secondaryTextLines={2}
        leftAvatar={leftAvatar}
        rightIconButton={rightIconButton}
        onClick={onClick}
      />
    </div>
  );
}

MetabolicChatListItem.propTypes = {
  metabolicChat: customPropTypes.metabolicChat.isRequired,
  onClick: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    secondaryText: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(MetabolicChatListItem);
