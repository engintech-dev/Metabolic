import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'material-ui/List';
import ListItem from './ListItem';
import customPropTypes from '../../prop-types';

function renderListItems(metabolicChats, goToMetabolicChat) {
  return metabolicChats.map((metabolicChat) => {
    const id = metabolicChat.customerSummary.id;
    return (
      <ListItem
        key={id}
        metabolicChat={metabolicChat}
        onClick={() => goToMetabolicChat(id)}
      />
    );
  });
}

function MetabolicChatList({ metabolicChats, goToMetabolicChat }) {
  return (
    <List>
      {renderListItems(metabolicChats, goToMetabolicChat)}
    </List>
  );
}

MetabolicChatList.propTypes = {
  metabolicChats: customPropTypes.metabolicChatsList.isRequired,
  goToMetabolicChat: PropTypes.func.isRequired,
};

export default MetabolicChatList;
