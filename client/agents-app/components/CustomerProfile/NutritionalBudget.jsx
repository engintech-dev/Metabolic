import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Field } from 'redux-form';
import { TextField } from 'redux-form-material-ui';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import { textCentered, container, centeredButtonWrapper } from '../../../common/theme';
import BiometricData from './BiometricData';
import customPropTypes from '../../prop-types';

const styles = {
  textCentered,
  fieldWidth: {
    width: '200px',
    marginRight: '20px',
  },
  container,
  submitWrapper: centeredButtonWrapper,
};

function renderNutritionalBudgetActions(
  onCloseNutritionalBudgetModal,
  handleSubmit,
  onSubmitNutritionalBudget,
  showCalculateNutritionalBudgetForm,
) {
  const actions = [];
  if (showCalculateNutritionalBudgetForm) {
    actions.push(<RaisedButton
      label="Submit"
      onClick={handleSubmit(onSubmitNutritionalBudget)}
      primary
    />);
  }
  actions.push(<RaisedButton
    label="Go Back"
    onClick={onCloseNutritionalBudgetModal}
    secondary
  />);

  return actions;
}

function biometricDataAvailable(biometricData) {
  return biometricData.birthdate &&
    biometricData.height &&
    biometricData.sex &&
    biometricData.weight;
}

function renderNutritionalBudgetForm(
  classes,
  handleSubmit,
  onSubmitNutritionalBudget,
  onRecommendedWeightChange,
  onCaloriesAdjustmentChange,
) {
  return (
    <div className={classes.textCentered}>
      <form
        className={classes.container}
        onSubmit={handleSubmit(onSubmitNutritionalBudget)}
      >
        <h2>Calculated Nutrition Budget</h2>
        <Field
          name="protein"
          floatingLabelText="Protein"
          style={styles.fieldWidth}
          component={TextField}
          type="number"
          disabled
        />
        <Field
          name="fat"
          floatingLabelText="Fat"
          style={styles.fieldWidth}
          component={TextField}
          type="number"
          disabled
        />
        <Field
          name="carbs"
          floatingLabelText="Carbs"
          style={styles.fieldWidth}
          component={TextField}
          type="number"
          disabled
        />
        <Field
          name="calories"
          floatingLabelText="Calories"
          style={styles.fieldWidth}
          component={TextField}
          type="number"
          disabled
        />
        <Field
          name="recommendedWeight"
          floatingLabelText="Recomended Weight"
          style={styles.fieldWidth}
          component={TextField}
          onChange={onRecommendedWeightChange}
          type="number"
        />
        <Field
          name="caloriesAdjustment"
          floatingLabelText="Calories Adjustment"
          style={styles.fieldWidth}
          component={TextField}
          onChange={onCaloriesAdjustmentChange}
          type="number"
        />
      </form>
    </div>
  );
}

function NutritionalBudget({
  classes,
  onCloseNutritionalBudgetModal,
  nutritionalBudgetModalOpen,
  onSubmitNutritionalBudget,
  handleSubmit,
  onCaloriesAdjustmentChange,
  onRecommendedWeightChange,
  biometricData,
  unitsSystem,
}) {
  const showCalculateNutritionalBudgetForm = biometricDataAvailable(biometricData);
  return (
    <div>
      <Dialog
        title="Nutritional Budget Calculator"
        actions={renderNutritionalBudgetActions(
          onCloseNutritionalBudgetModal,
          handleSubmit,
          onSubmitNutritionalBudget,
          showCalculateNutritionalBudgetForm,
        )}
        open={nutritionalBudgetModalOpen}
        autoScrollBodyContent
        modal
      >
        <h2 className={classes.textCentered}>Biometric Data</h2>
        <BiometricData unitsSystem={unitsSystem} biometricData={biometricData} />
        {showCalculateNutritionalBudgetForm ?
          renderNutritionalBudgetForm(
            classes,
            handleSubmit,
            onSubmitNutritionalBudget,
            onRecommendedWeightChange,
            onCaloriesAdjustmentChange,
          )
        : <span>We cannot calculate a budget without the missing Biometric data</span>
        }
      </Dialog>
    </div>
  );
}

NutritionalBudget.propTypes = {
  nutritionalBudgetModalOpen: PropTypes.bool.isRequired,
  onCloseNutritionalBudgetModal: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  unitsSystem: PropTypes.string.isRequired,
  biometricData: customPropTypes.biometricData,
  onCaloriesAdjustmentChange: PropTypes.func.isRequired,
  onRecommendedWeightChange: PropTypes.func.isRequired,
  onSubmitNutritionalBudget: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
    textCentered: PropTypes.string.isRequired,
  }).isRequired,
};

NutritionalBudget.defaultProps = {
  biometricData: {},
};

export default injectSheet(styles)(NutritionalBudget);
