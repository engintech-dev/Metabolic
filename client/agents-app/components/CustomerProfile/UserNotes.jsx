import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import { textCentered } from '../../../common/theme';

const styles = {
  userNotesContainer: textCentered,
};

class UserNotes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: props.notes,
      notesChanged: false,
      editingNotes: false,
      sendingNotes: false,
    };
  }

  enableEditMode() {
    this.setState({ editingNotes: true });
  }

  saveNotes() {
    if (this.state.notesChanged) {
      this.setState({ sendingNotes: true });
      this.props.updateUserNotes(this.props.userId, this.state.notes)
        .then(() => {
          this.setState({ editingNotes: false, notesChanged: false, sendingNotes: false });
        })
        .catch(() => {
          this.setState({ sendingNotes: false });
        });
    }
  }

  changeNotes(newNotes) {
    this.setState({ notes: newNotes, notesChanged: true });
  }

  render() {
    return (
      <div className={this.props.classes.userNotesContainer}>
        <h3>Notes</h3>
        <div>
          {
            this.state.editingNotes ?
              <TextField
                hintText="Write your notes"
                multiLine
                rows={2}
                rowsMax={4}
                onChange={(event, newValue) => this.changeNotes(newValue)}
                value={this.state.notes}
              />
            :
              <p>{this.state.notes}</p>
          }
        </div>
        <div className="notesActions">
          {
            this.state.editingNotes
            ? <FlatButton
              label="Save"
              onClick={() => this.saveNotes()}
              icon={this.state.sendingNotes && <CircularProgress size={30} />}
              disabled={!this.state.notesChanged || this.state.sendingNotes}
              secondary
            />
            : <FlatButton label="Edit" onClick={() => this.enableEditMode()} primary />
          }
        </div>
      </div>
    );
  }

}

UserNotes.defaultProps = {
  notes: '',
};

UserNotes.propTypes = {
  userId: PropTypes.string.isRequired,
  notes: PropTypes.string,
  updateUserNotes: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    userNotesContainer: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(UserNotes);
