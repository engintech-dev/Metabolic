import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import injectSheet from 'react-jss';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Loading from 'material-ui/CircularProgress';
import customPropTypes from '../../prop-types';
import Carousel from '../../../common/components/Carousel';
import { textCentered } from '../../../common/theme';

const styles = {
  nutritionTipsContainer: textCentered,
};

class NutritionTips extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newNutritionTip: '',
      tipChanged: false,
      writingTip: false,
      sendingNutritionTip: false,
    };
  }

  openNewTipForm() {
    this.setState({ writingTip: true });
  }

  closeNewTipForm() {
    this.setState({ writingTip: false, newNutritionTip: '', tipChanged: false });
  }

  changeTip(tip) {
    this.setState({ newNutritionTip: tip, tipChanged: true });
  }

  saveTip() {
    if (this.state.tipChanged) {
      this.setState({ sendingNutritionTip: true });
      this.props.addNewNutritionTip(this.props.customerId, this.state.newNutritionTip)
        .then(() => {
          this.setState({ sendingNutritionTip: false });
          this.closeNewTipForm();
        })
        .catch(() => {
          this.setState({ sendingNutritionTip: false });
        });
    }
  }

  renderNutritionTipForm() {
    if (!this.props.superAgentModeEnabled) {
      return null;
    }
    return (
      this.state.writingTip ?
        <div>
          <TextField
            hintText="Write your tip"
            multiLine
            rows={2}
            rowsMax={4}
            onChange={(event, newValue) => this.changeTip(newValue)}
            value={this.state.newNutritionTip}
          />
          <div>
            <FlatButton
              label="Save"
              onClick={() => this.saveTip()}
              disabled={this.state.sendingNutritionTip}
              icon={this.state.sendingNutritionTip && <Loading size={30} />}
              secondary
            />
            <FlatButton
              label="Close"
              onClick={() => this.closeNewTipForm()}
              disabled={this.state.sendingNutritionTip}
              primary
            />
          </div>
        </div>
      : <FlatButton label="Add Tip" onClick={() => this.openNewTipForm()} primary />
    );
  }

  render() {
    const { nutritionTipsLoading, nutritionTipsList, classes } = this.props;
    const nutritionTipsMessages = nutritionTipsList.map(nTip => nTip.message);
    let nutritionTipContent;

    if (nutritionTipsLoading) {
      nutritionTipContent = <Loading />;
    } else if (nutritionTipsMessages.isEmpty()) {
      nutritionTipContent = <p>No Nutrition Tips Available</p>;
    } else {
      nutritionTipContent = <Carousel content={nutritionTipsMessages} />;
    }

    return (
      <div className={classes.nutritionTipsContainer}>
        <h3>Nutrition Tips</h3>
        {this.renderNutritionTipForm()}
        {nutritionTipContent}
      </div>
    );
  }
}

NutritionTips.defaultProps = {
  currentCustomerDay: null,
  nutritionTipsList: List(),
};

NutritionTips.propTypes = {
  customerId: PropTypes.string.isRequired,
  nutritionTipsLoading: PropTypes.bool.isRequired,
  nutritionTipsList: customPropTypes.nutritionTipsList,
  superAgentModeEnabled: PropTypes.bool.isRequired,
  addNewNutritionTip: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    nutritionTipsContainer: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(NutritionTips);
