import React from 'react';
import injectSheet from 'react-jss';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import { Card, CardHeader, CardMedia } from 'material-ui/Card';
import Title from 'material-ui/svg-icons/editor/title';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import IconButton from 'material-ui/IconButton';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import ArrowForward from 'material-ui/svg-icons/navigation/arrow-forward';
import RadioChecked from 'material-ui/svg-icons/toggle/radio-button-checked';
import RadioUnchecked from 'material-ui/svg-icons/toggle/radio-button-unchecked';
import Check from 'material-ui/svg-icons/navigation/check';
import Circle from 'material-ui/svg-icons/av/fiber-manual-record';
import QuestionMark from 'material-ui/svg-icons/action/help';
import theme from '../../../common/theme';
import { entryStatuses } from '../../utils/entry-constants';
import customPropTypes from '../../prop-types';
import Image from '../../../common/components/Image';

const IMAGE_SIZE = 280;
const PADDING_SIZE = 10;

const styles = {
  arrowControlContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: '1 1 0',
    width: IMAGE_SIZE - (2 * PADDING_SIZE),
    padding: PADDING_SIZE,
  },
  pageControlContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'radial-gradient(rgba(255,255,255,.5) 5%, rgba(255,255,255,0) 70%)',
    width: 'inherit',
  },
  radioButton: {
    color: 'white',
  },
  pageControl: {
    height: '8px',
    width: '8px',
  },
  controlContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: IMAGE_SIZE - (2 * PADDING_SIZE),
    width: IMAGE_SIZE - (2 * PADDING_SIZE),
    position: 'absolute',
    top: 0,
    left: 0,
  },
  container: {
    width: '280px',
    position: 'relative',
    margin: '10px',
  },
  entryImage: {
    objectFit: 'contain',
    width: IMAGE_SIZE,
    height: IMAGE_SIZE,
  },
  entryStatus: {
    height: '36px',
    width: '36px',
  },
  activePhoto: {
  },
  inactivePhoto: {
    display: 'none',
  },
  cardTitle: {
    fontSize: '20px',
    fontWeight: 'bold',
    lineHeight: '36px',
  },
  cardHeader: {
    padding: '6px',
    cursor: 'pointer',
  },
};

class EntryCard extends React.Component {
  constructor() {
    super();
    this.state = {
      currentIndex: 0,
      showControls: false,
    };

    this.onPageFactory = page => () => this.goTo(page);
    this.onNext = this.onNext.bind(this);
    this.onPrev = this.onPrev.bind(this);
    this.showControls = this.showControls.bind(this);
    this.hideControls = this.hideControls.bind(this);
    this.goToEntry = this.goToEntry.bind(this);
  }

  onNext() {
    this.page();
  }

  onPrev() {
    this.page(-1);
  }

  page(direction = 1) {
    const newIndex = this.state.currentIndex + direction;
    const numberOfPhotos = get(this.props.entry, 'photosData.length', 0);
    if (newIndex >= 0 && newIndex < numberOfPhotos) {
      this.setState({ currentIndex: newIndex });
    }
  }

  goToEntry() {
    this.props.goToReadOnlyEntry(this.props.entry.id);
  }

  goTo(index) {
    this.setState({ currentIndex: index });
  }

  showControls() {
    this.setState({ showControls: true });
  }
  hideControls() {
    this.setState({ showControls: false });
  }

  renderEntryStatus() {
    switch (this.props.entry.status) {
      case entryStatuses.answered:
        return (
          <Check
            style={styles.entryStatus}
            color={theme.palette.primary1Color}
            title="Answered"
          />
        );
      case entryStatuses.evaluated:
        return (
          <Check style={styles.entryStatus} color={theme.palette.greenColor} title="Evaluated" />
        );
      case entryStatuses.claimed:
        return (
          <Circle style={styles.entryStatus} color={theme.palette.primary1Color} title="Claimed" />
        );
      case entryStatuses.needsInfo:
        return (
          <QuestionMark
            style={styles.entryStatus}
            color={theme.palette.primary1Color}
            title="Needs Info"
          />
        );
      default:
        return (
          <Circle
            style={styles.entryStatus}
            color={theme.palette.disabledColor}
            title="No Status"
          />
        );
    }
  }

  renderControls() {
    const photosData = this.props.entry.photosData;
    const nextDisabled = this.state.currentIndex === photosData.length - 1;
    const prevDisabled = this.state.currentIndex === 0;
    let style = { opacity: '0.3' };
    if (this.state.showControls) {
      style = {};
    }
    return (
      <div className={this.props.classes.controlContainer}>
        <div className={this.props.classes.arrowControlContainer} style={style}>
          <FloatingActionButton mini onClick={this.onPrev} disabled={prevDisabled}>
            <ArrowBack />
          </FloatingActionButton>
          <FloatingActionButton mini onClick={this.onNext} disabled={nextDisabled}>
            <ArrowForward />
          </FloatingActionButton>
        </div>
        {this.renderPageControls()}
      </div>
    );
  }

  renderPageControls() {
    const className = this.props.classes.radioButton;
    const photosData = this.props.entry.photosData;
    const pageControls = photosData.map((photo, i) =>
      (
        <IconButton
          className={this.props.classes.pageControl}
          onClick={this.onPageFactory(i)}
          key={photo.id}
        >
          {(
            i === this.state.currentIndex
              ? <RadioChecked className={className} />
              : <RadioUnchecked className={className} />
          )}
        </IconButton>
      ),
    );
    return (
      <div className={this.props.classes.pageControlContainer} >
        {pageControls}
      </div>
    );
  }

  renderMedia() {
    const entry = this.props.entry;
    const photosData = entry.photosData;
    const photoCount = get(entry, 'photosData.length', 0);

    let media = (
      <div className={this.props.classes.entryImage}>
        <Title style={styles.entryImage} color="#ffffff" />
      </div>
    );

    if (photoCount > 0) {
      media = photosData.map((photo, i) =>
        (
          <Image
            src={photo.location}
            key={photo.id}
            className={this.props.classes.entryImage}
            color={this.props.color}
            style={(i === this.state.currentIndex ? styles.activePhoto : styles.inactivePhoto)}
          />
        ),
      );
    }

    return (
      <div>
        {media}
      </div>
    );
  }

  render() {
    const time = moment(this.props.entry.datetime).tz(this.props.tz).format('h:mm a');
    const photoCount = get(this.props.entry, 'photosData.length', 0);

    return (
      <Card
        className={this.props.classes.container}
        onMouseOver={this.showControls}
        onMouseOut={this.hideControls}
        zDepth={2}
      >
        <CardHeader
          title={time}
          avatar={this.renderEntryStatus()}
          titleStyle={styles.cardTitle}
          style={styles.cardHeader}
          onClick={this.goToEntry}
        />
        <CardMedia>
          {this.renderMedia()}
          {photoCount > 1 && this.renderControls()}
        </CardMedia>
      </Card>
    );
  }

}


EntryCard.propTypes = {
  entry: customPropTypes.entry.isRequired,
  color: PropTypes.string.isRequired,
  tz: PropTypes.string.isRequired,
  goToReadOnlyEntry: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    arrowControlContainer: PropTypes.string.isRequired,
    pageControlContainer: PropTypes.string.isRequired,
    radioButton: PropTypes.string.isRequired,
    pageControl: PropTypes.string.isRequired,
    controlContainer: PropTypes.string.isRequired,
    container: PropTypes.string.isRequired,
    entryImage: PropTypes.string.isRequired,
    entryStatus: PropTypes.string.isRequired,
    activePhoto: PropTypes.string.isRequired,
    inactivePhoto: PropTypes.string.isRequired,
    cardTitle: PropTypes.string.isRequired,
    cardHeader: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(EntryCard);
