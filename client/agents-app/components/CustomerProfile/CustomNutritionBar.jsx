import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import LinearProgress from 'material-ui/LinearProgress';
import muiThemeable from 'material-ui/styles/muiThemeable';

const styles = {
  barContainer: {
    display: 'flex',
    flexFlow: 'row',
  },
  bar: {
    height: '6px',
  },
};

const CustomNutritionBar = ({ nutrient, value, maxValue, classes, muiTheme }) => {
  const bars = [
    <div key={`${nutrient}Bar`} style={{ flex: 100 }}>
      <LinearProgress
        mode="determinate"
        max={100}
        value={value}
        color={muiTheme.colors.nutritionBar}
        style={styles.bar}
      />
    </div>,
  ];

  if (value > 100) {
    const exceededPercentage = maxValue - 100;
    bars.push(
      <div key={`${nutrient}ExceededBar`} style={{ flex: exceededPercentage }}>
        <LinearProgress
          mode="determinate"
          max={maxValue - 100}
          value={value - 100}
          color={muiTheme.colors.nutritionBarExceeded}
          style={styles.bar}
        />
      </div>,
    );
  }

  return (
    <div className={classes.barContainer}>
      {bars}
    </div>
  );
};

CustomNutritionBar.propTypes = {
  nutrient: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  maxValue: PropTypes.number.isRequired,
  muiTheme: PropTypes.shape({
    colors: PropTypes.shape({
      nutritionBar: PropTypes.string.isRequired,
      nutritionBarExceeded: PropTypes.string.isRequired,
    }),
  }).isRequired,
  classes: PropTypes.shape({
    barContainer: PropTypes.string.isRequired,
    bar: PropTypes.string.isRequired,
  }).isRequired,
};

export default muiThemeable()(injectSheet(styles)(CustomNutritionBar));
