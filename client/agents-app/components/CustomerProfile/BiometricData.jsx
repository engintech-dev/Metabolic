import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import moment from 'moment';
import customPropTypes from '../../prop-types';
import { unitsSystems, prettyEnglishHeight, computeBMI } from '../../../common/lib/numbers';
import { tableWrapper } from '../../../common/theme';

const styles = {
  tableWrapper,
};

function displayHeight(unitsSystem, height) {
  return unitsSystem === unitsSystems.english ? prettyEnglishHeight(height) : `${height} cm`;
}

function displayWeight(unitsSystem, weight) {
  return unitsSystem === unitsSystems.english ? `${weight} lb` : `${weight} kg`;
}

const BiometricData = ({ unitsSystem, biometricData: { birthdate, sex, height, weight } }) => (
  <Table selectable={false} style={styles.tableWrapper}>
    <TableBody displayRowCheckbox={false}>
      <TableRow>
        <TableRowColumn>Birthdate</TableRowColumn>
        <TableRowColumn>{birthdate ? moment(birthdate, 'YYYY-MM-DD HH:mm:ss').format('MM-DD-YYYY') : 'N/A'}</TableRowColumn>
      </TableRow>
      <TableRow>
        <TableRowColumn>Sex</TableRowColumn>
        <TableRowColumn>{sex || 'N/A'}</TableRowColumn>
      </TableRow>
      <TableRow>
        <TableRowColumn>Weight</TableRowColumn>
        <TableRowColumn>{weight ? displayWeight(unitsSystem, weight) : 'N/A'}</TableRowColumn>
      </TableRow>
      <TableRow>
        <TableRowColumn>Height</TableRowColumn>
        <TableRowColumn>{height ? displayHeight(unitsSystem, height) : 'N/A'}</TableRowColumn>
      </TableRow>
      <TableRow>
        <TableRowColumn>BMI</TableRowColumn>
        <TableRowColumn>{(weight && height) ? computeBMI(weight, height, unitsSystem) : 'N/A'}</TableRowColumn>
      </TableRow>
    </TableBody>
  </Table>
);

BiometricData.propTypes = {
  unitsSystem: PropTypes.string.isRequired,
  biometricData: customPropTypes.biometricData,
};

BiometricData.defaultProps = {
  biometricData: {},
};

export default BiometricData;
