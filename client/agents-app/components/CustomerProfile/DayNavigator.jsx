import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import Loading from '../../../common/components/Loading';
import theme from '../../../common/theme';
import customPropTypes from '../../prop-types';
import NutritionBars from './NutritionBars';
import EntryCard from '../../containers/CustomerProfile/EntryCard';
import DaySwitcher from '../../../common/components/DaySwitcher';

const IMAGE_SIZE = 280;

const styles = {
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
  },
  navigatorWrapper: {
    extend: 'wrapper',
    alignItems: 'baseline',
  },
  dayTitleWrapper: {
    width: '150px',
    textAlign: 'center',
  },
  entriesWrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    '&:after': {
      flex: 'auto',
      content: '""',
    },
  },
  entry: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: '15px',
    cursor: 'pointer',
  },
  entryImage: {
    objectFit: 'contain',
    width: IMAGE_SIZE,
    height: IMAGE_SIZE,
  },
  iconContainer: {
    backgroundColor: '#bcbcbc',
  },
  time: {
    margin: '5px 0 0 0',
    textAlign: 'center',
  },
};

function renderEntry(entry, customerTimezone) {
  return (
    <EntryCard
      key={entry.id}
      entry={entry}
      color={theme.palette.primary1Color}
      tz={customerTimezone}
    />
  );
}

const DayNavigator = ({
  customerId,
  customerTimezone,
  currentCustomerDay,
  entriesForCurrentDay,
  loading,
  moveToCustomerDay,
  classes,
}) => (
  <div>
    <DaySwitcher
      timezone={customerTimezone}
      currentDate={currentCustomerDay.date}
      disabled={loading}
      onDaySwitchClick={nextDate => moveToCustomerDay(customerId, nextDate)}
    />

    { loading
      ? <Loading />
      : (
        <div>
          <NutritionBars currentCustomerDay={currentCustomerDay} />
          <div className={classes.entriesWrapper}>
            {entriesForCurrentDay
              .map(entry => renderEntry(entry, customerTimezone))}
          </div>
        </div>
      )
    }
  </div>
);

DayNavigator.propTypes = {
  customerId: PropTypes.string.isRequired,
  customerTimezone: PropTypes.string,
  currentCustomerDay: customPropTypes.customerDay.isRequired,
  entriesForCurrentDay: customPropTypes.entries.isRequired,
  loading: PropTypes.bool.isRequired,
  moveToCustomerDay: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    wrapper: PropTypes.string.isRequired,
    entriesWrapper: PropTypes.string.isRequired,
    entry: PropTypes.string.isRequired,
    entryImage: PropTypes.string.isRequired,
    iconContainer: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
  }).isRequired,
};

DayNavigator.defaultProps = {
  customerTimezone: undefined,
};

export default injectSheet(styles)(DayNavigator);
