import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { FormSection, Field } from 'redux-form';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import isEmpty from 'lodash/isEmpty';
import InternalGoalInput, { internalGoalIntegrity } from './InternalGoalInput';
import GoalInput, { userGoalIntegrity } from './GoalInput';
import customPropTypes from '../../../prop-types';
import { container, textCentered, centeredButtonWrapper } from '../../../../common/theme';

const styles = {
  buttonContainerRight: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  buttonContainerCenter: {
    display: 'flex',
    justifyContent: 'center',
  },
  submitWrapper: centeredButtonWrapper,
  container,
  textCentered,
};

function renderInternalGoalsInputs(internalGoalsActiveIds, handleInternalGoalRemoved) {
  return internalGoalsActiveIds.map(id => (
    <Field
      key={id}
      name={id}
      component={InternalGoalInput}
      handleRemove={() => handleInternalGoalRemoved(id)}
      validate={internalGoalIntegrity}
    />
  ));
}

function renderUserGoalInput(goal) {
  if (isEmpty(goal)) {
    return null;
  }

  return (
    <Field
      key="otherGoal"
      name="otherGoal"
      text={goal.text}
      component={GoalInput}
      validate={userGoalIntegrity}
    />
  );
}

function renderGoalInputs(goals) {
  return goals.map(preset => (
    <Field
      key={preset.name}
      name={preset.name}
      text={preset.text}
      component={GoalInput}
      validate={userGoalIntegrity}
    />
  ));
}

const GoalsForm = ({
  submitting,
  disabled,
  internalGoalsActiveIds,
  customerOtherGoal,
  customerPresetGoals,
  handleStopEditing,
  handleInternalGoalAdded,
  handleInternalGoalRemoved,
  handleSubmitGoals,
  classes,
}) => (
  <form className={classes.container}>
    <div className={classes.buttonContainerRight}>
      <FlatButton label="Cancel" onClick={handleStopEditing} primary />
    </div>

    <h1 className={classes.textCentered}>Goals</h1>

    <FormSection name="goals">
      {renderGoalInputs(customerPresetGoals)}
    </FormSection>

    <FormSection name="userGoal">
      <h3 className={classes.textCentered}>User Goal</h3>
      {renderUserGoalInput(customerOtherGoal)}
    </FormSection>

    <FormSection name="internalGoals">
      <h3 className={classes.textCentered}>Internal Goals</h3>
      <div className={classes.buttonContainerCenter}>
        <FlatButton label="Add" onClick={handleInternalGoalAdded} primary />
      </div>

      {renderInternalGoalsInputs(internalGoalsActiveIds, handleInternalGoalRemoved)}
    </FormSection>

    <div className={classes.submitWrapper}>
      <RaisedButton
        label="Submit Goals"
        labelPosition="before"
        onClick={handleSubmitGoals}
        disabled={disabled || submitting}
        icon={submitting ? <CircularProgress size={25} color="white" /> : null}
        fullWidth
        primary
      />
    </div>
  </form>
);

GoalsForm.defaultProps = {
  customerOtherGoal: {},
  customerPresetGoals: [],
};

GoalsForm.propTypes = {
  submitting: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  internalGoalsActiveIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  customerOtherGoal: customPropTypes.otherGoal,
  customerPresetGoals: customPropTypes.presetGoals,
  handleStopEditing: PropTypes.func.isRequired,
  handleInternalGoalAdded: PropTypes.func.isRequired,
  handleInternalGoalRemoved: PropTypes.func.isRequired,
  handleSubmitGoals: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
    buttonContainerRight: PropTypes.string.isRequired,
    buttonContainerCenter: PropTypes.string.isRequired,
    submitWrapper: PropTypes.string.isRequired,
    textCentered: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(GoalsForm);
