import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import isEmpty from 'lodash/isEmpty';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import customPropTypes from '../../../prop-types';
import { container, textCentered } from '../../../../common/theme';

const styles = {
  buttonContainerRight: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  textCentered,
  container,
};

function renderTableWithItems(rows) {
  return (
    <Table selectable={false}>
      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
        <TableRow>
          <TableHeaderColumn>Goal</TableHeaderColumn>
          <TableHeaderColumn>Status</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {rows}
      </TableBody>
    </Table>
  );
}

function renderPresetsGoals(presetsGoals, classes) {
  if (isEmpty(presetsGoals)) {
    return <h3 className={classes.textCentered}>There are no Goals Yet</h3>;
  }

  const rows = presetsGoals.map(goal => (
    <TableRow key={goal.name}>
      <TableRowColumn>{goal.text}</TableRowColumn>
      <TableRowColumn>{goal.status}</TableRowColumn>
    </TableRow>
  ));

  return renderTableWithItems(rows);
}

function renderInternalGoals(internalGoalsActiveIds, internalGoals, classes) {
  if (internalGoalsActiveIds.length === 0) {
    return <h3 className={classes.textCentered}>There are no Internal Goals Yet</h3>;
  }

  const rows = internalGoalsActiveIds.map(id => (
    <TableRow key={id}>
      <TableRowColumn>{internalGoals[id].text}</TableRowColumn>
      <TableRowColumn>{internalGoals[id].status}</TableRowColumn>
    </TableRow>
  ));

  return renderTableWithItems(rows);
}

function renderUserGoal(otherGoal, classes) {
  if (isEmpty(otherGoal)) {
    return <h3 className={classes.textCentered}>There is no User Goal Yet</h3>;
  }

  const rows = [
    <TableRow key="userGoal">
      <TableRowColumn>{otherGoal.text}</TableRowColumn>
      <TableRowColumn>{otherGoal.status}</TableRowColumn>
    </TableRow>,
  ];

  return renderTableWithItems(rows);
}

const ReadOnlyGoals = ({
  internalGoalsActiveIds,
  internalGoals,
  customerPresetGoals,
  customerOtherGoal,
  handleEditClicked,
  classes,
}) => (
  <div className={classes.container}>
    <div className={classes.buttonContainerRight}>
      <FlatButton label="Edit" onClick={handleEditClicked} primary />
    </div>

    <h1 className={classes.textCentered}>Goals</h1>
    {renderPresetsGoals(customerPresetGoals, classes)}

    <h2 className={classes.textCentered}>User Goal</h2>
    {renderUserGoal(customerOtherGoal, classes)}

    <h2 className={classes.textCentered}>Internal Goals</h2>
    {renderInternalGoals(internalGoalsActiveIds, internalGoals, classes)}
  </div>
);

ReadOnlyGoals.defaultProps = {
  customerPresetGoals: [],
  customerOtherGoal: {},
};

ReadOnlyGoals.propTypes = {
  internalGoalsActiveIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  customerPresetGoals: customPropTypes.presetGoals,
  customerOtherGoal: customPropTypes.otherGoal,
  internalGoals: PropTypes.shape({}).isRequired,
  handleEditClicked: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
    buttonContainerRight: PropTypes.string.isRequired,
    textCentered: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(ReadOnlyGoals);
