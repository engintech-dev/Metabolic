import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import customPropTypes from '../../prop-types';
import CustomNutritionBar from './CustomNutritionBar';
import { textCentered } from '../../../common/theme';

const styles = {
  nutrientContainer: {
    padding: '10px',
  },
  nutrientInformation: {
    display: 'flex',
    flexFlow: 'row',
  },
  nutrientName: {
    textTransform: 'uppercase',
    marginRight: 'auto',
  },
  nutrientBudget: {
    alignContent: 'flexEnd',
  },
  nutrientPercentage: {
    marginLeft: '10px',
    alignContent: 'flexEnd',
  },
  textCentered,
};


const renderNutrientBar = (nutrient, nutrientPercentage, maxPercentage, classes) =>
  <div key={nutrient.name} className={classes.nutrientContainer}>
    <div className={classes.barContainer}>
      <CustomNutritionBar
        nutrient={nutrient.name}
        value={nutrientPercentage}
        maxValue={maxPercentage}
      />
    </div>
    <div className={classes.nutrientInformation}>
      <span className={classes.nutrientName}>{nutrient.humanName}</span>
      <span className={classes.nutrientBudget}>({nutrient.budget}{nutrient.unit})</span>
      <span className={classes.nutrientPercentage}>{nutrientPercentage}%</span>
    </div>
  </div>;


const renderNutrientInformation = (nutrientsSummary, classes) => {
  const nutrientPercentages = nutrientsSummary
    .map(nutrient => Math.round((nutrient.value / nutrient.budget) * 100));
  const maxPercentage = Math.max(...nutrientPercentages);

  return nutrientsSummary.map((nutrient, index) =>
    renderNutrientBar(nutrient, nutrientPercentages[index], maxPercentage, classes));
};

const NutritionBars = ({ currentCustomerDay, classes }) => (
  <div>
    <h3 className={classes.textCentered}>Nutritional Information</h3>
    {renderNutrientInformation(currentCustomerDay.nutritionSummary, classes)}
  </div>
);


NutritionBars.propTypes = {
  currentCustomerDay: customPropTypes.customerDay.isRequired,
  classes: PropTypes.shape({
    nutrientContainer: PropTypes.string.isRequired,
    nutrientInformation: PropTypes.string.isRequired,
    nutrientBudget: PropTypes.string.isRequired,
    nutrientName: PropTypes.string.isRequired,
    nutrientPercentage: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(NutritionBars);
