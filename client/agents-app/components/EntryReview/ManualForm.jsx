import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { TextField } from 'redux-form-material-ui';
import injectSheet from 'react-jss';
import map from 'lodash/map';
import customPropTypes from '../../prop-types';
import { required } from '../../../common/forms/validators';
import { narrowContainer, textCentered } from '../../../common/theme';

const styles = {
  textCentered,
  formContainer: narrowContainer,
  fieldContainer: {
    display: 'flex',
    alignItems: 'baseline',
  },
};

function renderFields(percentages, handleNutrientChanged, disabled, classes) {
  const nutrientWords = { proteins: 'Proteins', fats: 'Fats', carbs: 'Carbs' };
  return map(nutrientWords, (value, key) => (
    <div key={key} className={classes.fieldContainer}>
      <Field
        type="number"
        name={nutrientWords[key].toLowerCase()}
        component={TextField}
        hintText="0"
        floatingLabelText={`${nutrientWords[key]} grams`}
        validate={[required]}
        onChange={handleNutrientChanged}
        disabled={disabled}
        fullWidth
      />
      <span>{`${percentages[key]}%`}</span>
    </div>
  ));
}

const ManualForm = ({
  percentages,
  disabled,
  classes,
  handleNutrientChanged,
  handleCaloriesChanged,
}) => (
  <div className={classes.formContainer}>
    <h1 className={classes.textCentered}>Manually Resolve</h1>
    <form noValidate>
      {renderFields(percentages, handleNutrientChanged, disabled, classes)}
      <Field
        type="number"
        name="calories"
        component={TextField}
        hintText="0"
        floatingLabelText="Calories"
        onChange={handleCaloriesChanged}
        onFocus={event => event.target.select()}
        disabled={disabled}
        fullWidth
      />
    </form>
  </div>
);

ManualForm.propTypes = {
  disabled: PropTypes.bool.isRequired,
  percentages: customPropTypes.nutrients.isRequired,
  handleNutrientChanged: PropTypes.func.isRequired,
  handleCaloriesChanged: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
    formContainer: PropTypes.string.isRequired,
    fieldContainer: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(ManualForm);
