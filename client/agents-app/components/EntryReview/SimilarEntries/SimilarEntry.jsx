import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import injectSheet from 'react-jss';
import { blue700 } from 'material-ui/styles/colors';
import { List } from 'immutable';
import FoodInformation from '../../../containers/EntryReview/ReadOnly/FoodInformation';
import Carousel from '../../../../common/components/Carousel';
import customPropTypes from '../../../prop-types';

const styles = {
  carouselContainer: {
    backgroundColor: blue700,
    color: '#ffffff',
    paddingBottom: '10px',
    paddingTop: '10px',
  },
  textCentered: {
    textAlign: 'center',
  },
  dialogRoot: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 0,
  },
  dialogContent: {
    position: 'relative',
    width: '80vw',
    transform: '',
  },
  dialogBody: {
    paddingBottom: 0,
  },
};

class SimilarEntry extends Component {
  constructor(props) {
    super(props);
    this.closeDialog = this.closeDialog.bind(this);
    this.onCopyEntryReview = this.onCopyEntryReview.bind(this);
  }

  onCopyEntryReview() {
    const { entry, copyEntryReview, targetEntryId, addedFood } = this.props;
    copyEntryReview(targetEntryId, entry, addedFood);
    this.closeDialog();
  }

  closeDialog() {
    this.props.onClose();
  }

  render() {
    const { entry, classes } = this.props;
    const shouldShowCarousel = entry.photosData && entry.photosData.length > 0;
    const actions = [
      <FlatButton
        label="Apply Evaluation"
        primary
        onClick={this.onCopyEntryReview}
      />,
      <FlatButton
        label="Close"
        primary
        onClick={this.closeDialog}
      />,
    ];
    return (
      <Dialog
        actions={actions}
        open
        onRequestClose={this.closeDialog}
        repositionOnUpdate={false}
        autoScrollBodyContent
        style={styles.dialogRoot}
        bodyStyle={styles.dialogBody}
        contentStyle={styles.dialogContent}
      >
        <div className={classes.carouselContainer}>
          <p className={classes.textCentered}>{entry.text}</p>
          {shouldShowCarousel && <Carousel content={entry.photosData} />}
        </div>

        <FoodInformation
          entryId={entry.id}
          nutrientsSummary={entry.nutrientsSummary}
          eatenFoodsData={entry.eatenFoodsData}
        />
      </Dialog>
    );
  }
}

SimilarEntry.propTypes = {
  entry: customPropTypes.entry.isRequired,
  onClose: PropTypes.func.isRequired,
  copyEntryReview: PropTypes.func.isRequired,
  targetEntryId: PropTypes.string.isRequired,
  addedFood: customPropTypes.foodList,
  classes: PropTypes.shape({
    carouselContainer: PropTypes.string.isRequired,
    textCentered: PropTypes.string.isRequired,
    dialogRoot: PropTypes.string.isRequired,
    dialogContent: PropTypes.string.isRequired,
    dialogBody: PropTypes.string.isRequired,
  }).isRequired,
};

SimilarEntry.defaultProps = {
  addedFood: List(),
};

export default injectSheet(styles)(SimilarEntry);
