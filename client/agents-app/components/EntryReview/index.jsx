import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import get from 'lodash/get';
import pick from 'lodash/pick';
import { blue700 } from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import BackIcon from 'material-ui/svg-icons/navigation/chevron-left';
import Avatar from 'material-ui/Avatar';
import AccountIcon from 'material-ui/svg-icons/action/account-circle';
import Chip from 'material-ui/Chip';
import { reviewTypes } from '../../utils/review-constants';
import EntryReviewToolbar from '../../containers/EntryReviewToolbar';
import SimilarEntries from '../../containers/EntryReview/SimilarEntries';
import ManualForm from '../../containers/EntryReview/ManualForm';
import FoodForm from '../../containers/EntryReview/FoodForm';
import AnalyticsForm from '../../containers/EntryReview/AnalyticsForm';
import EntryMainData from './EntryMainData';
import Chat from '../../containers/Chat';
import customPropTypes from '../../prop-types';
import ClaimToolbar from '../../containers/ClaimToolbar';
import SpellingErrors from '../../containers/SpellingErrors';
import { origins } from '../../store/reducers/ui/spell-check';
import { textCentered, centeredButtonWrapper, errorStyle } from '../../../common/theme';

const styles = {
  carouselContainer: {
    backgroundColor: blue700,
    color: '#ffffff',
    paddingBottom: '10px',
    paddingTop: '10px',
  },
  button: {
    margin: '10px',
  },
  headerActions: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingBottom: '10px',
  },
  centeredButtonWrapper,
  textCentered,
  errorStyle,
};

export function shouldBeMainFood(grams, totalGrams, biggestMass) {
  if (grams === totalGrams) {
    return false;
  }

  return grams === biggestMass || ((biggestMass / totalGrams) - (grams / totalGrams)) < 0.1;
}

function renderSpellCheckActions(handleSubmit, handleCloseSpellCheckModal) {
  return [
    <RaisedButton label="Go Back And Fix" onClick={handleCloseSpellCheckModal} primary />,
    <FlatButton label="Submit Anyway" onClick={() => handleSubmit(true)} primary />,
  ];
}

function EntryReview({
  entry,
  customer,
  currentReviewState,
  currentReviewType,
  disabled,
  loading,
  spellCheckModalOpen,
  submitBody,
  classes,
  handleSubmit,
  handleCloseSpellCheckModal,
  goToCustomer,
  goBack,
  handleAllImagesReviewedCheck,
  evaluationError,
}) {
  const customerAvatarData = customer.avatarData;
  const text = get(submitBody, 'message.text');
  const analyticsInitialValues = pick(entry.entryAnalytics, 'trafficLightEvaluation');
  const canNotSubmit = evaluationError ? true : disabled;
  return (
    <div>
      <div className={classes.headerActions}>
        <FlatButton label="Back" icon={<BackIcon />} onClick={goBack} />
        <span>{`Status: ${entry.status}`}</span>
        <Chip onClick={() => goToCustomer(customer.id)}>
          {
            customerAvatarData
            ? <Avatar src={customerAvatarData.thumbnailLocation || customerAvatarData.location} />
            : <Avatar icon={<AccountIcon />} />
          }
          {customer.name}
        </Chip>
      </div>

      <ClaimToolbar entry={entry} />

      <EntryReviewToolbar
        currentReviewState={currentReviewState}
        currentReviewType={currentReviewType}
        disabled={loading}
      />

      <EntryMainData
        entry={entry}
        customerTimezone={customer.timezone}
        className={classes.carouselContainer}
        handleAllImagesReviewedCheck={handleAllImagesReviewedCheck}
      />

      <SimilarEntries entryId={entry.id} />

      {currentReviewType === reviewTypes.manual &&
        <ManualForm nutrientsSummary={entry.nutrientsSummary} disabled={loading} />}

      {currentReviewType === reviewTypes.search &&
        <FoodForm entryId={entry.id} eatenFoodsData={entry.eatenFoodsData} disabled={loading} />}

      {currentReviewType === reviewTypes.na &&
        <p className={classes.textCentered}>This entry will be answered without evaluation</p>}

      {(currentReviewType === reviewTypes.search || currentReviewType === reviewTypes.manual) &&
        <AnalyticsForm initialValues={analyticsInitialValues} disabled={loading} />}

      <Chat entryId={entry.id} disabled={loading} />

      <div className={classes.centeredButtonWrapper}>
        <RaisedButton
          label="Submit"
          labelPosition="before"
          onClick={() => handleSubmit(false)}
          disabled={canNotSubmit}
          icon={loading ? <CircularProgress size={25} color="white" /> : null}
          fullWidth
          primary
        />
      </div>

      {evaluationError &&
        <div className={classes.errorStyle}>
          <span>{evaluationError}</span>
        </div>
      }

      <Dialog
        title="Evaluation Answer Spell Check"
        actions={renderSpellCheckActions(handleSubmit, handleCloseSpellCheckModal)}
        open={spellCheckModalOpen}
        autoScrollBodyContent
        modal
      >
        {text && <SpellingErrors origin={origins.composer} originalText={text} />}
      </Dialog>
    </div>
  );
}

EntryReview.propTypes = {
  entry: customPropTypes.entry.isRequired,
  evaluationError: PropTypes.string.isRequired,
  customer: customPropTypes.customer.isRequired,
  handleAllImagesReviewedCheck: PropTypes.func.isRequired,
  currentReviewState: PropTypes.string.isRequired,
  currentReviewType: PropTypes.string,
  disabled: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  spellCheckModalOpen: PropTypes.bool.isRequired,
  submitBody: PropTypes.shape({ message: customPropTypes.reviewMessage }),
  handleSubmit: PropTypes.func.isRequired,
  handleCloseSpellCheckModal: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  goToCustomer: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    carouselContainer: PropTypes.string.isRequired,
    button: PropTypes.string.isRequired,
    centeredButtonWrapper: PropTypes.string.isRequired,
    textCentered: PropTypes.string.isRequired,
    errorStyle: PropTypes.string.isRequired,
    headerActions: PropTypes.string.isRequired,
  }).isRequired,
};

EntryReview.defaultProps = {
  currentReviewType: null,
  submitBody: undefined,
};

export default injectSheet(styles)(EntryReview);
