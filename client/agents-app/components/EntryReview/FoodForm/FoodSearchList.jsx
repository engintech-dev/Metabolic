import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'material-ui/List';
import customPropTypes from '../../../prop-types';
import FoodSearchForm from '../../../containers/EntryReview/FoodForm/FoodSearchForm';
import FoodSearchListItem from '../../../containers/EntryReview/FoodForm/FoodSearchListItem';

export const searchOptions = {
  all: 0,
  branded: 1,
  standard: 2,
};

function renderListItems(entryId, foodSearchResults) {
  return foodSearchResults
    .map(food => <FoodSearchListItem key={food.externalId} food={food} entryId={entryId} />);
}

const FoodSearchList = ({ entryId, foodSearchResults }) => (
  <div>
    <FoodSearchForm />
    <List>
      {renderListItems(entryId, foodSearchResults)}
    </List>
  </div>
);

FoodSearchList.propTypes = {
  entryId: PropTypes.string.isRequired,
  foodSearchResults: customPropTypes.foodSearchResultsList.isRequired,
};

export default FoodSearchList;
