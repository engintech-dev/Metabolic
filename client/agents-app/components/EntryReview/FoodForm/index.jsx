import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import injectSheet from 'react-jss';
import mapValues from 'lodash/mapValues';
import Divider from 'material-ui/Divider';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FoodSearchList from '../../../containers/EntryReview/FoodForm/FoodSearchList';
import EatenFoodForm from '../../../containers/EntryReview/FoodForm/EatenFoodForm';
import HumanNamesForm from '../../../containers/EntryReview/HumanNamesForm';
import NutrientCompositionTable from '../../../../common/components/NutrientCompositionTable';
import { calculateNutrientPercentage, translatedCaloriesPerGram } from '../../../../common/lib/nutrients';
import customPropTypes from '../../../prop-types';
import { container, centeredButtonWrapper } from '../../../../common/theme';

const DISABLE_HUMAN_NAMES_FORM = true; // TODO: update this when required (DEV-18)

const styles = {
  dialogRoot: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 0,
  },
  dialogContent: {
    position: 'relative',
    width: '80vw',
    transform: '',
  },
  dialogBody: {
    paddingBottom: 0,
  },
  container,
  centeredButtonWrapper,
};

class FoodForm extends Component {
  constructor(props) {
    super(props);

    this.state = { searchOpen: false };

    this.onDialogOpen = this.onDialogOpen.bind(this);
    this.onDialogClose = this.onDialogClose.bind(this);
  }

  onDialogOpen() {
    this.setState({ searchOpen: true });
  }

  onDialogClose() {
    this.setState({ searchOpen: false });
  }

  calculateComposition(nutrientName) {
    return this.props.eatenFood.reduce((totalNutrient, { foodId, measureAmount, measureId }) => {
      const food = this.props.addedFood.find(({ id }) => id === foodId);
      const nutrientValue = food.nutrientsValues.find(({ name }) => name === nutrientName);

      if (!nutrientValue || !measureAmount) {
        return totalNutrient;
      }

      const { value } = nutrientValue;
      const { equivalentValue } = food.measuresValues.find(({ id }) => measureId === id);
      return totalNutrient + ((value / 100) * +(measureAmount) * equivalentValue);
    }, 0);
  }

  calculateNutrientsValuesAndPercentages() {
    const calories = this.calculateComposition('calories');

    return mapValues(translatedCaloriesPerGram, (value, key) => {
      const amount = this.calculateComposition(key);
      return {
        amount,
        percentage: calculateNutrientPercentage(amount, calories, translatedCaloriesPerGram[key]),
      };
    });
  }

  renderHumanNamesForm() {
    if (DISABLE_HUMAN_NAMES_FORM || this.props.addedFood.isEmpty()) {
      return null;
    }

    return (
      <div>
        <Divider />
        <div className={this.props.classes.container}>
          <HumanNamesForm
            addedFood={this.props.addedFood}
            eatenFoodsData={this.props.eatenFoodsData}
          />
        </div>
      </div>
    );
  }

  renderNutrientCompositionTable() {
    if (this.props.addedFood.isEmpty() || this.props.eatenFood.isEmpty()) {
      return null;
    }

    return (
      <div className={this.props.classes.container}>
        <NutrientCompositionTable
          nutrients={this.calculateNutrientsValuesAndPercentages()}
          calories={this.calculateComposition('calories')}
        />
      </div>
    );
  }

  render() {
    return (
      <div>
        <Divider />

        <div className={this.props.classes.centeredButtonWrapper}>
          <RaisedButton
            label="Search"
            onClick={this.onDialogOpen}
            fullWidth
            disabled={this.props.disabled}
          />
        </div>

        <Dialog
          title="Search For Food"
          open={this.state.searchOpen}
          actions={[<RaisedButton label="Close" onClick={this.onDialogClose} />]}
          onRequestClose={this.onDialogClose}
          autoScrollBodyContent
          style={styles.dialogRoot}
          bodyStyle={styles.dialogBody}
          contentStyle={styles.dialogContent}
          repositionOnUpdate={false}
        >
          <FoodSearchList entryId={this.props.entryId} />
        </Dialog>

        <Divider />

        <div className={this.props.classes.container}>
          <EatenFoodForm
            entryId={this.props.entryId}
            addedFood={this.props.addedFood}
            eatenFoodsData={this.props.eatenFoodsData}
            disabled={this.props.disabled}
          />
        </div>

        {this.renderHumanNamesForm()}

        <Divider />

        {this.renderNutrientCompositionTable()}
      </div>
    );
  }
}

FoodForm.propTypes = {
  entryId: PropTypes.string.isRequired,
  addedFood: customPropTypes.foodList,
  eatenFood: customPropTypes.eatenFoodList.isRequired,
  eatenFoodsData: customPropTypes.eatenFoods.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
    centeredButtonWrapper: PropTypes.string.isRequired,
  }).isRequired,
  disabled: PropTypes.bool,
};

FoodForm.defaultProps = {
  eatenFoodsData: [],
  addedFood: List(),
  disabled: false,
};

export default injectSheet(styles)(FoodForm);
