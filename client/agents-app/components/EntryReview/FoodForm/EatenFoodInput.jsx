import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import injectSheet from 'react-jss';
import { capitalize, keyBy, values } from 'lodash';
import customPropTypes from '../../../prop-types';
import { errorStyle } from '../../../../common/theme';
import { pluralizeNutrientName, oldNutrientsNames } from '../../../../common/lib/nutrients';

const styles = {
  inputWrapper: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginTop: '20px',
  },
  field: {
    marginLeft: '10px',
    marginRight: '10px',
  },
  mainFood: {
    fontWeight: 'bold',
  },
  foodName: {
    textAlign: 'right',
    marginRight: '25px',
    width: '400px',
  },
  foodMacroNutrients: {
    textAlign: 'left',
    width: '300px',
  },
  foodMacroNutrientDetail: {
    fontSize: '13px',
    margin: '0px',
  },
  error: {
    ...errorStyle,
    marginTop: '25px',
  },
};

export function eatenFoodIntegrity(value) {
  if (!value) {
    return 'required';
  }

  const measureAmount = +(value.measureAmount);
  if (!measureAmount || !value.measureId) {
    return 'Both amount and measure are required';
  } else if (measureAmount <= 0) {
    return 'Amount must be greater than 0.';
  }

  return undefined;
}

const renderMenuItems = measuresValues => (
  Object.values(measuresValues)
    .map(({ id, label }) => <MenuItem key={id} value={id} primaryText={label} />)
);

const buildEatenFood = (foodId, eatenFood, newEatenFood) =>
  ({ foodId, ...eatenFood, ...newEatenFood });

const renderMaconutrientsInformation = (nutrientsValues, classes) => {
  const nutrients = keyBy(nutrientsValues, 'name');
  return values(oldNutrientsNames).map(nutrient =>
    <p key={nutrient} className={classes.foodMacroNutrientDetail}>
      {`${capitalize(pluralizeNutrientName(nutrient))}: ${nutrients[nutrient].value} ${nutrients[nutrient].unit}`}
    </p>,
  );
};

const EatenFoodInput = ({
  input: { value, onChange },
  meta: { error },
  foodId,
  foodName,
  mainFood,
  massPercentage,
  nutrientsValues,
  measuresValues,
  handleRemove,
  classes,
}) => (
  <div className={mainFood ? classes.mainFood : undefined}>
    <div className={classes.inputWrapper}>
      <p className={classes.foodName}>{foodName}</p>
      <div className={classes.foodMacroNutrients}>
        {renderMaconutrientsInformation(nutrientsValues, classes)}
      </div>

      <TextField
        floatingLabelText="Amount"
        hintText="e.g. 1"
        onChange={(event, measureAmount) =>
          onChange(buildEatenFood(foodId, value, { measureAmount }))}
        type="number"
        value={value.measureAmount}
        className={classes.field}
      />

      <SelectField
        floatingLabelText="Measures"
        onChange={(event, key, measureId) =>
          onChange(buildEatenFood(foodId, value, { measureId }))}
        value={value.measureId}
        className={classes.field}
      >
        {renderMenuItems(measuresValues)}
      </SelectField>

      <TextField
        floatingLabelText="Mass %"
        value={`${massPercentage || 0}%`}
        className={classes.field}
        disabled
      />

      <FlatButton label="remove" onClick={handleRemove} primary />
    </div>

    {error && <p className={classes.error}>{error}</p>}
  </div>
);

EatenFoodInput.propTypes = {
  input: PropTypes.shape({
    value: customPropTypes.eatenFood,
    onChange: PropTypes.func.isRequired,
  }),
  meta: PropTypes.shape({ error: PropTypes.string }),
  foodId: PropTypes.string.isRequired,
  foodName: PropTypes.string.isRequired,
  mainFood: PropTypes.bool.isRequired,
  massPercentage: PropTypes.number.isRequired,
  nutrientsValues: customPropTypes.nutrientValues.isRequired,
  measuresValues: customPropTypes.measuresValues.isRequired,
  handleRemove: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    inputWrapper: PropTypes.string.isRequired,
    field: PropTypes.string.isRequired,
    mainFood: PropTypes.string.isRequired,
    foodName: PropTypes.string.isRequired,
    foodMacroNutrients: PropTypes.string.isRequired,
    foodMacroNutrientDetail: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired,
  }).isRequired,
};

EatenFoodInput.defaultProps = {
  input: { value: undefined },
  meta: { error: undefined },
};

export default injectSheet(styles)(EatenFoodInput);
