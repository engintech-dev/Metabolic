import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import injectSheet from 'react-jss';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Divider from 'material-ui/Divider';
import isEmpty from 'lodash/isEmpty';
import NutrientCompositionTable from '../../../../common/components/NutrientCompositionTable';
import customPropTypes from '../../../prop-types';
import { processNutrientsSummary } from '../../../../common/lib/nutrients';
import prettyNumber from '../../../../common/lib/numbers';
import { shouldBeMainFood } from '../index';
import { container, textCentered } from '../../../../common/theme';

const styles = {
  container,
  textCentered,
  mainFood: {
    fontWeight: 'bold',
  },
};

class FoodInformation extends PureComponent {
  renderEatenFood() {
    const {
      addedFood,
      massInformation: { perFood, totalGrams, biggestMass },
      classes,
    } = this.props;

    return addedFood.map(({ id, name }) => {
      const { label, measureAmount, equivalentValue } = perFood[id];
      const grams = measureAmount * equivalentValue;
      const isMainFood = shouldBeMainFood(grams, totalGrams, biggestMass);

      const amountText = label !== 'g'
        ? `${prettyNumber(measureAmount)}${label} (${prettyNumber(grams)}g)`
        : `${prettyNumber(grams)}g`;

      return (
        <TableRow key={id} className={isMainFood ? classes.mainFood : undefined}>
          <TableRowColumn>{name}</TableRowColumn>
          <TableRowColumn>{amountText}</TableRowColumn>
          <TableRowColumn>{Math.round((grams / totalGrams) * 100 * 100) / 100}%</TableRowColumn>
        </TableRow>
      );
    });
  }

  renderNutrientComposition() {
    const { nutrients, calories } = processNutrientsSummary(this.props.nutrientsSummary);
    return (
      <div className={this.props.classes.container}>
        <NutrientCompositionTable nutrients={nutrients} calories={calories} />
      </div>
    );
  }

  render() {
    const { massInformation, nutrientsSummary, classes } = this.props;

    if (isEmpty(massInformation) && isEmpty(nutrientsSummary)) {
      return (
        <h2 className={classes.textCentered}>
          This entry does not have any nutrient information yet.
        </h2>
      );
    } else if (isEmpty(massInformation)) {
      return this.renderNutrientComposition();
    }

    return (
      <div>
        <div className={classes.container}>
          <h2 className={classes.textCentered}>Added Food</h2>
          <Table selectable={false}>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                <TableHeaderColumn>Food</TableHeaderColumn>
                <TableHeaderColumn>Amount</TableHeaderColumn>
                <TableHeaderColumn>Mass Percentage</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderEatenFood()}
            </TableBody>
          </Table>
        </div>

        <Divider />

        {this.renderNutrientComposition()}
      </div>
    );
  }
}

FoodInformation.propTypes = {
  addedFood: customPropTypes.foodList,
  massInformation: customPropTypes.massInformation,
  nutrientsSummary: customPropTypes.nutrientsSummary,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
    textCentered: PropTypes.string.isRequired,
    mainFood: PropTypes.string.isRequired,
  }).isRequired,
};

FoodInformation.defaultProps = {
  addedFood: List(),
  massInformation: undefined,
  nutrientsSummary: [],
};

export default injectSheet(styles)(FoodInformation);
