import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import Divider from 'material-ui/Divider';
import { blue700 } from 'material-ui/styles/colors';
import FlatButton from 'material-ui/FlatButton';
import BackIcon from 'material-ui/svg-icons/navigation/chevron-left';
import Avatar from 'material-ui/Avatar';
import AccountIcon from 'material-ui/svg-icons/action/account-circle';
import Chip from 'material-ui/Chip';
import FoodInformation from '../../../containers/EntryReview/ReadOnly/FoodInformation';
import AnalyticsInformation from '../../../components/EntryReview/ReadOnly/AnalyticsInformation';
import Chat from '../../../containers/Chat';
import customPropTypes from '../../../prop-types';
import ClaimToolbar from '../../../containers/ClaimToolbar';
import EntryMainData from '../EntryMainData';

const styles = {
  carouselContainer: {
    backgroundColor: blue700,
    color: '#ffffff',
    paddingBottom: '10px',
    paddingTop: '10px',
  },
  headerActions: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingBottom: '10px',
  },
};

function ReadOnlyEntryReview({ entry, customer, goToCustomer, goBack, classes }) {
  const shouldShowAnalyticsInformation = !!entry.entryAnalytics;
  const customerAvatarData = customer.avatarData;

  return (
    <div>
      <div className={classes.headerActions}>
        <FlatButton label="Back" icon={<BackIcon />} onClick={goBack} />
        <span>{`Status: ${entry.status}`}</span>
        <Chip onClick={() => goToCustomer(customer.id)}>
          {
            customerAvatarData
            ? <Avatar src={customerAvatarData.thumbnailLocation || customerAvatarData.location} />
            : <Avatar icon={<AccountIcon />} />
          }
          {customer.name}
        </Chip>
      </div>

      <ClaimToolbar entry={entry} />

      <EntryMainData
        entry={entry}
        customerTimezone={customer.timezone}
        className={classes.carouselContainer}
      />

      <FoodInformation
        entryId={entry.id}
        nutrientsSummary={entry.nutrientsSummary}
        eatenFoodsData={entry.eatenFoodsData}
      />

      {shouldShowAnalyticsInformation &&
        <AnalyticsInformation activeTrafficLight={entry.entryAnalytics.trafficLightEvaluation} />}

      <Divider />

      <Chat entryId={entry.id} writable={false} />
    </div>
  );
}

ReadOnlyEntryReview.propTypes = {
  entry: customPropTypes.entry.isRequired,
  customer: customPropTypes.customer.isRequired,
  goToCustomer: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    carouselContainer: PropTypes.string.isRequired,
    headerActions: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(ReadOnlyEntryReview);
