import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Field } from 'redux-form';
import TrafficLight from 'react-trafficlight';
import classNames from 'classnames';
import { textCentered } from '../../../common/theme';

const styles = {
  trafficLightContainer: {
    transform: 'rotate(-90deg)',
    display: 'inline-block',
    padding: '2px',
    marginTop: '-40px',
  },
  analyticsItem: {
    padding: '5px',
  },
  hiddenElement: {
    display: 'none',
  },
  disabled: {
    cursor: 'not-allowed',
  },
  textCentered,
};

class AnalyticsForm extends Component {
  constructor(props) {
    super(props);
    const trafficLight = { red: false, yellow: false, green: false };

    const initialTrafficLight = props.initialValues.trafficLightEvaluation;
    if (initialTrafficLight) {
      trafficLight[initialTrafficLight] = true;
    }

    this.state = { trafficLight };
  }

  onClickTrafficLight(value) {
    if (this.props.disabled) {
      return;
    }
    this.props.change('trafficLightEvaluation', value);
    this.setState({
      trafficLight: { red: false, yellow: false, green: false, [value]: true },
    });
  }

  render() {
    const { classes, disabled } = this.props;
    return (
      <div className={classes.textCentered}>
        <div className={classes.analyticsItem}>
          <h2>Traffic Light Evaluation</h2>
          <div
            className={classNames({
              [classes.trafficLightContainer]: true,
              [classes.disabled]: disabled,
            })}
          >
            <TrafficLight
              onRedClick={() => this.onClickTrafficLight('red')}
              onYellowClick={() => this.onClickTrafficLight('yellow')}
              onGreenClick={() => this.onClickTrafficLight('green')}
              RedOn={this.state.trafficLight.red}
              YellowOn={this.state.trafficLight.yellow}
              GreenOn={this.state.trafficLight.green}
              Size={50}
            />
          </div>
          <form>
            <Field
              name="trafficLightEvaluation"
              className={classes.hiddenElement}
              component="input"
              disabled
            />
          </form>
        </div>
      </div>
    );
  }
}

AnalyticsForm.propTypes = {
  initialValues: PropTypes.shape({
    trafficLightEvaluation: PropTypes.string,
  }),
  change: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
    trafficLightContainer: PropTypes.string.isRequired,
    analyticsItem: PropTypes.string.isRequired,
    hiddenElement: PropTypes.string.isRequired,
  }).isRequired,
  disabled: PropTypes.bool,
};

AnalyticsForm.defaultProps = {
  initialValues: {},
  disabled: false,
};

export default injectSheet(styles)(AnalyticsForm);
