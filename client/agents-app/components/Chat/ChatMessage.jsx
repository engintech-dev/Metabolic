import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import classNames from 'classnames';
import moment from 'moment-timezone';
import { blue700 } from 'material-ui/styles/colors';
import Avatar from 'material-ui/Avatar';
import get from 'lodash/get';
import isEmoji from 'is-emoji';
import customPropTypes from '../../prop-types';

const styles = {
  wrapper: {
    borderRadius: '5px',
    margin: '10px',
    padding: '10px',
  },
  agentWrapper: {
    extend: 'wrapper',
    backgroundColor: '#e9eaea',
    marginRight: '100px',
  },
  customerWrapper: {
    extend: 'wrapper',
    backgroundColor: '#45acf7',
    marginLeft: '100px',
    color: 'white',
  },
  ownerWrapper: {
    extend: 'agentWrapper',
    backgroundColor: '#e2e2e2',
  },
  sourceWrapper: {
    extend: 'wrapper',
    backgroundColor: blue700,
    marginTop: '33px',
    marginBottom: '33px',
    color: 'white',
    textAlign: 'center',
  },
  agentImages: {
    display: 'flex',
    justifyContent: 'flex-start',
  },
  customerImages: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  baseTime: {
    marginTop: '10px',
  },
  agentTime: {
    extend: 'baseTime',
    textAlign: 'right',
  },
  customerTime: {
    extend: 'baseTime',
    color: 'white',
    textAlign: 'left',
  },
  sourceTime: {
    color: 'white',
    textAlign: 'center',
  },
  avatar: {
    cursor: 'pointer',
  },
  displayedText: {
    whiteSpace: 'pre-wrap',
  },
  sentBy: {
    fontSize: '12px',
    fontStyle: 'italic',
  },
  oneEmojiText: {
    fontSize: 50,
  },
  twoEmojisText: {
    fontSize: 40,
  },
  multiEmojisText: {
    fontSize: 30,
  },
};

const renderImages = (images, fromAgent, classes, handleOpenLightBox) => {
  if (!images) {
    return null;
  }

  const imageLocations = images.map(image => image.location);
  const messageImages = images.map((imageData, index) => (
    <Avatar
      key={imageData.id}
      src={imageData.thumbnailLocation || imageData.location}
      size={64}
      onClick={() => handleOpenLightBox(imageLocations, index)}
      style={styles.avatar}
    />
  ));

  const className = fromAgent ? classes.agentImages : classes.customerImages;
  return <div className={className}>{messageImages}</div>;
};

function renderSourceTitle(sourceType) {
  if (!sourceType) {
    return null;
  }

  const title = sourceType.replace(/([a-z])([A-Z])/g, '$1 $2');

  return <h3>{title}</h3>;
}

const ChatMessage = ({
  message: {
    text,
    source,
    sourceType,
    createdAt,
    imagesData,
    fromAgent,
    authorId,
    author,
  },
  handleOpenLightBox,
  classes,
  currentUserId,
}) => {
  const displayedText = text || get(source, 'message');

  const appliedClasses = {
    agent: authorId !== currentUserId && fromAgent && !sourceType,
    customer: !fromAgent && !sourceType,
    source: sourceType,
    owner: authorId === currentUserId && fromAgent && !sourceType,
  };

  const wrapperClass = classNames({
    [classes.agentWrapper]: appliedClasses.agent,
    [classes.customerWrapper]: appliedClasses.customer,
    [classes.sourceWrapper]: appliedClasses.source,
    [classes.ownerWrapper]: appliedClasses.owner,
  });

  const timeClass = classNames({
    [classes.agentTime]: appliedClasses.agent,
    [classes.customerTime]: appliedClasses.customer,
    [classes.sourceTime]: appliedClasses.source,
  });

  const emojiCount = isEmoji.getAllEmojiCount(displayedText);
  const textClass = classNames({
    [classes.oneEmojiText]: emojiCount === 1,
    [classes.twoEmojisText]: emojiCount === 2,
    [classes.multiEmojisText]: emojiCount > 2,
    [classes.displayedText]: true,
  });

  return (
    <div>
      <div className={wrapperClass}>
        {renderImages(imagesData, fromAgent, classes, handleOpenLightBox)}
        {renderSourceTitle(sourceType)}
        {displayedText && <div className={textClass}>{displayedText}</div>}
        <div className={timeClass}>
          {moment(createdAt).fromNow()}
          {displayedText && !sourceType && authorId !== currentUserId && fromAgent &&
            <div className={classes.sentBy}>Sent by {author.name || author.email}</div>}
        </div>
      </div>
    </div>
  );
};

ChatMessage.propTypes = {
  message: customPropTypes.message.isRequired,
  classes: PropTypes.shape({
    wrapper: PropTypes.string.isRequired,
    agentWrapper: PropTypes.string.isRequired,
    customerWrapper: PropTypes.string.isRequired,
    sourceWrapper: PropTypes.string.isRequired,
    agentImages: PropTypes.string.isRequired,
    customerImages: PropTypes.string.isRequired,
    agentTime: PropTypes.string.isRequired,
    customerTime: PropTypes.string.isRequired,
    sourceTime: PropTypes.string.isRequired,
    displayedText: PropTypes.string.isRequired,
    sentBy: PropTypes.string.isRequired,
    oneEmojiText: PropTypes.string.isRequired,
    twoEmojisText: PropTypes.string.isRequired,
    multiEmojisText: PropTypes.string.isRequired,
  }).isRequired,
  handleOpenLightBox: PropTypes.func.isRequired,
  currentUserId: PropTypes.string.isRequired,
};

export default injectSheet(styles)(ChatMessage);
