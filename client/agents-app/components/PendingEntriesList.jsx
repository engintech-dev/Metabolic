import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'material-ui/List';
import EntryListItem from './EntryListItem';
import customPropTypes from '../prop-types';

function renderListItems(pendingEntries, entryBeingClaimed, claimEntry) {
  return pendingEntries.map(entry => (
    <EntryListItem
      key={entry.id}
      entry={entry}
      entryBeingClaimed={entryBeingClaimed}
      onClick={() => claimEntry(entry.id)}
    />
  ));
}

const PendingEntriesList = ({ pendingEntries, entryBeingClaimed, claimEntry }) => (
  <List>
    {renderListItems(pendingEntries, entryBeingClaimed, claimEntry)}
  </List>
);

PendingEntriesList.propTypes = {
  pendingEntries: customPropTypes.entrySummariesOrderedSet.isRequired,
  claimEntry: PropTypes.func.isRequired,
  entryBeingClaimed: PropTypes.string,
};

PendingEntriesList.defaultProps = {
  entryBeingClaimed: undefined,
};

export default PendingEntriesList;
