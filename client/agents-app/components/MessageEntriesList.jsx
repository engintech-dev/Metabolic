import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { List } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import EntryListItem from './EntryListItem';
import customPropTypes from '../prop-types';
import EntryFilterByAgentToolbar from '../containers/EntryFilterByAgentToolbar';

class MessageEntriesList extends PureComponent {
  renderListItems(entries) {
    return entries.map(entry => (
      <EntryListItem
        key={entry.id}
        entry={entry}
        extendedInfo={this.props.superAgentMode}
        onClick={() => (this.props.currentUserId === entry.agentId
          ? this.props.goToEntryReview(entry.id)
          : this.props.goToReadOnlyEntry(entry.id)
        )}
      />
    ));
  }

  render() {
    return (
      <List>
        {this.props.superAgentMode && <EntryFilterByAgentToolbar />}
        <Subheader>Active</Subheader>
        {this.renderListItems(this.props.activeEntries)}
        <Subheader>Waiting</Subheader>
        {this.renderListItems(this.props.waitingEntries)}
        <Subheader>Inactive</Subheader>
        {this.renderListItems(this.props.inactiveEntries)}
      </List>
    );
  }
}

MessageEntriesList.propTypes = {
  activeEntries: customPropTypes.entrySummariesOrderedSet.isRequired,
  waitingEntries: customPropTypes.entrySummariesOrderedSet.isRequired,
  inactiveEntries: customPropTypes.entrySummariesOrderedSet.isRequired,
  currentUserId: PropTypes.string.isRequired,
  superAgentMode: PropTypes.bool,
  goToReadOnlyEntry: PropTypes.func.isRequired,
  goToEntryReview: PropTypes.func.isRequired,
};

MessageEntriesList.defaultProps = {
  superAgentMode: false,
};

export default MessageEntriesList;
