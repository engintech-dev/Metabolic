import React from 'react';
import injectSheet from 'react-jss';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import uuid from 'uuid/v4';
import theme, { textCentered } from '../../common/theme';
import customPropTypes from '../prop-types';

const styles = {
  error: {
    fontWeight: 'bold',
    color: theme.textField.errorColor,
  },
  textCentered,
};

function renderOriginalText(originalText, spellingErrors, classes) {
  const sentence = [];
  let currentCharIndex = 0;

  spellingErrors.forEach((error) => {
    if (error.offset + error.length <= currentCharIndex) {
      return;
    }

    if (currentCharIndex < error.offset) {
      sentence.push(<span key={uuid()}>{originalText.slice(currentCharIndex, error.offset)}</span>);
    }

    sentence.push(<span key={uuid()} className={classes.error}>{error.bad}</span>);
    currentCharIndex = error.offset + error.length;
  });

  if (currentCharIndex < originalText.length - 1) {
    sentence.push(<span key={uuid()}>{originalText.slice(currentCharIndex)}</span>);
  }

  return <p className={classes.textCentered}>{sentence}</p>;
}

const renderSpellingErrorRow = (error, classes) => (
  <TableRow key={uuid()}>
    <TableRowColumn><p className={classes.error}>{error.bad}</p></TableRowColumn>
    <TableRowColumn>
      {error.better.map(suggestion => <p key={uuid()}>{suggestion}</p>)}
    </TableRowColumn>
  </TableRow>
);

const SpellingErrors = ({ originalText, spellingErrors, classes }) => (
  <div>
    {renderOriginalText(originalText, spellingErrors, classes)}
    <Table selectable={false}>
      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
        <TableRow>
          <TableHeaderColumn>Problem</TableHeaderColumn>
          <TableHeaderColumn>Suggestion</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {spellingErrors.map(error => renderSpellingErrorRow(error, classes))}
      </TableBody>
    </Table>
  </div>
);

SpellingErrors.propTypes = {
  originalText: PropTypes.string.isRequired,
  spellingErrors: customPropTypes.spellingErrors,
  classes: PropTypes.shape({
    error: PropTypes.string.isRequired,
    textCentered: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(SpellingErrors);
