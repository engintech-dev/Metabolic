import React from 'react';
import PropTypes from 'prop-types';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress';
import customPropTypes from '../prop-types';

function renderAgentItems(agentList) {
  return agentList.map(agent => (
    <MenuItem
      key={agent.id}
      value={agent.id}
      primaryText={agent.name}
    />
  ));
}

function renderAgentsDropdown(
  agentsList,
  areAgentsLoaded,
  selectedAgentId,
  changeAgentFilterHandler,
) {
  if (areAgentsLoaded) {
    return (
      <DropDownMenu
        value={selectedAgentId}
        onChange={changeAgentFilterHandler}
      >
        <MenuItem key="0" value="" primaryText="All Agents" />
        {renderAgentItems(agentsList)}
      </DropDownMenu>
    );
  }
  return <CircularProgress />;
}

const EntryFilterByAgentToolbar = (props) => {
  const { agentsList, areAgentsLoaded, selectedAgentId, changeAgentFilter } = props;
  const changeAgentFilterHandler = (event, key, value) => changeAgentFilter(value);

  return (
    <Toolbar>
      <ToolbarGroup firstChild>
        <ToolbarTitle text="Filter by:" />
        {renderAgentsDropdown(agentsList, areAgentsLoaded,
          selectedAgentId, changeAgentFilterHandler)}
      </ToolbarGroup>
    </Toolbar>
  );
};

EntryFilterByAgentToolbar.propTypes = {
  changeAgentFilter: PropTypes.func.isRequired,
  agentsList: customPropTypes.agentSummariesList.isRequired,
  selectedAgentId: PropTypes.string.isRequired,
  areAgentsLoaded: PropTypes.bool.isRequired,
};

export default EntryFilterByAgentToolbar;
