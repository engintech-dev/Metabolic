import React from 'react';
import injectSheet from 'react-jss';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  TextField,
} from 'redux-form-material-ui';
import Notification from '../../common/components/Notification';
import FormActions from '../../common/components/FormActions';
import SubmitButton from '../../common/components/SubmitButton';
import { required, repeatPassword } from '../../common/forms/validators';
import customPropTypes from '../prop-types';
import { textCentered } from '../../common/theme';

const styles = {
  container: textCentered,
};

function ChangePassword(props) {
  const { handleSubmit, submitting, error } = props;
  let errorMessage;
  if (error) {
    errorMessage = error.getHumanMessage();
  }
  return (
    <div className={props.classes.container}>
      <Notification type="error" message={errorMessage} />
      <h1>Change your password</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <Field
            name="currentPassword" component={TextField}
            type="password" hintText="******" floatingLabelText="Your current password"
            disabled={submitting} validate={required}
          />
        </div>
        <div>
          <Field
            name="password" component={TextField}
            type="password" hintText="******" floatingLabelText="Your new password"
            disabled={submitting} validate={required}
          />
        </div>
        <div>
          <Field
            name="repeatPassword" component={TextField}
            type="password" hintText="******" floatingLabelText="Repeat your new password"
            disabled={submitting} validate={repeatPassword}
          />
        </div>
        <FormActions>
          <SubmitButton
            label="Change password" submitting={submitting} disabled={submitting}
          />
        </FormActions>
      </form>
    </div>
  );
}

export default injectSheet(styles)(ChangePassword);

ChangePassword.propTypes = {
  error: customPropTypes.error,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
  }).isRequired,
};

ChangePassword.defaultProps = {
  error: undefined,
};
