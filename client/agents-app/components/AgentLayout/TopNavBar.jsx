import React from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

const styles = {
  appBar: {
    zIndex: 1,
  },
  title: {
    cursor: 'pointer',
  },
};

export default function TopNavBar(props) {
  const superAgentOption = props.superAgentAvailable
    ? (
      <MenuItem
        primaryText="Super Agent Mode"
        onClick={props.toggleSuperAgentMode}
        checked={props.isSuperAgentModeEnabled}
      />)
    : undefined;

  const rightBtn = props.signedIn
    ? (<IconMenu
      iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
      targetOrigin={{ horizontal: 'right', vertical: 'top' }}
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
    >
      {superAgentOption}
      <MenuItem primaryText="Change Password" onClick={props.goToChangePassword} />
      <MenuItem primaryText="Sign Out" onClick={props.signOut} />
    </IconMenu>)
    : undefined;

  return (
    <AppBar
      style={styles.appBar}
      title="Metabolic"
      showMenuIconButton={false}
      iconElementRight={rightBtn}
      titleStyle={styles.title}
      onTitleTouchTap={props.goToHome}
    />
  );
}

TopNavBar.defaultProps = {
  signedIn: false,
  superAgentAvailable: false,
  isSuperAgentModeEnabled: false,
};

TopNavBar.propTypes = {
  signedIn: PropTypes.bool,
  signOut: PropTypes.func.isRequired,
  goToHome: PropTypes.func.isRequired,
  goToChangePassword: PropTypes.func.isRequired,
  toggleSuperAgentMode: PropTypes.func.isRequired,
  superAgentAvailable: PropTypes.bool,
  isSuperAgentModeEnabled: PropTypes.bool,
};
