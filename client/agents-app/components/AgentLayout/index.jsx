import React from 'react';
import PropTypes from 'prop-types';
import Snackbar from 'material-ui/Snackbar';
import LayoutPaper from './LayoutPaper';
import TopNavBar from '../../containers/AgentLayout/TopNavBar';

export default function Layout(props) {
  return (
    <div>
      <TopNavBar />
      <main>
        <LayoutPaper>{props.children}</LayoutPaper>
      </main>
      <Snackbar
        open={props.notification.open || false}
        message={props.notification.message || ''}
        autoHideDuration={props.notification.duration || 4000}
        onRequestClose={props.onNotificationClose}
      />
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  notification: PropTypes.shape({
    message: PropTypes.string,
    open: PropTypes.bool,
    duration: PropTypes.number,
  }).isRequired,
  onNotificationClose: PropTypes.func.isRequired,
};
