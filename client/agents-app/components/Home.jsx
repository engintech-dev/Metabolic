import React from 'react';
import PropTypes from 'prop-types';
import keyMirror from 'keymirror';
import { Tabs, Tab } from 'material-ui/Tabs';
import PendingEntriesList from '../containers/PendingEntriesList';
import MessageEntriesList from '../containers/MessageEntriesList';
import CustomersList from '../containers/CustomersList';
import MetabolicChatsList from '../containers/MetabolicChat/List';

export const tabValues = keyMirror({
  pending: null,
  messages: null,
  customers: null,
  metabolicChatList: null,
});

const Home = ({ selectedTab, handleTabChange, currentUserSuperAgentModeCapability }) => (
  <Tabs value={tabValues[selectedTab] || tabValues.pending} onChange={handleTabChange}>
    <Tab label="Pending Entries" value={tabValues.pending}>
      <PendingEntriesList />
    </Tab>
    <Tab label="Messages" value={tabValues.messages}>
      <MessageEntriesList />
    </Tab>
    <Tab label="Customers" value={tabValues.customers}>
      <CustomersList />
    </Tab>
    {currentUserSuperAgentModeCapability &&
      <Tab label="Metabolic Chats" value={tabValues.metabolicChatList}>
        <MetabolicChatsList />
      </Tab>}
  </Tabs>
);

Home.propTypes = {
  selectedTab: PropTypes.string.isRequired,
  handleTabChange: PropTypes.func.isRequired,
  currentUserSuperAgentModeCapability: PropTypes.bool.isRequired,
};

export default Home;
