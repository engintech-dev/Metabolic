import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar';
import customPropTypes from '../prop-types';

const ClaimToolbar = (props) => {
  const claimed = !!props.entry.agentId;
  const claimedByCurrentUser = props.entry.agentId === props.userId;
  const claimButtonText = 'Claim';
  const canClaim = (props.superAgentModeEnabled && !claimedByCurrentUser) || !claimed;
  let claimedText = 'Not claimed yet';

  if (claimed) {
    claimedText = claimedByCurrentUser ? 'Claimed by me' : `Claimed by ${props.entry.agentSummary.name}`;
  }

  return (
    <div>
      <Toolbar>
        <ToolbarGroup firstChild>
          <ToolbarTitle text={claimedText} />
        </ToolbarGroup>
        <ToolbarGroup lastChild>
          {canClaim &&
            <RaisedButton
              label={claimButtonText}
              onClick={() => props.claimEntry(props.entry.id)}
              primary
            />
          }
        </ToolbarGroup>
      </Toolbar>
      <br />
    </div>
  );
};

ClaimToolbar.propTypes = {
  entry: customPropTypes.entry.isRequired,
  userId: PropTypes.string.isRequired,
  claimEntry: PropTypes.func.isRequired,
  superAgentModeEnabled: PropTypes.bool.isRequired,
};

export default ClaimToolbar;
