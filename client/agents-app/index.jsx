import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';
import { useBasename } from 'history';
import { AppContainer } from 'react-hot-loader';
import configureStore from './store';
import initializePrimus from './primus';
import App from './app';
// required so agents.html is also copied to /dist by webpack
import indexHtml from './index.html'; // eslint-disable-line no-unused-vars

const store = configureStore();
initializePrimus();

const history = syncHistoryWithStore(
  useBasename(() => browserHistory)({ basename: '/agents' }),
  store,
);

ReactDOM.render(
  <AppContainer>
    <App store={store} history={history} />
  </AppContainer>,
  document.getElementById('root'),
);

if (module.hot) {
  module.hot.accept('./app', () => {
    const NewApp = require('./app').default; // eslint-disable-line global-require
    ReactDOM.render(
      <NewApp store={store} history={history} />,
      document.getElementById('root'),
    );
  });
}
