import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import CustomerStats from '../../components/CustomerStats';
import { getCustomer, loadCustomer } from '../../store/ducks/data/customers';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import routeHelper from '../../routes-helpers';

function promiseFn(props) {
  return {
    customerLoading: props.customer
      ? Promise.resolve(props.customer)
      : props.loadCustomer(props.params.customerId),
  };
}

function mapStateToProps(state, ownProps) {
  const props = { customerId: ownProps.params.customerId };
  return {
    customer: getCustomer(state, props),
    tab: ownProps.params.statsSection,
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    handleTabChange: tab =>
      dispatch(routeHelper.goToCustomer({ id: ownProps.params.customerId, statsSection: tab })),
    goBack: () => dispatch(goBack()),
    loadCustomer: userId => dispatch(loadCustomer(userId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, CustomerStats),
);
