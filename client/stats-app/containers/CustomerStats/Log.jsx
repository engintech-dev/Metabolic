import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../../common/components/Loading';
import Log from '../../components/CustomerStats/Log';
import {
  getCustomerEntriesLog,
  getCustomer,
  loadCustomerEntriesLog,
} from '../../store/ducks/data/customers';

class LogContainer extends Component {
  constructor(props) {
    super(props);
    this.reloadEntriesLog = this.reloadEntriesLog.bind(this);
  }

  componentWillMount() {
    this.reloadEntriesLog();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.customerId !== nextProps.customerId && !nextProps.entriesLog) {
      this.props.loadCustomerEntriesLog(nextProps.customerId);
    }
  }

  reloadEntriesLog() {
    return this.props.loadCustomerEntriesLog(this.props.customerId);
  }

  render() {
    return this.props.entriesLog
      ? <Log {...this.props} reloadEntriesLog={this.reloadEntriesLog} />
      : <Loading />;
  }
}

LogContainer.propTypes = {
  customerId: PropTypes.string.isRequired,
  loadCustomerEntriesLog: PropTypes.func.isRequired,
  entriesLog: PropTypes.shape({}),
};

LogContainer.defaultProps = {
  entriesLog: undefined,
};

function mapStateToProps(state, ownProps) {
  return {
    entriesLog: getCustomerEntriesLog(state, ownProps),
    customer: getCustomer(state, ownProps),
  };
}

const mapDispatchToProps = {
  loadCustomerEntriesLog,
};

export default connect(mapStateToProps, mapDispatchToProps)(LogContainer);
