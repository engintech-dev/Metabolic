import { connect } from 'react-redux';
import OneChangePeriod from '../../../components/CustomerStats/OneChangePeriod/Current';
import {
  loadCustomerCurrentOneChangePeriod,
  getCustomerOneChangePeriod,
} from '../../../store/ducks/data/one-change-periods';
import PromiseLoadingWrapper from '../../../../common/hoc/PromiseLoadingWrapper';

function promiseFn(props) {
  return {
    oneChangePeriodLoading: props.loadCustomerCurrentOneChangePeriod(props.customerId),
  };
}

function mapStateToProps(state, ownProps) {
  return {
    oneChangePeriod: getCustomerOneChangePeriod(state, ownProps),
  };
}

const mapDispatchToProps = {
  loadCustomerCurrentOneChangePeriod,
};

export default connect(mapStateToProps, mapDispatchToProps)(
 PromiseLoadingWrapper(promiseFn, OneChangePeriod),
);
