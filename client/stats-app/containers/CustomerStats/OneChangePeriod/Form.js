import { connect } from 'react-redux';
import { reduxForm, getFormValues } from 'redux-form';
import get from 'lodash/get';
import { replaceCurrent } from '../../../store/ducks/data/one-change-periods';
import Form from '../../../components/CustomerStats/OneChangePeriod/Form';

const FORM_NAME = 'oneChangePeriod';

function mapStateToProps(state, ownProps) {
  const { oneChangePeriod: { oneChange, customerId } } = ownProps;
  return {
    initialValues: { customerId, oneChange, shouldUpdate: false },
    shouldUpdate: get(getFormValues(FORM_NAME)(state), 'shouldUpdate', false),
  };
}

const mapDispatchToProps = {
  replaceCurrent,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(reduxForm({
  form: FORM_NAME,
  onSubmit: (values, dispatch, props) => {
    const { customerId, oneChange, shouldUpdate } = values;
    return props.replaceCurrent(customerId, oneChange, shouldUpdate)
      .then(() => props.onSuccessSubmit());
  },
})(Form));
