import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../../common/components/Loading';
import Week from '../../components/CustomerStats/Week';
import {
  getCustomerWeekStats,
  loadCustomerWeekStats,
} from '../../store/ducks/data/customers';

class WeekContainer extends Component {
  constructor(props) {
    super(props);
    this.reloadWeekStats = this.reloadWeekStats.bind(this);
  }

  componentWillMount() {
    this.reloadWeekStats();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.customerId !== nextProps.customerId && !nextProps.weekStats) {
      this.props.loadCustomerWeekStats(nextProps.customerId);
    }
  }

  reloadWeekStats() {
    return this.props.loadCustomerWeekStats(this.props.customerId);
  }

  render() {
    return this.props.weekStats
      ? <Week {...this.props} reloadWeekStats={this.reloadWeekStats} />
      : <Loading />;
  }
}

WeekContainer.propTypes = {
  customerId: PropTypes.string.isRequired,
  loadCustomerWeekStats: PropTypes.func.isRequired,
  weekStats: PropTypes.shape({}),
};

WeekContainer.defaultProps = {
  weekStats: undefined,
};

function mapStateToProps(state, ownProps) {
  return {
    weekStats: getCustomerWeekStats(state, ownProps),
  };
}

const mapDispatchToProps = {
  loadCustomerWeekStats,
};

export default connect(mapStateToProps, mapDispatchToProps)(WeekContainer);
