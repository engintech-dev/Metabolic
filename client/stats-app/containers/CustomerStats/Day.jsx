import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../../../common/components/Loading';
import Day from '../../components/CustomerStats/Day';
import {
  getCustomerDayStats,
  loadCustomerDayStats,
} from '../../store/ducks/data/customers';

class DayContainer extends Component {
  constructor(props) {
    super(props);
    this.reloadDayStats = this.reloadDayStats.bind(this);
  }

  componentWillMount() {
    this.reloadDayStats();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.customerId !== nextProps.customerId && !nextProps.dayStats) {
      this.props.loadCustomerDayStats(nextProps.customerId);
    }
  }

  reloadDayStats() {
    return this.props.loadCustomerDayStats(this.props.customerId);
  }

  render() {
    return this.props.dayStats
      ? <Day {...this.props} reloadDayStats={this.reloadDayStats} />
      : <Loading />;
  }
}

DayContainer.propTypes = {
  customerId: PropTypes.string.isRequired,
  loadCustomerDayStats: PropTypes.func.isRequired,
  dayStats: PropTypes.shape({}),
};

DayContainer.defaultProps = {
  dayStats: undefined,
};

function mapStateToProps(state, ownProps) {
  return {
    dayStats: getCustomerDayStats(state, ownProps),
  };
}

const mapDispatchToProps = {
  loadCustomerDayStats,
};

export default connect(mapStateToProps, mapDispatchToProps)(DayContainer);
