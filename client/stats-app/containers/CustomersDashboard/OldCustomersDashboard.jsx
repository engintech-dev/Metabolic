import { connect } from 'react-redux';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import OldCustomersDashboard, { tabValues } from '../../components/CustomersDashboard/OldCustomersDashboard';
import { loadCustomers } from '../../store/ducks/data/customers';
import { toISODateFormat } from '../../../common/lib/datetime';
import routeHelper from '../../routes-helpers';

function promiseFn(props) {
  return {
    customersLoading: props.loadCustomers(),
  };
}

function mapStateToProps(state, ownProps) {
  return {
    activeTab: ownProps.location.query.tab,
    currentDate: ownProps.location.query.date,
  };
}

const mapDispatchToProps = {
  loadCustomers,
  goToCustomersTrackingTab: routeHelper.goToCustomersTracking,
};

function mergeProps(state, actions, ownProps) {
  return {
    ...state,
    ...actions,
    onTabChange: (tabValue) => {
      const { location: { query } } = ownProps;

      switch (tabValue) {
        case tabValues.overview:
          return actions.goToCustomersTrackingTab({
            date: toISODateFormat(query.date),
            tab: tabValue,
          });
        default:
          return actions.goToCustomersTrackingTab({ tab: tabValue });
      }
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  PromiseLoadingWrapper(promiseFn, OldCustomersDashboard),
);
