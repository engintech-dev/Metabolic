import { connect } from 'react-redux';
import { loadCustomersWeeklySummaries, getCustomersWeeklySummaries, updateWeeklyCustomerSummaryNotes } from '../../store/ducks/data/summaries';
import PromiseLoadingWrapper from '../../../common/hoc/PromiseLoadingWrapper';
import CustomersWeeklySummary from '../../components/CustomersDashboard/CustomersWeeklySummary';

const promiseFn = props => ({
  customersWeeklySummariesLoading: props.loadCustomersWeeklySummaries(),
});

function mapStateToProps(state) {
  return {
    customersWeeklySummaries: getCustomersWeeklySummaries(state),
  };
}

const mapDispatchToProps = {
  loadCustomersWeeklySummaries,
  updateNotes: updateWeeklyCustomerSummaryNotes,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, CustomersWeeklySummary),
);
