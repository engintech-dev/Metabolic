import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import Tracking from '../../components/CustomersDashboard/Tracking';
import routeHelper from '../../routes-helpers';

function mapStateToProps(state, ownProps) {
  return {
    activeTab: ownProps.location.query.tab,
  };
}


function mapDispatchToProps(dispatch) {
  return {
    onTabChange: tab => dispatch(routeHelper.goToCustomersTracking({ tab })),
    goBack: () => dispatch(goBack()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Tracking);
