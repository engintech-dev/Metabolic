import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Loading from '../../../common/components/Loading';

class NotesDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notesChanged: false,
      editingNotes: false,
      sendingNotes: false,
      notes: props.initialNotes,
    };
    this.changeNotes = this.changeNotes.bind(this);
    this.saveNotes = this.saveNotes.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.initialNotes !== this.props.initialNotes) {
      this.setState({ notes: nextProps.initialNotes });
    }
  }

  changeNotes(newNotes) {
    this.setState({ notes: newNotes, notesChanged: true });
  }

  saveNotes() {
    this.setState({ sendingNotes: true });
    return this.props.updateNotes(this.props.noteableId, this.state.notes)
      .then(() => {
        this.setState({ editingNotes: false, notesChanged: false, sendingNotes: false });
        this.props.onClose();
      })
      .catch(() => {
        this.setState({ sendingNotes: false });
      });
  }

  renderDialogBody() {
    if (this.state.sendingNotes) {
      return <Loading />;
    }
    return (
      <TextField
        hintText="Write your notes"
        multiLine
        autoFocus
        rows={2}
        rowsMax={4}
        onChange={(event, newValue) => this.changeNotes(newValue)}
        value={this.state.notes}
      />
    );
  }

  render() {
    const actions = [
      <RaisedButton
        label="Update"
        disabled={!this.state.notesChanged || this.state.sendingNotes}
        primary
        onClick={this.saveNotes}
      />,
      <FlatButton
        label="Close"
        primary
        onClick={this.props.onClose}
      />,
    ];
    return (
      <Dialog
        title={this.props.title}
        open={this.props.open}
        actions={actions}
        onRequestClose={this.props.onClose}
        autoScrollBodyContent
        modal
      >
        {this.renderDialogBody()}
      </Dialog>
    );
  }
}

NotesDialog.propTypes = {
  noteableId: PropTypes.string.isRequired,
  updateNotes: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  initialNotes: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
};

export default NotesDialog;
