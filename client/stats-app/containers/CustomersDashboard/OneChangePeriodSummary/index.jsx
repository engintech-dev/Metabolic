import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Loading from '../../../../common/components/Loading';
import {
  loadOneChangePeriodSummariesData,
  getOneChangePeriodSummaries,
  getOneChangePeriodSummariesLoading,
  updateOneChangePeriodSummaryNotes,
} from '../../../store/ducks/data/summaries';
import OneChangePeriodSummary from '../../../components/CustomersDashboard/OneChangePeriodSummary';

class OneChangePeriodContainer extends Component {
  constructor(props) {
    super(props);
    this.loadOneChangePeriodSummariesData = this.loadOneChangePeriodSummariesData.bind(this);
  }

  componentWillMount() {
    this.loadOneChangePeriodSummariesData();
  }

  loadOneChangePeriodSummariesData() {
    return this.props.loadOneChangePeriodSummariesData();
  }

  render() {
    if (this.props.isLoading) {
      return <Loading />;
    }
    return (
      <OneChangePeriodSummary {...this.props} />
    );
  }
}

OneChangePeriodContainer.propTypes = {
  loadOneChangePeriodSummariesData: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    oneChangePeriodSummariesData: getOneChangePeriodSummaries(state, ownProps),
    isLoading: getOneChangePeriodSummariesLoading(state, ownProps),
  };
}

const mapDispatchToProps = {
  loadOneChangePeriodSummariesData,
  updateNotes: updateOneChangePeriodSummaryNotes,
};

export default connect(mapStateToProps, mapDispatchToProps)(OneChangePeriodContainer);
