import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import routeHelper from '../../../routes-helpers';
import { loadCustomers, getCustomerEntitiesList } from '../../../store/ducks/data/customers';
import Analytics from '../../../components/CustomersDashboard/Analytics';
import PromiseLoadingWrapper from '../../../../common/hoc/PromiseLoadingWrapper';

function promiseFn(props) {
  return {
    customersLoading: props.loadCustomers(),
  };
}

function mapStateToProps(state) {
  return {
    customers: getCustomerEntitiesList(state),
  };
}

const mapDispatchToProps = {
  loadCustomers,
  goToCustomer: routeHelper.goToCustomer,
  goBack,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, Analytics),
);
