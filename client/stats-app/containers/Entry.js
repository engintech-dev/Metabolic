import { connect } from 'react-redux';
import PromiseLoadingWrapper from '../../common/hoc/PromiseLoadingWrapper';
import { getEntry, getEntryFromApi } from '../store/ducks/data/entries';
import Entry from '../components/Entry';

function promiseFn(props) {
  return {
    entryLoading: props.getEntryFromApi(props.entryId),
  };
}

function mapStateToProps(state, ownProps) {
  return {
    entry: getEntry(state, ownProps),
  };
}

const mapDispatchToProps = {
  getEntryFromApi,
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PromiseLoadingWrapper(promiseFn, Entry),
);
