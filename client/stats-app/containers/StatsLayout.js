import { connect } from 'react-redux';
import { signOut } from '../store/ducks/session';
import routeHelper from '../routes-helpers';
import StatsLayout from '../components/StatsLayout';
import { getNotification } from '../../common/store/selectors/ui/notification';
import { hideNotification } from '../../common/store/actions/ui/notification';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps.route.componentProps,
  notification: getNotification(state),
});

const mapDispatchToProps = {
  signOut,
  goToDashboard: () => routeHelper.goToDashboard(),
  onNotificationClose: hideNotification,
};

export default connect(mapStateToProps, mapDispatchToProps)(StatsLayout);
