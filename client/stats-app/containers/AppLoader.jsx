import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { assignCurrentUser } from '../store/ducks/current-user';
import metabolicTheme from '../../common/theme';
import Loading from '../../common/components/Loading';

class AppLoaderContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  componentWillMount() {
    this.setState({ loading: true });
    this.props.assignCurrentUser()
      .then(() => this.setState({ loading: false }));
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={metabolicTheme}>
        {this.state.loading ? <Loading /> : this.props.children }
      </MuiThemeProvider>
    );
  }
}

AppLoaderContainer.propTypes = {
  assignCurrentUser: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

const mapDispatchToProps = {
  assignCurrentUser,
};

export default connect(null, mapDispatchToProps)(AppLoaderContainer);
