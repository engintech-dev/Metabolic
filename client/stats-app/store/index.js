import { createStore, applyMiddleware, compose } from 'redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import rootReducer from './ducks';
import { REHYDRATE } from './ducks/rehydration';
import storageService from '../../common/services/storage';
import apiService from '../services/api';

let store;

/* eslint-disable no-underscore-dangle, global-require */
export default function configureStore() {
  if (store) {
    return store;
  }
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const middleware = applyMiddleware(routerMiddleware(browserHistory), thunk);
  store = createStore(
    rootReducer,
    composeEnhancers(middleware),
  );
  store.subscribe(() => apiService(store.getState()));
  store.dispatch({ type: REHYDRATE, payload: storageService.get('stats-redux-store') });
  if (module.hot) {
    module.hot.accept('./ducks', () => {
      const nextReducer = require('./ducks').default;
      store.replaceReducer(nextReducer);
    });
  }
  return store;
}
