import { combineReducers } from 'redux';
import notifications from '../../../../common/store/reducers/ui/notification';
import redirections from './redirections';

export default combineReducers({
  notifications,
  redirections,
});
