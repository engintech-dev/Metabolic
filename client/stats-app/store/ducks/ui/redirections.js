import reducer from '../../../../common/store/reducers/ui/redirections';

export default reducer;

export {
  clearSignInRedirection,
  setSignInRedirection,
} from '../../../../common/store/actions/ui/redirections';
