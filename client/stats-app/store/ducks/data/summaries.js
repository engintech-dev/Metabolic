import { Map } from 'immutable';
import { createSelector } from 'reselect';
import moment from 'moment';
import get from 'lodash/get';
import set from 'lodash/set';
import { showNotification } from '../../../../common/store/actions/ui/notification';
import apiService from '../../../services/api';
import { getData } from './common';

const ONE_CHANGE_PERIOD_SUMMARIES_DATA = 'oneChangePeriodSummariesData';
const ONE_CHANGE_PERIOD_SUMMARIES_DATA_LOADING = `${ONE_CHANGE_PERIOD_SUMMARIES_DATA}Loading`;
const CUSTOMER_DAILY_SUMMARIES_DATA = 'customerDailySummaries';
const CUSTOMER_WEEKLY_SUMMARIES_DATA = 'customerWeeklySummaries';

const INITIAL_STATE = new Map({
  [ONE_CHANGE_PERIOD_SUMMARIES_DATA]: [],
  [ONE_CHANGE_PERIOD_SUMMARIES_DATA_LOADING]: false,
  [CUSTOMER_WEEKLY_SUMMARIES_DATA]: [],
  [CUSTOMER_DAILY_SUMMARIES_DATA]: [],
});

const SET_ONE_CHANGE_PERIOD_SUMMARIES_DATA = `summaries/${ONE_CHANGE_PERIOD_SUMMARIES_DATA}/SET`;
const SET_ONE_CHANGE_PERIOD_SUMMARIES_DATA_LOADING = `summaries/${ONE_CHANGE_PERIOD_SUMMARIES_DATA}/loading/SET`;
const SET_ONE_CHANGE_PERIOD_SUMMARY = 'summaries/oneChangePeriodSummary/SET';
const SET_CUSTOMERS_DAILY_SUMMARY = 'summaries/dailyCustomerSummary/SET';
const SET_WEEKLY_CUSTOMER_SUMMARY = 'summaries/weeklyCustomerSummary/SET';
const SET_CUSTOMERS_DAILY_SUMMARIES = 'summaries/customers/daily/SET';
const SET_CUSTOMERS_WEEKLY_SUMMARIES = 'summaries/customers/weekly/SET';

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_ONE_CHANGE_PERIOD_SUMMARIES_DATA_LOADING:
      return state.set(ONE_CHANGE_PERIOD_SUMMARIES_DATA_LOADING, true);
    case SET_ONE_CHANGE_PERIOD_SUMMARIES_DATA:
      return state
        .set(ONE_CHANGE_PERIOD_SUMMARIES_DATA, action.payload)
        .set(ONE_CHANGE_PERIOD_SUMMARIES_DATA_LOADING, false);
    case SET_CUSTOMERS_WEEKLY_SUMMARIES:
      return state.set(CUSTOMER_WEEKLY_SUMMARIES_DATA, action.payload);
    case SET_CUSTOMERS_DAILY_SUMMARIES:
      return state.set(CUSTOMER_DAILY_SUMMARIES_DATA, action.payload);
    case SET_CUSTOMERS_DAILY_SUMMARY: {
      const newSummary = action.payload;
      const summaries = state.get(CUSTOMER_DAILY_SUMMARIES_DATA);
      const summaryDataIndex = summaries.findIndex(data =>
        data.customer.id === newSummary.customerId,
      );
      return state.setIn(
        CUSTOMER_DAILY_SUMMARIES_DATA,
        set(summaries, [summaryDataIndex, 'dailySummaries', newSummary.day], newSummary),
      );
    }
    case SET_WEEKLY_CUSTOMER_SUMMARY: {
      const newSummary = action.payload;
      const day = moment.utc(newSummary.startedAt).format('YYYY-MM-DD');
      const summaries = state.get(CUSTOMER_WEEKLY_SUMMARIES_DATA);
      const summaryDataIndex = summaries.findIndex(data =>
        data.customer.id === newSummary.customerId,
      );
      return state.setIn(
        CUSTOMER_WEEKLY_SUMMARIES_DATA,
        set(summaries, [summaryDataIndex, 'weeklySummaries', day], newSummary),
      );
    }
    case SET_ONE_CHANGE_PERIOD_SUMMARY: {
      const newSummary = action.payload;
      const summaries = state.get(ONE_CHANGE_PERIOD_SUMMARIES_DATA);
      const summaryDataIndex = summaries.findIndex(data =>
        data.customer.id === newSummary.customerId,
      );
      const summaryIndex = get(summaries, [summaryDataIndex, 'oneChangePeriodSummaries']).findIndex(summary =>
        summary.id === newSummary.id,
      );
      return state.setIn(
        ONE_CHANGE_PERIOD_SUMMARIES_DATA,
        set(summaries, [summaryDataIndex, 'oneChangePeriodSummaries', summaryIndex], newSummary),
      );
    }
    default:
      return state;
  }
}

function setOneChangePeriodSummary(oneChangePeriodSummary) {
  return {
    type: SET_ONE_CHANGE_PERIOD_SUMMARY,
    payload: oneChangePeriodSummary,
  };
}

function setDailyCustomerSummary(summary) {
  return {
    type: SET_CUSTOMERS_DAILY_SUMMARY,
    payload: summary,
  };
}

function setWeeklyCustomerSummary(summary) {
  return {
    type: SET_WEEKLY_CUSTOMER_SUMMARY,
    payload: summary,
  };
}

export function loadOneChangePeriodSummariesData() {
  return (dispatch) => {
    dispatch({ type: SET_ONE_CHANGE_PERIOD_SUMMARIES_DATA_LOADING });
    return apiService.summaries.oneChangePeriodSummaries()
      .then(summary =>
        dispatch({ type: SET_ONE_CHANGE_PERIOD_SUMMARIES_DATA, payload: summary }),
      );
  };
}

export function loadCustomersWeeklySummaries() {
  return dispatch =>
    apiService.summaries.getWeeklySummaries()
      .then(weeklySummaries =>
        dispatch({ type: SET_CUSTOMERS_WEEKLY_SUMMARIES, payload: weeklySummaries }),
      )
      .catch(() => dispatch(
        showNotification({
          message: 'There was an error loading weekly stats.',
          duration: 7000,
        })));
}

export function loadCustomersDailySummaries() {
  return dispatch =>
    apiService.summaries.dailySummaries()
      .then(dailySummaries =>
        dispatch({ type: SET_CUSTOMERS_DAILY_SUMMARIES, payload: dailySummaries }),
      )
      .catch(() => dispatch(
        showNotification({
          message: 'There was an error loading daily stats.',
          duration: 7000,
        })));
}

export function updateOneChangePeriodSummaryNotes(id, notes) {
  return dispatch =>
    apiService.summaries.updateOneChangePeriodSummaryNotes(id, notes)
      .then((oneChangePeriodSummary) => {
        dispatch(showNotification({
          message: 'Notes saved successfully.',
          duration: 7000,
        }));
        return dispatch(setOneChangePeriodSummary(oneChangePeriodSummary));
      })
      .catch(() => {
        dispatch(showNotification({
          message: 'There was an error while updating notes.',
          duration: 7000,
        }));
      });
}

export function updateDailyCustomerSummaryNotes(id, notes) {
  return dispatch =>
    apiService.summaries.updateDailyCustomerSummaryNotes(id, notes)
      .then((dailyCustomerSummary) => {
        dispatch(showNotification({
          message: 'Notes saved successfully.',
          duration: 7000,
        }));
        return dispatch(setDailyCustomerSummary(dailyCustomerSummary));
      })
      .catch(() => {
        dispatch(showNotification({
          message: 'There was an error while updating notes.',
          duration: 7000,
        }));
      });
}

export function updateWeeklyCustomerSummaryNotes(id, notes) {
  return dispatch =>
    apiService.summaries.updateWeeklyCustomerSummaryNotes(id, notes)
      .then((weeklyCustomerSummary) => {
        dispatch(showNotification({
          message: 'Notes saved successfully.',
          duration: 7000,
        }));
        return dispatch(setWeeklyCustomerSummary(weeklyCustomerSummary));
      })
      .catch(() => {
        dispatch(showNotification({
          message: 'There was an error while updating notes.',
          duration: 7000,
        }));
      });
}

export const getSummaries = createSelector(getData, data => data.summaries);

export const getOneChangePeriodSummaries = createSelector(
  getSummaries,
  summaries => summaries.get(ONE_CHANGE_PERIOD_SUMMARIES_DATA),
);

export const getOneChangePeriodSummariesLoading = createSelector(
  getSummaries,
  summaries => summaries.get(ONE_CHANGE_PERIOD_SUMMARIES_DATA_LOADING),
);

export const getCustomersWeeklySummaries = createSelector(
  getSummaries,
  summaries => summaries.get(CUSTOMER_WEEKLY_SUMMARIES_DATA),
);

export const getCustomersDailySummaries = createSelector(
  getSummaries,
  summaries => summaries.get(CUSTOMER_DAILY_SUMMARIES_DATA),
);
