import mapValues from 'lodash/mapValues';
import { Map } from 'immutable';
import { createSelector } from 'reselect';
import { getData } from './common';

const INITIAL_STATE = new Map({
  customers: new Map(),
  entries: new Map(),
  agentSummaries: new Map(),
  customerSummaries: new Map(),
  oneChangePeriods: new Map(),
  oneChangePeriodSummaries: new Map(),
  oneChangePeriodSummaryData: new Map(),
  dailyCustomerSummaries: new Map(),
  dailyCustomerSummaryData: new Map(),
  weeklyCustomerSummaries: new Map(),
  weeklyCustomerSummaryData: new Map(),
});

export default function entitiesReducer(state = INITIAL_STATE, action) {
  // Actions like @@redux/INIT do not have a payload
  if (!action.payload || !action.payload.entities) {
    return state;
  }

  const { entities } = action.payload;
  return state.merge(
    mapValues(entities, (collection, collectionName) =>
      state.get(collectionName).merge(new Map(collection))));
}

export const getEntities = createSelector(getData, data => data.entities);
