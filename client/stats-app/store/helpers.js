// eslint-disable-next-line import/prefer-default-export
export function buildApiActionTypes(baseName) {
  return {
    START: `${baseName}/START`,
    SUCCESS: `${baseName}/SUCCESS`,
    FAILURE: `${baseName}/FAILURE`,
  };
}
