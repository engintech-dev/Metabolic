import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';

const styles = {
  topBar: {
    position: 'fixed !important',
    top: 0,
  },
  mainContainer: {
    marginTop: '80px',
  },
};

function StatsLayout(props) {
  return (
    <div>
      <AppBar
        title="Stats App"
        titleStyle={{ cursor: 'pointer' }}
        className={props.classes.topBar}
        onTitleTouchTap={props.goToDashboard}
        showMenuIconButton={false}
        iconElementRight={<FlatButton label="Sign Out" />}
        onRightIconButtonTouchTap={props.signOut}
      />
      <main className={props.classes.mainContainer}>
        {props.children}
      </main>
      <Snackbar
        open={props.notification.open || false}
        message={props.notification.message || ''}
        autoHideDuration={props.notification.duration || 4000}
        onRequestClose={props.onNotificationClose}
      />
    </div>
  );
}

StatsLayout.propTypes = {
  classes: PropTypes.shape({
    topBar: PropTypes.string.isRequired,
    mainContainer: PropTypes.string.isRequired,
  }).isRequired,
  goToDashboard: PropTypes.func.isRequired,
  signOut: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  notification: PropTypes.shape({
    message: PropTypes.string,
    open: PropTypes.bool,
    duration: PropTypes.number,
  }).isRequired,
  onNotificationClose: PropTypes.func.isRequired,
};

export default injectSheet(styles)(StatsLayout);
