import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import isEmpty from 'lodash/isEmpty';
import Divider from 'material-ui/Divider';
import { entryShape, customerShape } from '../../common/prop-types/agents';
import { timezonedDateTime } from '../../common/lib/time';
import theme from '../../common/theme';
import { processNutrientsSummary } from '../../common/lib/nutrients';
import Carousel from '../../common/components/Carousel';
import NutrientCompositionTable from '../../common/components/NutrientCompositionTable';

const styles = {
  textCentered: {
    textAlign: 'center',
  },
};

function renderNutrientComposition(nutrientsSummary) {
  if (isEmpty(nutrientsSummary)) {
    return <h3>This entry does not have any nutrient information yet.</h3>;
  }

  const { nutrients, calories } = processNutrientsSummary(nutrientsSummary);
  return <NutrientCompositionTable nutrients={nutrients} calories={calories} />;
}

function Entry({ entry, customer, classes }) {
  const shouldShowCarousel = entry.photosData && entry.photosData.length > 0;
  return (
    <div>
      <h3 className={classes.textCentered}>
        {timezonedDateTime(entry.datetime, entry.timezone || customer.timezone)}
      </h3>
      <p className={classes.textCentered}>
        {entry.text}
      </p>

      {shouldShowCarousel &&
        <Carousel
          content={entry.photosData}
          loadingPlaceholderColor={theme.palette.primary1Color}
        />
      }

      <Divider />

      {renderNutrientComposition(entry.nutrientsSummary)}
    </div>
  );
}

Entry.propTypes = {
  entry: entryShape.isRequired,
  customer: customerShape.isRequired,
  classes: PropTypes.shape({
    textCentered: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(Entry);
