import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import PropTypes from 'prop-types';
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';
import moment from 'moment';

export default function RefreshStats({ createdAt, refresh }) {
  return (
    <p>
      Stats retrieved on {moment(createdAt).toString()}
      <FlatButton icon={<RefreshIcon />} onClick={() => refresh()} />
    </p>
  );
}

RefreshStats.propTypes = {
  createdAt: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]).isRequired,
  refresh: PropTypes.func.isRequired,
};
