import React from 'react';
import PropTypes from 'prop-types';
import CaloriesInsight from './CaloriesInsight';
import EntriesCountInsight from './EntriesCountInsight';
import LateEatingInsight from './LateEatingInsight';
import MealInsight from './MealInsight';

const COMPONENTS = {
  entriesCount: EntriesCountInsight,
  breakfast: MealInsight,
  amSnack: MealInsight,
  lunch: MealInsight,
  pmSnack: MealInsight,
  dinner: MealInsight,
  caloriesEaten: CaloriesInsight,
  lateEating: LateEatingInsight,
};

export default function Insights({ insight }) {
  const InsightComponent = COMPONENTS[insight.key];
  return <InsightComponent insight={insight} />;
}

Insights.propTypes = {
  insight: PropTypes.shape({}).isRequired,
};
