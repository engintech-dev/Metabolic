import React from 'react';
import PropTypes from 'prop-types';
import inflection from 'inflection';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import INSIGHTS_HUMAN_NAMES from '../../../../../common/lib/insights';

export default function CaloriesInsight({ insight }) {
  return (
    <div>
      <h4>{INSIGHTS_HUMAN_NAMES[insight.key]} details</h4>
      <Table selectable={false}>
        <TableBody displayRowCheckbox={false}>
          <TableRow>
            <TableRowColumn>Has Problem</TableRowColumn>
            <TableRowColumn>{insight.metadata.hasProblem ? 'Yes' : 'No'}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Status</TableRowColumn>
            <TableRowColumn>{inflection.capitalize(insight.metadata.status)}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Problem Index</TableRowColumn>
            <TableRowColumn>{insight.metadata.problemIndex}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Average Period Calories</TableRowColumn>
            <TableRowColumn>{insight.metadata.averagePeriodCalories}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Max Calories</TableRowColumn>
            <TableRowColumn>{insight.metadata.maxCalories}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>Min Calories</TableRowColumn>
            <TableRowColumn>{insight.metadata.minCalories}</TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
    </div>
  );
}

CaloriesInsight.propTypes = {
  insight: PropTypes.shape({}).isRequired,
};
