import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Insights from './Insights';

export default function DailySummary({ summary, insight }) {
  return (
    <div>
      <Table selectable={false}>
        <TableBody displayRowCheckbox={false}>
          <TableRow>
            <TableRowColumn>Date</TableRowColumn>
            <TableRowColumn>{summary.day}</TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
      <Insights insight={insight} />
    </div>
  );
}

DailySummary.propTypes = {
  summary: PropTypes.shape({}).isRequired,
  insight: PropTypes.shape({}).isRequired,
};
