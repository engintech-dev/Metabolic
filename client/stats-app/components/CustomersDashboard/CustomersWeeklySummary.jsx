import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {
  Table,
  TableBody,
  TableHeader,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
} from 'material-ui/Table';
import find from 'lodash/find';
import pick from 'lodash/pick';
import flatMap from 'lodash/flatMap';
import minBy from 'lodash/minBy';
import maxBy from 'lodash/maxBy';
import round from 'lodash/round';
import theme, { textCentered } from '../../../common/theme';
import Customer from './Customer';
import { INSIGHTS } from '../../../../common/lib/insights';
import INSIGHTS_HUMAN_NAMES from '../../../common/lib/insights';
import WeeklySummary from './Summaries/WeeklySummary';
import NotesDialog from '../../containers/CustomersDashboard/notes-dialog';
import NoteRow from './NoteRow';
import { customerSummaryShape } from '../../../common/prop-types/agents';

const MAX_NOTE_HEIGHT = '80px';

const styles = {
  good: {
    background: theme.palette.goodColor,
    cursor: 'pointer',
    ...textCentered,
  },
  ok: {
    background: theme.palette.okColor,
    cursor: 'pointer',
    ...textCentered,
  },
  bad: {
    background: theme.palette.badColor,
    color: 'white',
    cursor: 'pointer',
    ...textCentered,
  },
  tableColumn: {
    width: '100px',
    border: '1px solid rgb(224, 224, 224)',
    ...textCentered,
  },
  tableHeader: {
    borderBottom: 'none',
  },
  insightsTable: {
    flex: '0 0 200px',
  },
  insightTitle: {
    width: '150px',
    border: '1px solid rgb(224, 224, 224)',
  },
  tableBody: {
    overflow: 'visible',
  },
  customerSummaryContainer: {
    marginTop: '10px',
  },
  tableContainer: {
    display: 'flex',
    alignItems: 'start',
  },
  notesCell: {
    textAlign: 'left',
    height: MAX_NOTE_HEIGHT,
    whiteSpace: 'normal',
  },
  noteTitle: {
    height: MAX_NOTE_HEIGHT,
  },
};

function renderInsightValue(key, value, status) {
  return (
    <TableRowColumn
      key={key}
      style={{ ...styles[status], ...styles.tableColumn }}
    >
      {value}
    </TableRowColumn>
  );
}

function renderInsight(customerSummary, insightKey, date) {
  const summary = customerSummary.weeklySummaries[date];
  const emptyRowColumn = <TableRowColumn key={date} style={styles.tableColumn} />;
  if (!summary || !summary.insightsData) {
    return emptyRowColumn;
  }
  const insight = find(summary.insightsData, { key: insightKey });
  if (!insight) {
    return emptyRowColumn;
  }
  return renderInsightValue(
    date,
    round(insight.metadata.problemIndex, 2),
    insight.metadata.status,
  );
}

function renderCustomerInsights(customerSummary, dateRange) {
  return INSIGHTS.map(insightKey =>
    <TableRow key={insightKey}>
      {dateRange.map(({ start: date }) => renderInsight(customerSummary, insightKey, date))}
    </TableRow>,
  );
}

function renderInsightsTable() {
  return (
    <div style={styles.insightsTable}>
      <Table selectable={false}>
        <TableHeader displaySelectAll={false}>
          <TableRow>
            <TableHeaderColumn />
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          <TableRow>
            <TableRowColumn style={{ ...styles.insightTitle, ...styles.noteTitle }}>
              Notes
            </TableRowColumn>
          </TableRow>
          {
            INSIGHTS.map(insightKey =>
              <TableRow key={insightKey}>
                <TableRowColumn style={styles.insightTitle}>
                  {INSIGHTS_HUMAN_NAMES[insightKey]}
                </TableRowColumn>
              </TableRow>,
            )
          }
        </TableBody>
      </Table>
    </div>
  );
}

function renderNote(customerSummary, date, editNotesFor) {
  const summary = customerSummary.weeklySummaries[date];
  if (summary) {
    return (
      <NoteRow
        key={date}
        rowStyle={{ ...styles.tableColumn, ...styles.notesCell }}
        summary={summary}
        editNotesFor={editNotesFor}
        customer={customerSummary.customer}
        notes={summary.notes || ''}
      />
    );
  }
  // Should not be the case, but if summary doesn't exist we should not be able to add notes
  return <TableRowColumn key={date} style={{ ...styles.tableColumn, ...styles.notesCell }} />;
}

function renderNotes(customerSummary, dateRange, editNotesFor) {
  return (
    <TableRow>
      {dateRange.map(({ start: date }) => renderNote(customerSummary, date, editNotesFor))}
    </TableRow>
  );
}

function renderCustomer(customerSummary, dateRange, editNotesFor, handleCellClick) {
  return (
    <div key={customerSummary.customer.id} style={styles.customerSummaryContainer}>
      <Customer customer={customerSummary.customer} />
      <div style={styles.tableContainer}>
        {renderInsightsTable()}
        <Table
          key={customerSummary.customer.id}
          selectable={false}
          bodyStyle={styles.tableBody}
          onCellClick={(row, column) =>
            handleCellClick(customerSummary, dateRange[column], INSIGHTS[row - 1])}
        >
          <TableHeader
            style={styles.tableHeader}
            displaySelectAll={false}
            adjustForCheckbox={false}
          >
            <TableRow style={styles.tableHeader}>
              {
                dateRange.map(({ display: date }) =>
                  <TableHeaderColumn
                    key={date}
                    style={{ ...styles.tableColumn, ...styles.tableHeader }}
                  >
                    {date}
                  </TableHeaderColumn>,
                )
              }
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {renderNotes(customerSummary, dateRange, editNotesFor)}
            {renderCustomerInsights(customerSummary, dateRange)}
          </TableBody>
        </Table>
      </div>
    </div>
  );
}

function generateDateRange(customersSummaries) {
  const customersSummariesDates = flatMap(customersSummaries,
    customer => Object.keys(customer.weeklySummaries));
  if (!customersSummariesDates.length) {
    return [];
  }

  const startMoment = moment(minBy(customersSummariesDates));
  const endMoment = moment(maxBy(customersSummariesDates));
  const dates = [];
  for (const currentDate = endMoment; currentDate >= startMoment; currentDate.subtract(1, 'week')) {
    dates.push({
      start: currentDate.format('YYYY-MM-DD'),
      display: `${currentDate.format('MMM D')} to ${currentDate.clone().add(6, 'days').format('MMM D')}`,
    });
  }
  return dates;
}

class CustomersWeeklySummary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weeklyCustomerSummary: null,
      customer: null,
      lastClickedInsight: undefined,
      lastClickedSummary: undefined,
      openDialog: false,
    };
    this.editNotesFor = this.editNotesFor.bind(this);
    this.handleCellClick = this.handleCellClick.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.closeEditNotesFor = this.closeEditNotesFor.bind(this);
  }

  handleCellClick(customerSummary, date, insightKey) {
    const weeklySummary = customerSummary.weeklySummaries[date.start];
    const insight = weeklySummary &&
      find(weeklySummary.insightsData, ['key', insightKey]);
    if (insight) {
      this.setState({
        lastClickedInsight: insight,
        lastClickedSummary: pick(weeklySummary, ['startedAt', 'finishedAt']),
        openDialog: true,
      });
    }
  }

  handleCloseDialog() {
    this.setState({
      openDialog: false,
      lastClickedInsight: undefined,
      lastClickedSummary: undefined,
    });
  }

  closeEditNotesFor() {
    this.setState({
      weeklyCustomerSummary: null,
      customer: null,
    });
  }

  editNotesFor(weeklyCustomerSummary, customer) {
    this.setState({
      weeklyCustomerSummary,
      customer,
    });
  }

  render() {
    const { customersWeeklySummaries, updateNotes } = this.props;
    const { weeklyCustomerSummary, customer } = this.state;
    const dateRange = generateDateRange(customersWeeklySummaries);
    return (
      <div>
        {customersWeeklySummaries.map(customerSummary => renderCustomer(
          customerSummary, dateRange, this.editNotesFor, this.handleCellClick),
        )}
        <Dialog
          title="Insight information"
          modal={false}
          open={this.state.openDialog}
          onRequestClose={this.handleCloseDialog}
          actions={<FlatButton label="Close" primary onClick={this.handleCloseDialog} />}
          repositionOnUpdate
          autoScrollBodyContent
        >
          {this.state.lastClickedInsight &&
            <WeeklySummary
              insight={this.state.lastClickedInsight}
              summary={this.state.lastClickedSummary}
            />}
        </Dialog>
        {weeklyCustomerSummary &&
          <NotesDialog
            open
            noteableId={weeklyCustomerSummary.id}
            updateNotes={updateNotes}
            title={`Update notes of ${customer.name} for ${weeklyCustomerSummary.startDay}`}
            initialNotes={weeklyCustomerSummary.notes || ''}
            onClose={this.closeEditNotesFor}
          />
        }
      </div>
    );
  }
}

CustomersWeeklySummary.propTypes = {
  customersWeeklySummaries: PropTypes.arrayOf(PropTypes.shape({
    customer: customerSummaryShape.isRequired,
    customersDailySummaries: PropTypes.shape({}),
  })).isRequired,
  updateNotes: PropTypes.func.isRequired,
};

export default CustomersWeeklySummary;
