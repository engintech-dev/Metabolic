import React from 'react';
import PropTypes from 'prop-types';
import {
  TableRowColumn,
} from 'material-ui/Table';
import LinesEllipsis from 'react-lines-ellipsis';
import FlatButton from 'material-ui/FlatButton';

function NoteRow(props) {
  const { notes, summary, customer, editNotesFor, rowStyle } = props;
  return (
    <TableRowColumn style={rowStyle} >
      <LinesEllipsis maxLine={3} text={notes} />
      <FlatButton
        primary
        onClick={() => editNotesFor(summary, customer)}
      >
        Edit
      </FlatButton>
    </TableRowColumn>
  );
}

NoteRow.propTypes = {
  editNotesFor: PropTypes.func.isRequired,
  notes: PropTypes.string.isRequired,
  summary: PropTypes.shape({}).isRequired,
  customer: PropTypes.shape({}).isRequired,
  rowStyle: PropTypes.shape({}),
};

NoteRow.defaultProps = {
  rowStyle: {},
};

export default NoteRow;
