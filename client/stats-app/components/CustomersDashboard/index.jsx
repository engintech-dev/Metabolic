import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import injectSheet from 'react-jss';
import SmallPaper from '../../../common/components/SmallPaper';
import routeHelper from '../../routes-helpers';

const styles = {
  dashboardsList: {
    textAlign: 'left',
  },
};

function CustomersDashboard(props) {
  return (
    <SmallPaper>
      <h1>Customers Dashboard</h1>
      <ul className={props.classes.dashboardsList}>
        <li><Link to={routeHelper.getCustomersTrackingPath()}>Tracking</Link></li>
        <li><Link to={routeHelper.getCustomersAnalyticsPath()}>Analytics</Link></li>
      </ul>
    </SmallPaper>
  );
}

CustomersDashboard.propTypes = {
  classes: PropTypes.shape({
    dashboardsList: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(CustomersDashboard);
