import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Tabs, Tab } from 'material-ui/Tabs';
import keyMirror from 'keymirror';
import { container } from '../../../common/theme';
import CustomersStatusSummary from '../../containers/CustomersDashboard/CustomersStatusSummary';
import OneChangePeriodSummaryContainer from '../../containers/CustomersDashboard/OneChangePeriodSummary';
import CustomersWeeklySummary from '../../containers/CustomersDashboard/CustomersWeeklySummary';
import CustomersDailySummary from '../../containers/CustomersDashboard/CustomersDailySummary';

const styles = {
  container,
};

export const tabValues = keyMirror({
  overview: null,
  daily: null,
  weekly: null,
  ocPeriod: null,
});

function OldCustomersDashboard(props) {
  const { activeTab, onTabChange } = props;

  const usersSummary = (
    <Tabs value={tabValues[activeTab] || tabValues.overview} onChange={onTabChange}>
      <Tab label="Overview" value={tabValues.overview}>
        <CustomersStatusSummary currentDate={props.currentDate} />
      </Tab>
      <Tab label="Daily" value={tabValues.daily}>
        <CustomersDailySummary />
      </Tab>
      <Tab label="Weekly" value={tabValues.weekly}>
        <CustomersWeeklySummary />
      </Tab>
      <Tab label="OC Period" value={tabValues.ocPeriod}>
        <OneChangePeriodSummaryContainer />
      </Tab>
    </Tabs>
  );

  return (
    <div className={props.classes.container}>
      {usersSummary}
    </div>
  );
}

OldCustomersDashboard.propTypes = {
  activeTab: PropTypes.string,
  currentDate: PropTypes.string,
  onTabChange: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
  }).isRequired,
};

OldCustomersDashboard.defaultProps = {
  children: undefined,
  activeTab: tabValues.overview,
  currentDate: undefined,
};

export default injectSheet(styles)(OldCustomersDashboard);
