import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Tabs, Tab } from 'material-ui/Tabs';
import FlatButton from 'material-ui/FlatButton';
import BackIcon from 'material-ui/svg-icons/navigation/chevron-left';
import keyMirror from 'keymirror';
import { container } from '../../../common/theme';

const styles = {
  container,
};

const tabValues = keyMirror({
  daily: null,
  weekly: null,
  reportCountdown: null,
});

function Tracking({ activeTab, onTabChange, goBack, classes }) {
  return (
    <div className={classes.container}>
      <h1>
        <FlatButton
          label="Back"
          icon={<BackIcon />}
          onClick={goBack}
        />
        Tracking
      </h1>
      <Tabs value={tabValues[activeTab] || tabValues.daily} onChange={onTabChange}>
        <Tab label="Daily" value={tabValues.daily}>
          <h2>Daily</h2>
        </Tab>
        <Tab label="Weekly" value={tabValues.weekly}>
          <h2>Weekly</h2>
        </Tab>
        <Tab label="Report Countdown" value={tabValues.reportCountdown}>
          <h2>Report Countdown</h2>
        </Tab>
      </Tabs>
    </div>
  );
}

Tracking.propTypes = {
  activeTab: PropTypes.string,
  onTabChange: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
  }).isRequired,
};

Tracking.defaultProps = {
  activeTab: tabValues.daily,
};

export default injectSheet(styles)(Tracking);
