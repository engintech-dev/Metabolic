import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import {
  Table,
  TableBody,
  TableHeader,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import memoize from 'lodash/memoize';
import theme from '../../../common/theme';
import { customerShape } from '../../../common/prop-types/agents';
import RefreshStats from '../RefreshStats';
import DaySwitcher from '../../../common/components/DaySwitcher';
import Customer from './Customer';

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: '25px',
    marginBottom: '25px',
  },
  good: {
    background: theme.palette.goodColor,
  },
  ok: {
    background: theme.palette.okColor,
  },
  bad: {
    background: theme.palette.badColor,
  },
};

function entriesCountStyle(count) {
  if (count >= 3) {
    return styles.good;
  }
  if (count === 0) {
    return styles.bad;
  }
  return styles.ok;
}

function filterUsers(textFilter, data) {
  const filterToLowerCase = textFilter.toLowerCase();
  const matches = text => text.toLowerCase().indexOf(filterToLowerCase) >= 0;

  return textFilter
    ? data.filter(({ user: { name, email } }) => matches(name) || matches(email))
    : data;
}

function resolver(textFilter, data, currentDate) {
  return textFilter + currentDate;
}

class CustomersStatusSummary extends Component {
  constructor(props) {
    super(props);

    this.state = { textFilter: '' };

    this.onSearch = this.onSearch.bind(this);
    this.filterUsers = memoize(filterUsers, resolver);
  }

  onSearch(event, textFilter) {
    this.setState({ textFilter });
  }

  renderRows() {
    const filteredUsers = this.filterUsers(
      this.state.textFilter,
      this.props.overview.data,
      this.props.currentDate,
    );

    return filteredUsers.map(customerData => (
      <TableRow key={customerData.user.id}>
        <TableRowColumn>
          <Customer customer={customerData.user} />
        </TableRowColumn>
        <TableRowColumn style={entriesCountStyle(customerData.entriesCount)}>
          {customerData.entriesCount}
        </TableRowColumn>
      </TableRow>
    ));
  }

  render() {
    const {
      currentUserTimezone,
      currentDate,
      overview,
      reloadData,
      onDaySwitch,
      classes,
    } = this.props;

    return (
      <div>
        <DaySwitcher
          timezone={currentUserTimezone}
          currentDate={currentDate}
          onDaySwitchClick={onDaySwitch}
        />

        <div className={classes.container}>
          <TextField hintText="Search Customers" onChange={this.onSearch} />
          <RefreshStats createdAt={overview.createdAt} refresh={reloadData} />
        </div>

        <Table selectable={false}>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false} enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Customer</TableHeaderColumn>
              <TableHeaderColumn>Entries Count</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} showRowHover={false}>
            {this.renderRows()}
          </TableBody>
        </Table>
      </div>
    );
  }
}

CustomersStatusSummary.propTypes = {
  currentUserTimezone: PropTypes.string.isRequired,
  currentDate: PropTypes.string,
  overview: PropTypes.shape({
    data: PropTypes.arrayOf(PropTypes.shape({
      user: customerShape,
      entriesCount: PropTypes.number,
    })).isRequired,
    createdAt: PropTypes.string.isRequired,
  }).isRequired,
  onDaySwitch: PropTypes.func.isRequired,
  reloadData: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
  }).isRequired,
};

CustomersStatusSummary.defaultProps = {
  currentDate: undefined,
};

export default injectSheet(styles)(CustomersStatusSummary);
