import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {
  Table,
  TableBody,
  TableHeader,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
} from 'material-ui/Table';
import find from 'lodash/find';
import pick from 'lodash/pick';
import inflection from 'inflection';
import LinesEllipsis from 'react-lines-ellipsis';
import NotesDialog from '../../containers/CustomersDashboard/notes-dialog';
import theme from '../../../common/theme';
import { INSIGHTS } from '../../../../common/lib/insights';
import INSIGHTS_HUMAN_NAMES from '../../../common/lib/insights';
import { DAY_FORMAT } from '../../../../common/lib/datetime';
import Customer from './Customer';
import OCSummary from './Summaries/OCSummary';
import { customerSummaryShape } from '../../../common/prop-types/agents';

const MAX_NOTE_HEIGHT = '80px';
const CELL_BORDER = '1px solid rgb(224, 224, 224)';
const CELL_WIDTH = '100px';

const styles = {
  good: {
    background: theme.palette.goodColor,
    cursor: 'pointer',
    textAlign: 'center',
  },
  ok: {
    background: theme.palette.okColor,
    cursor: 'pointer',
    textAlign: 'center',
  },
  bad: {
    background: theme.palette.badColor,
    cursor: 'pointer',
    textAlign: 'center',
  },
  header: {
    textAlign: 'center',
    width: CELL_WIDTH,
  },
  cell: {
    border: CELL_BORDER,
    width: CELL_WIDTH,
    textAlign: 'center',
  },
  notesCell: {
    textAlign: 'left',
    height: MAX_NOTE_HEIGHT,
    maxHeight: MAX_NOTE_HEIGHT,
    border: CELL_BORDER,
    whiteSpace: 'normal',
    width: CELL_WIDTH,
  },
  actionButton: {
    cursor: 'pointer',
  },
  customerSummaryContainer: {
    marginTop: '10px',
  },
  tableContainer: {
    display: 'flex',
    alignItems: 'start',
  },
  tableBody: {
    overflow: 'visible',
  },
  insightsTable: {
    flex: '0 0 200px',
  },
  insightTitle: {
    width: '150px',
    border: '1px solid transparent',
  },
  noteTitle: {
    height: MAX_NOTE_HEIGHT,
  },
};

const RENDER_DAY_FORMAT = 'MMM D';

function insightStyle(insight) {
  return !insight ? {} : styles[insight.metadata.status];
}

function getHeaderTitle(fromMoment, toMoment) {
  let diff = toMoment.diff(fromMoment, 'weeks');
  let unit = 'week';
  if (diff === 0) {
    diff = toMoment.diff(fromMoment, 'days');
    unit = 'day';
  }
  return `${diff} ${inflection.inflect(unit, diff)}`;
}

function momentize(date) {
  if (!date) {
    return null;
  }
  return moment.utc(date, DAY_FORMAT);
}

function getMomentRange(fromMoment, toMoment) {
  if (!fromMoment || !toMoment) {
    return null;
  }
  return `${fromMoment.format(RENDER_DAY_FORMAT)} to ${toMoment.format(RENDER_DAY_FORMAT)}`;
}

function renderHeaderCell(oneChangePeriodSummary) {
  const fromMoment = momentize(oneChangePeriodSummary.startedAt);
  const toMoment = momentize(oneChangePeriodSummary.finishedAt);
  return (
    <TableHeaderColumn
      key={oneChangePeriodSummary.id}
      title={getHeaderTitle(fromMoment, toMoment)}
      style={styles.header}
    >
      {getMomentRange(fromMoment, toMoment)}
    </TableHeaderColumn>
  );
}

function renderInsightCell(oneChangePeriodSummary, key) {
  const insight = find(oneChangePeriodSummary.insightsData, { key });
  return (
    <TableRowColumn
      style={{ ...styles.cell, ...insightStyle(insight) }}
      key={oneChangePeriodSummary.id}
    />
  );
}

function renderInsightsTable() {
  return (
    <div style={styles.insightsTable}>
      <Table selectable={false}>
        <TableHeader displaySelectAll={false}>
          <TableRow>
            <TableHeaderColumn />
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          <TableRow>
            <TableRowColumn style={{ ...styles.insightTitle, ...styles.noteTitle }}>
              Notes
            </TableRowColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn style={styles.insightTitle}>
              One Change
            </TableRowColumn>
          </TableRow>
          {
            INSIGHTS.map(insightKey =>
              <TableRow key={insightKey}>
                <TableRowColumn style={styles.insightTitle}>
                  {INSIGHTS_HUMAN_NAMES[insightKey]}
                </TableRowColumn>
              </TableRow>,
            )
          }
        </TableBody>
      </Table>
    </div>
  );
}

function renderInsights(oneChangePeriodSummaries) {
  return (
    INSIGHTS.map(insight =>
      <TableRow key={insight}>
        {oneChangePeriodSummaries
          .map(oneChangePeriod => renderInsightCell(oneChangePeriod, insight))}
      </TableRow>,
    )
  );
}

function renderOneChangeSummaryCell(oneChangePeriodSummary) {
  const oneChange = oneChangePeriodSummary.oneChange;
  const insight = find(oneChangePeriodSummary.insightsData, { key: oneChange });
  const displayText = INSIGHTS_HUMAN_NAMES[oneChange] || '-';
  return (
    <TableRowColumn
      style={{ ...styles.cell, ...insightStyle(insight) }}
      key={oneChangePeriodSummary.id}
      title={displayText}
    >
      {displayText}
    </TableRowColumn>
  );
}

function renderOneChanges(oneChangePeriodSummaries) {
  return (
    <TableRow>
      {oneChangePeriodSummaries.map(renderOneChangeSummaryCell)}
    </TableRow>
  );
}

class OneChangePeriod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oneChangePeriodSummaryId: null,
      lastClickedInsight: null,
      lastClickedSummary: null,
      openDialog: false,
      oneChangePeriodSummary: null,
      customer: null,
    };

    this.handleCellClick = this.handleCellClick.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.renderTimeline = this.renderTimeline.bind(this);
    this.renderNotes = this.renderNotes.bind(this);
    this.onEditNotesClose = this.onEditNotesClose.bind(this);
  }

  onEditNotesClose() {
    this.setState({
      oneChangePeriodSummary: null,
      customer: null,
    });
  }

  editNotesFor(oneChangePeriodSummary, customer) {
    if (oneChangePeriodSummary) {
      this.setState({
        oneChangePeriodSummary,
        customer,
      });
    }
  }

  handleCellClick(customerSummary, insightKey) {
    const insight = find(customerSummary.insightsData, ['key', insightKey]);
    if (insight) {
      this.setState({
        lastClickedInsight: insight,
        lastClickedSummary: pick(customerSummary, ['startedAt', 'finishedAt', 'oneChange']),
        openDialog: true,
      });
    }
  }

  handleCloseDialog() {
    this.setState({
      openDialog: false,
      lastClickedInsight: null,
      lastClickedSummary: null,
    });
  }

  renderNotes(oneChangePeriodSummaryData) {
    const { oneChangePeriodSummaries, customer } = oneChangePeriodSummaryData;
    return (
      <TableRow>
        {oneChangePeriodSummaries.map(oneChangePeriodSummary =>
          <TableRowColumn
            style={styles.notesCell}
            key={oneChangePeriodSummary.id}
          >
            <LinesEllipsis maxLine={3} text={oneChangePeriodSummary.notes} />
            <FlatButton
              primary
              onClick={() => this.editNotesFor(oneChangePeriodSummary, customer)}
            >
              Edit
            </FlatButton>
          </TableRowColumn>,
        )}
      </TableRow>
    );
  }

  renderTimeline(data) {
    const oneChangePeriodSummaries = data.oneChangePeriodSummaries;
    return (
      <div key={data.customer.id} style={styles.customerSummaryContainer}>
        <Customer customer={data.customer} />
        <div style={styles.tableContainer}>
          {renderInsightsTable()}
          <Table
            selectable={false}
            bodyStyle={styles.tableBody}
            key={data.customer.id}
            fixedHeader
            onCellClick={(row, column) =>
              this.handleCellClick(oneChangePeriodSummaries[column], INSIGHTS[row - 2])}
          >
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                {oneChangePeriodSummaries.map(renderHeaderCell)}
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderNotes(data)}
              {renderOneChanges(oneChangePeriodSummaries)}
              {renderInsights(oneChangePeriodSummaries)}
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }

  render() {
    const { oneChangePeriodSummary } = this.state;
    return (
      <div>
        {this.props.oneChangePeriodSummariesData.map(this.renderTimeline)}
        <Dialog
          title={'Insight information'}
          modal={false}
          open={this.state.openDialog}
          onRequestClose={this.handleCloseDialog}
          actions={<FlatButton label="Close" primary onClick={this.handleCloseDialog} />}
          repositionOnUpdate
          autoScrollBodyContent
        >
          {this.state.lastClickedInsight &&
            <OCSummary
              insight={this.state.lastClickedInsight}
              summary={this.state.lastClickedSummary}
            />}
        </Dialog>
        {oneChangePeriodSummary &&
          <NotesDialog
            open
            noteableId={oneChangePeriodSummary.id}
            updateNotes={this.props.updateNotes}
            title={`Update notes of ${this.state.customer.name} from ${getMomentRange(momentize(oneChangePeriodSummary.startedAt), momentize(oneChangePeriodSummary.finishedAt))}`}
            initialNotes={this.state.oneChangePeriodSummary.notes || ''}
            onClose={this.onEditNotesClose}
          />
        }
      </div>
    );
  }
}

OneChangePeriod.propTypes = {
  oneChangePeriodSummariesData: PropTypes.arrayOf(PropTypes.shape({
    customer: customerSummaryShape.isRequired,
    oneChangePeriodSummaries: PropTypes.arrayOf(PropTypes.shape({})),
  })).isRequired,
  updateNotes: PropTypes.func.isRequired,
};

export default OneChangePeriod;
