import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import Divider from 'material-ui/Divider';
import FlatButton from 'material-ui/FlatButton';
import {
  Table,
  TableBody,
  TableHeader,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
} from 'material-ui/Table';
import get from 'lodash/get';
import keyBy from 'lodash/keyBy';
import pick from 'lodash/pick';
import moment from 'moment-timezone';
import theme from '../../../common/theme';
import { customerShape } from '../../../common/prop-types/agents';
import RefreshStats from '../RefreshStats';
import Entry from '../Entry';
import { nutrientsNames } from '../../../common/lib/nutrients';
import { MEALS_DEFINITION, groupEntriesByMeal } from '../../../../common/lib/meals';

const styles = {
  good: {
    background: theme.palette.goodColor,
    textAlign: 'center',
    cursor: 'pointer',
  },
  ok: {
    background: theme.palette.okColor,
    textAlign: 'center',
    cursor: 'pointer',
  },
  bad: {
    background: theme.palette.badColor,
    textAlign: 'center',
    cursor: 'pointer',
  },
  entryDivider: {
    height: '2px',
    margin: '20px 0',
  },
};

function getCarbsIndicator(carbs) {
  if (carbs < 15) {
    return 'good';
  }
  if (carbs < 40) {
    return 'ok';
  }
  return 'bad';
}

function processStats(customer, entriesByDay) {
  const data = entriesByDay.data;
  const timezone = customer.timezone;
  const dataRows = [];
  const firstDay = get(data, [0, 'day']);
  if (firstDay) {
    const firstMoment = moment.tz(firstDay, timezone);
    const lastMoment = moment.tz(timezone); // always show data until today
    const dataByDay = keyBy(data, 'day');
    for (let date = firstMoment; date <= lastMoment; date.add(1, 'day')) {
      const dateStr = date.format('YYYY-MM-DD');
      const dateEntries = get(dataByDay, [dateStr, 'entries']);
      const entriesByMeal = groupEntriesByMeal(dateEntries, timezone);
      dataRows.push({
        dateStr,
        weekDayStr: date.format('dddd'),
        cells: MEALS_DEFINITION.map(meal => ({
          meal,
          entries: entriesByMeal[meal.key],
        })),
      });
    }
  }
  return dataRows;
}

class EntriesByDay extends Component {
  constructor(props) {
    super(props);
    this.handleCellClick = this.handleCellClick.bind(this);
    this.handleDetailsClose = this.handleDetailsClose.bind(this);
    this.state = {
      open: false,
      dataRows: processStats(this.props.customer, this.props.entriesByDay),
    };
  }

  componentWillReceiveProps(newProps) {
    if (this.props.customer === newProps.customer
      && this.props.entriesByDay === newProps.entriesByDay) {
      return;
    }
    this.setState({ dataRows: processStats(newProps.customer, newProps.entriesByDay) });
  }

  handleCellClick(rowNumber, colNumber) {
    const dataRow = this.state.dataRows[rowNumber];
    // first two cells are dateStr and weekDaystr, so we subtract two to get the correct data cell
    const cell = dataRow.cells[colNumber - 2];
    if (!cell.entries) {
      return;
    }
    this.setState({ open: true, details: { ...cell, ...pick(dataRow, 'weekDayStr', 'dateStr') } });
  }

  handleDetailsClose() {
    this.setState({ open: false });
  }

  renderDataRowColumn(meal, entries = []) {
    if (!entries.length) {
      return <TableRowColumn key={meal.key} />;
    }
    const totalCarbs = Math.round(entries.reduce(
      (result, entry) =>
        result + entry.nutrientsSummary.reduce(
          (carbs, nutrient) =>
            carbs + (nutrient.name === nutrientsNames.carbs ? nutrient.value : 0),
          0,
        ),
      0,
    ));
    return (
      <TableRowColumn
        key={meal.key}
        onClick={() => this.handleOpen(entries)}
        style={styles[getCarbsIndicator(totalCarbs)]}
      >
        {totalCarbs}
      </TableRowColumn>
    );
  }

  renderRows() {
    return this.state.dataRows.map(dataRow => (
      <TableRow key={dataRow.dateStr}>
        <TableHeaderColumn>{dataRow.dateStr}</TableHeaderColumn>
        <TableHeaderColumn>{dataRow.weekDayStr}</TableHeaderColumn>
        {dataRow.cells.map(cell => this.renderDataRowColumn(cell.meal, cell.entries))}
      </TableRow>
    ));
  }

  render() {
    const { details } = this.state;
    return (
      <div>
        <RefreshStats
          createdAt={this.props.entriesByDay.createdAt}
          refresh={this.props.reloadEntriesByDay}
        />
        <Table selectable={false} onCellClick={this.handleCellClick}>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false} enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Date</TableHeaderColumn>
              <TableHeaderColumn>Day</TableHeaderColumn>
              {MEALS_DEFINITION.map(meal => (
                <TableHeaderColumn key={meal.key} tooltip={`${meal.from} - ${meal.to}`}>
                  {meal.name}
                </TableHeaderColumn>
              ))}
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} showRowHover={false}>
            {this.renderRows()}
          </TableBody>
        </Table>
        {details &&
          <Dialog
            title={`Entries at ${details.meal.name} - ${details.weekDayStr}, ${details.dateStr}`}
            modal={false}
            open={this.state.open}
            onRequestClose={this.handleDetailsClose}
            actions={<FlatButton label="Close" primary onClick={this.handleDetailsClose} />}
            repositionOnUpdate={false}
            autoScrollBodyContent
          >
            {details.entries.map((entry, index) =>
              <div key={entry.id}>
                {details.entries.length > 1 &&
                  <h2>Entry {index + 1} of {details.entries.length}</h2>}
                <Entry entry={entry} customer={this.props.customer} />
                {index !== details.entries.length - 1 && <Divider style={styles.entryDivider} />}
              </div>)}
          </Dialog>}
      </div>
    );
  }
}

EntriesByDay.propTypes = {
  reloadEntriesByDay: PropTypes.func.isRequired,
  entriesByDay: PropTypes.shape({
    createdAt: PropTypes.string.isRequired,
    customerId: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(PropTypes.shape({})),
  }).isRequired,
  customer: customerShape.isRequired,
};

export default EntriesByDay;
