import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { Field } from 'redux-form';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import { SelectField } from 'redux-form-material-ui';
import moment from 'moment';
import { required } from '../../../../common/forms/validators';
import SubmitButton from '../../../../common/components/SubmitButton';
import { INSIGHT_TRANSLATIONS } from '../../../../../common/lib/insights';
import { DEFAULT_ONE_CHANGE } from '../../../../../common/lib/one-change-period';
import { oneChangePeriodShape } from '../../../../common/prop-types/insights';

const styles = {
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  select: {
    minWidth: '480px',
  },
  buttonsContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  cancelButton: {
    marginRight: '10px',
  },
  submitButton: {
    marginLeft: '10px',
  },
};

class OneChangePeriodForm extends Component {
  constructor(props) {
    super(props);

    this.state = { dialogOpen: false };
    this.closeDialog = this.closeDialog.bind(this);
  }

  componentDidMount() {
    const { oneChangePeriod: { startedAt, oneChange } } = this.props;
    if (moment().diff(startedAt, 'days') <= 1 && oneChange !== DEFAULT_ONE_CHANGE) {
      this.openDialog();
    }
  }

  setShouldUpdate(shouldUpdate) {
    this.props.change('shouldUpdate', shouldUpdate);
    this.closeDialog();
  }

  openDialog() {
    this.setState({ dialogOpen: true });
  }

  closeDialog() {
    this.setState({ dialogOpen: false });
  }

  render() {
    const { pristine, submitting, handleSubmit, oneChangePeriod: { startedAt } } = this.props;
    const menuItems = Object.keys(INSIGHT_TRANSLATIONS)
      .map(key => <MenuItem key={key} value={key} primaryText={INSIGHT_TRANSLATIONS[key]} />);

    const dialogActions = [
      <FlatButton
        label="Create New One Change"
        onClick={() => this.setShouldUpdate(false)}
      />,
      <FlatButton
        label="Update One Change"
        primary
        onClick={() => this.setShouldUpdate(true)}
      />,
    ];

    return (
      <form onSubmit={handleSubmit} style={styles.wrapper}>
        <h1>
          {this.props.shouldUpdate ? 'Update One Change' : 'Create New One Change'}
        </h1>

        <Field
          floatingLabelText="Text"
          name="oneChange"
          component={SelectField}
          style={styles.select}
          validate={required}
        >
          {menuItems}
        </Field>
        <div style={styles.buttonsContainer}>
          <RaisedButton
            label="Cancel"
            onClick={this.props.handleCancel}
            style={styles.cancelButton}
          />
          <SubmitButton
            disabled={pristine || submitting}
            submitting={submitting}
            label="Submit"
            style={styles.submitButton}
          />
        </div>

        <Dialog
          title="Before you add a new One Change..."
          actions={dialogActions}
          open={this.state.dialogOpen}
          onRequestClose={this.closeDialog}
          modal
        >
          <p>It seems that this One Change was created no more than one day ago ({moment(startedAt).format('LLLL')}).</p>
          <p>Do you want to create a new one, or simply update the existing one?</p>
        </Dialog>
      </form>
    );
  }
}

OneChangePeriodForm.propTypes = {
  oneChangePeriod: oneChangePeriodShape.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  shouldUpdate: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
};

export default injectSheet(styles)(OneChangePeriodForm);
