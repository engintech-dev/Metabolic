import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import classNames from 'classnames';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import moment from 'moment';
import { INSIGHT_TRANSLATIONS } from '../../../../../common/lib/insights';
import { DEFAULT_ONE_CHANGE } from '../../../../../common/lib/one-change-period';
import Form from '../../../containers/CustomerStats/OneChangePeriod/Form';
import { oneChangePeriodShape } from '../../../../common/prop-types/insights';

const styles = {
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  opaque: {
    opacity: 0.25,
  },
  divider: {
    marginTop: '25px',
    marginBottom: '25px',
  },
};

class CurrentOneChangePeriod extends Component {
  constructor(props) {
    super(props);

    this.state = { editing: false };
    this.toggleEdit = this.toggleEdit.bind(this);
  }

  toggleEdit() {
    this.setState(previousState => ({ editing: !previousState.editing }));
  }

  renderBody() {
    const { oneChangePeriod: { startedAt, finishedAt, oneChange }, classes } = this.props;

    if (oneChange === DEFAULT_ONE_CHANGE) {
      return <h1>There is currently no active One Change</h1>;
    }

    return (
      <div className={classes.wrapper}>
        <h1>Current One Change</h1>
        <h3>{`${moment(startedAt).format('MMM D')} to ${moment(finishedAt).format('MMM D')}`}</h3>
        <p>{INSIGHT_TRANSLATIONS[oneChange]}</p>
      </div>
    );
  }

  renderForm() {
    if (!this.state.editing) {
      return null;
    }

    return (
      <div>
        <Divider style={styles.divider} />
        <Form
          oneChangePeriod={this.props.oneChangePeriod}
          handleCancel={this.toggleEdit}
          onSuccessSubmit={this.toggleEdit}
        />
      </div>
    );
  }

  render() {
    const { oneChangePeriod: { oneChange }, classes } = this.props;
    const { editing } = this.state;

    return (
      <div>
        <div className={classNames({ [classes.opaque]: editing, [classes.wrapper]: true })}>
          {this.renderBody()}
          <RaisedButton
            primary
            label={oneChange === DEFAULT_ONE_CHANGE ? 'Create One' : 'Change It'}
            onClick={this.toggleEdit}
            disabled={editing}
          />
        </div>
        {this.renderForm()}
      </div>
    );
  }
}

CurrentOneChangePeriod.propTypes = {
  oneChangePeriod: oneChangePeriodShape.isRequired,
  classes: PropTypes.shape({
    wrapper: PropTypes.string.isRequired,
    opaque: PropTypes.string.isRequired,
    divider: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(CurrentOneChangePeriod);
