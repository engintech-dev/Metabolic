import React from 'react';
import { Line } from 'react-chartjs-2';
import PropTypes from 'prop-types';
import map from 'lodash/map';

const chartOptions = {
  scales: {
    yAxes: [{
      ticks: {
        callback: value => `${value}%`,
      },
    }],
    xAxes: [{
      ticks: {
        callback: value => `${value}:00`,
      },
    }],
  },
  tooltips: {
    mode: 'index', // show all series values in tooltip
    position: 'nearest', // tooltip located nearest to the hovered point
    callbacks: {
      label: (tooltipItem, data) => `${data.datasets[tooltipItem.datasetIndex].label}: ${tooltipItem.yLabel}%`,
    },
  },
};

function hexToRgba(hex, alpha) {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);
  const a = alpha || 1;
  return `rgba(${r}, ${g}, ${b}, ${a})`;
}

function datasetForNutrient(nutrient, nutrientsData, colors, fill) {
  return {
    label: nutrient,
    fill,
    lineTension: 0.1,
    backgroundColor: hexToRgba(colors[nutrient], 0.2),
    borderColor: colors[nutrient],
    pointBorderColor: colors[nutrient],
    pointHoverBackgroundColor: colors[nutrient],
    pointHoverBorderColor: colors[nutrient],
    pointBackgroundColor: colors[nutrient],
    data: nutrientsData[nutrient],
  };
}

function dayStatsToData(props) {
  const { nutrientsData, nutrients, colors, fill, hours } = props;
  const nutrientsToDataset = nutrient => datasetForNutrient(nutrient, nutrientsData, colors, fill);
  return { labels: hours, datasets: map(nutrients, nutrientsToDataset) };
}

function DayChart(props) {
  const data = dayStatsToData(props);
  return <Line data={data} options={chartOptions} />;
}

DayChart.defaultProps = {
  fill: false,
};

DayChart.propTypes = {
  nutrientsData: PropTypes.shape({}).isRequired,
  nutrients: PropTypes.arrayOf(PropTypes.string).isRequired,
  colors: PropTypes.shape({}).isRequired,
  fill: PropTypes.bool,
  hours: PropTypes.arrayOf(PropTypes.number).isRequired,
};

export default DayChart;
