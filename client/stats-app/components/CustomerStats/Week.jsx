import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import { HorizontalBar } from 'react-chartjs-2';
import { GridList, GridTile } from 'material-ui/GridList';
import { cloneDeep, keyBy } from 'lodash';
import theme from '../../../common/theme';
import RefreshStats from '../RefreshStats';

const styles = {
  gridList: {
    display: 'flex',
    flexWrap: 'wrap',
  },
};

const chartDefaultOptions = {
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
      },
    ],
    yAxes: [
      {
        barPercentage: 0.8,
        categoryPercentage: 1,
        stacked: true,
        gridLines: {
          display: false,
        },
        ticks: {
          fontStyle: 'bold',
          callback: value => value,
        },
      },
    ],
  },
  title: {
    display: true,
    position: 'bottom',
  },
  tooltips: {
    callbacks: {
      title: () => '',
      label: (tooltipItem, data) => `${Math.floor(data.datasets[tooltipItem.datasetIndex].originalData[tooltipItem.index] * 100)}%`,
    },
    displayColors: false,
  },
};

const nutrients = ['protein', 'fat', 'carbs', 'calories'];
const nutrientLabels = nutrients.map(nutrient =>
  nutrient.replace(nutrient[0], nutrient[0].toUpperCase()),
);

const daysMap = {
  1: 'MONDAY',
  2: 'TUESDAY',
  3: 'WEDNESDAY',
  4: 'THURSDAY',
  5: 'FRIDAY',
  6: 'SATURDAY',
  7: 'SUNDAY',
};

const colSize = 4;

function chartOptions({ textTitle = '', displayYAxes = false }) {
  const options = cloneDeep(chartDefaultOptions);
  options.title.text = textTitle;
  if (!displayYAxes) {
    options.scales.yAxes[0].ticks.callback = () => undefined;
  }

  return options;
}

function nutrientPercentages(weekDay) {
  return nutrients.map(nutrient => weekDay[nutrient]);
}

/**
 * Budget percentage per nutrient returning one of the following:
 * - 0 (don't display bar): nutrient percentage is greater than 1
 * - 1 (display bar): nutrient percentage is less than or equal to 1
 */
function nutrientBudgetPercentage(percentages) {
  return percentages.map(nutrientPercentage => (nutrientPercentage > 1 ? 0 : 1));
}

function budgetDataset(percentages) {
  return {
    label: 'Budget',
    data: nutrientBudgetPercentage(percentages),
    originalData: percentages,
  };
}

function nutrientNormalPercentage(percentages) {
  const maxNutrientPercentage = Math.max(...percentages);
  return percentages.map(nutrientPercentage => (
    nutrientPercentage > 1 ? 1 / maxNutrientPercentage : nutrientPercentage
  ));
}

function normalDataset(percentages) {
  return {
    label: 'Normal',
    backgroundColor: theme.colors.nutritionBar,
    data: nutrientNormalPercentage(percentages),
    originalData: percentages,
  };
}

function nutrientExceededPercentage(percentages) {
  const maxNutrientPercentage = Math.max(...percentages);
  return percentages.map(nutrientPercentage => (
    nutrientPercentage > 1 ? nutrientPercentage / maxNutrientPercentage : 0
  ));
}

function exceededDataset(percentages) {
  return {
    label: 'Exceeded',
    backgroundColor: theme.colors.nutritionBarExceeded,
    data: nutrientExceededPercentage(percentages),
    originalData: percentages,
  };
}

function chartData(weekDay) {
  const percentages = nutrientPercentages(weekDay);
  return {
    labels: nutrientLabels,
    datasets: [
      budgetDataset(percentages),
      normalDataset(percentages),
      exceededDataset(percentages),
    ],
  };
}

function computedWeekDays(weekStats) {
  const { weekDays, nutritionalBudget } = weekStats;
  const presentDays = keyBy(weekDays, 'weekDay');
  const computedDays = [];

  Object.keys(daysMap).forEach((day) => {
    if (presentDays[day]) {
      computedDays.push({
        ...presentDays[day],
        protein: presentDays[day].protein / nutritionalBudget.protein,
        fat: presentDays[day].fat / nutritionalBudget.fat,
        carbs: presentDays[day].carbs / nutritionalBudget.carbs,
        calories: presentDays[day].calories / nutritionalBudget.calories,
      });
    } else {
      computedDays.push({
        weekDay: day,
        protein: 0,
        fat: 0,
        carbs: 0,
        calories: 0,
      });
    }
  });

  return computedDays;
}

function Week({ weekStats, reloadWeekStats, classes }) {
  return (
    <div>
      <RefreshStats createdAt={weekStats.createdAt} refresh={reloadWeekStats} />
      <GridList className={classes.gridList} cols={colSize} cellHeight={200}>
        {computedWeekDays(weekStats).map(weekDay => (
          <GridTile key={weekDay.weekDay}>
            <HorizontalBar
              data={chartData(weekDay)}
              options={chartOptions({
                textTitle: daysMap[weekDay.weekDay],
                displayYAxes: weekDay.weekDay % colSize === 1,
              })}
            />
          </GridTile>
        ))}
      </GridList>
    </div>
  );
}

Week.propTypes = {
  reloadWeekStats: PropTypes.func.isRequired,
  weekStats: PropTypes.shape({
    createdAt: PropTypes.string.isRequired,
    customerId: PropTypes.string.isRequired,
    weekDays: PropTypes.arrayOf(
      PropTypes.shape({
        calories: PropTypes.number.isRequired,
        carbs: PropTypes.number.isRequired,
        count: PropTypes.number.isRequired,
        fat: PropTypes.number.isRequired,
        protein: PropTypes.number.isRequired,
        weekDay: PropTypes.number.isRequired,
      }),
    ).isRequired,
    nutritionalBudget: PropTypes.shape({
      calories: PropTypes.number.isRequired,
      carbs: PropTypes.number.isRequired,
      fat: PropTypes.number.isRequired,
      protein: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
  classes: PropTypes.shape({
    gridList: PropTypes.string.isRequired,
  }).isRequired,
};

export default injectSheet(styles)(Week);
