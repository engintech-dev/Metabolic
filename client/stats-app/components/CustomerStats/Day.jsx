import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
  TableHeaderColumn,
  TableHeader,
} from 'material-ui/Table';
import injectSheet from 'react-jss';
import zipObject from 'lodash/zipObject';
import map from 'lodash/map';
import RefreshStats from '../RefreshStats';
import DayChart from './DayChart';

const styles = {
  tableRow: {
    whiteSpace: 'normal',
  },
};

const nutrientColors = {
  carbs: '#F77F00',
  fat: '#D62828',
  protein: '#003049',
  calories: '#00cc00',
};

const cumulativeNutrientColors = {
  cumulativeCarbs: nutrientColors.carbs,
  cumulativeFat: nutrientColors.fat,
  cumulativeProtein: nutrientColors.protein,
  cumulativeCalories: nutrientColors.calories,
};

function nutrientsDataFor(nutrients, dayStats) {
  return zipObject(nutrients, map(nutrients, nutrient => map(dayStats.data, nutrient)));
}

function Day(props) {
  const nutrients = ['fat', 'carbs', 'protein', 'calories'];
  const cumulativeNutrients = ['cumulativeFat', 'cumulativeCarbs', 'cumulativeProtein', 'cumulativeCalories'];
  const hours = map(props.dayStats.data, 'hour');
  return (
    <div>
      <RefreshStats createdAt={props.dayStats.createdAt} refresh={props.reloadDayStats} />
      <DayChart
        nutrients={nutrients}
        nutrientsData={nutrientsDataFor(nutrients, props.dayStats)}
        hours={hours}
        colors={nutrientColors}
      />
      <DayChart
        nutrients={cumulativeNutrients}
        nutrientsData={nutrientsDataFor(cumulativeNutrients, props.dayStats)}
        colors={cumulativeNutrientColors}
        hours={hours}
        fill
      />
      <Table selectable={false}>
        <TableHeader displaySelectAll={false}>
          <TableRow>
            <TableHeaderColumn># of meals</TableHeaderColumn>
            <TableHeaderColumn>% carbs</TableHeaderColumn>
            <TableHeaderColumn>cumulative</TableHeaderColumn>
            <TableHeaderColumn>% protein</TableHeaderColumn>
            <TableHeaderColumn>cumulative</TableHeaderColumn>
            <TableHeaderColumn>% fat</TableHeaderColumn>
            <TableHeaderColumn>cumulative</TableHeaderColumn>
            <TableHeaderColumn>% calories</TableHeaderColumn>
            <TableHeaderColumn>cumulative</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false} showRowHover stripedRows>
          {props.dayStats.data.map(hourData => (
            <TableRow key={hourData.hour}>
              <TableHeaderColumn>{`${hourData.hour}:00`}</TableHeaderColumn>
              <TableRowColumn style={styles.tableRow}>{hourData.count}</TableRowColumn>
              <TableRowColumn style={styles.tableRow}>{hourData.carbs}</TableRowColumn>
              <TableRowColumn style={styles.tableRow}>{hourData.cumulativeCarbs}</TableRowColumn>
              <TableRowColumn style={styles.tableRow}>{hourData.protein}</TableRowColumn>
              <TableRowColumn style={styles.tableRow}>
                {hourData.cumulativeProtein}
              </TableRowColumn>
              <TableRowColumn style={styles.tableRow}>{hourData.fat}</TableRowColumn>
              <TableRowColumn style={styles.tableRow}>{hourData.cumulativeFat}</TableRowColumn>
              <TableRowColumn style={styles.tableRow}>{hourData.calories}</TableRowColumn>
              <TableRowColumn style={styles.tableRow}>
                {hourData.cumulativeCalories}
              </TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
}

Day.propTypes = {
  reloadDayStats: PropTypes.func.isRequired,
  dayStats: PropTypes.shape({
    createdAt: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(
      PropTypes.shape({
        hour: PropTypes.number.isRequired,
        fat: PropTypes.number.isRequired,
        protein: PropTypes.number.isRequired,
        carbs: PropTypes.number.isRequired,
        calories: PropTypes.number.isRequired,
        cumulativeCalories: PropTypes.number.isRequired,
        cumulativeFat: PropTypes.number.isRequired,
        cumulativeProtein: PropTypes.number.isRequired,
        cumulativeCarbs: PropTypes.number.isRequired,
      }),
    ).isRequired,
  }).isRequired,
};

export default injectSheet(styles)(Day);
