/* global Stripe */
/* global Mtblc */
import PropTypes from 'prop-types';
import EventEmitter from 'eventemitter3';

export default class StripeContext extends EventEmitter {
  constructor() {
    super();
    this.stripe = Stripe(Mtblc.config.STRIPE_PUBLIC_KEY);
    // TODO: find better way to load Google fonts inside stripe's iframe
    this.elements = this.stripe.elements({
      locale: 'en',
      fonts: [{
        family: 'Noto Sans',
        src: 'url(https://fonts.gstatic.com/s/notosans/v6/LeFlHvsZjXu2c3ZRgBq9nJBw1xU1rKptJj_0jans920.woff2)',
      }],
    });
  }

  getCardElement() {
    if (!this.cardElement) {
      this.cardElement = this.elements.create('card');
      this.cardElement.on('change', event => this.emit('change', event));
      this.cardElement.on('focus', () => this.emit('focus'));
      this.cardElement.on('blur', () => this.emit('blur'));
    }
    return this.cardElement;
  }

  createToken() {
    this.emit('error', undefined);
    return this.stripe.createToken(this.getCardElement())
      .then((result) => {
        this.emit('error', result.error);
        if (result.error) {
          throw new Error(result.error);
        }
        return result;
      });
  }
}

export const stripeContextShape = PropTypes.shape({
  getCardElement: PropTypes.func.isRequired,
  createToken: PropTypes.func.isRequired,
  on: PropTypes.func.isRequired,
});
