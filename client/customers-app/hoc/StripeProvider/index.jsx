import React, { Component } from 'react';
import StripeContext, { stripeContextShape } from './stripe-context';

export default function StripeProvider(Wrapped) {
  class StripeProviderEnhancer extends Component {
    getChildContext() {
      return { stripeContext: this.stripeContext };
    }

    componentWillMount() {
      this.stripeContext = new StripeContext();
    }

    render() {
      return <Wrapped {...this.props} stripeContext={this.stripeContext} />;
    }
  }

  StripeProviderEnhancer.displayName = `StripeProvider(${Wrapped.displayName})`;

  StripeProviderEnhancer.childContextTypes = {
    stripeContext: stripeContextShape,
  };

  return StripeProviderEnhancer;
}
