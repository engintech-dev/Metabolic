import React from 'react';
import { Route } from 'react-router';
import AcceptFriend from './containers/AcceptFriend';
import ChangePassword from './containers/ChangePassword';
import Dashboard from './containers/Dashboard';
import EmailVerified from './components/EmailVerified';
import EnsureSignedIn from '../common/containers/EnsureSignedIn';
import Home from './containers/Home';
import PublicLayout from '../common/containers/PublicLayout';
import ResetPassword from './containers/ResetPassword';
import ResetPasswordRequest from './containers/ResetPasswordRequest';
import SignInUp from './containers/SignInUp';
import Welcome from './containers/Welcome';
import VerifyEmailRequest from './containers/VerifyEmailRequest';

export default [
  <Route>
    <Route path="/" component={Home} />
    <Route component={PublicLayout}>
      <Route path="sign-up" component={SignInUp} />
      <Route path="sign-in" component={SignInUp} />
      <Route path="verified" component={EmailVerified} />
      <Route path="welcome" component={Welcome} />
      <Route path="reset-password/:accessToken" component={ResetPassword} />
      <Route path="reset-password-request" component={ResetPasswordRequest} />
      <Route path="verify-email-request" component={VerifyEmailRequest} />
      <Route component={EnsureSignedIn}>
        <Route path="dashboard" component={Dashboard} />
        <Route path="change-password" component={ChangePassword} />
        <Route path="invite/:inviteToken/accept" component={AcceptFriend} />
      </Route>
    </Route>
  </Route>,
];
