import React from 'react';
import { Link } from 'react-router';
import DownloadMobileApp from './DownloadMobileApp';

export default function EmailVerified() {
  return (
    <div>
      <h1>Your e-mail has been verified</h1>
      <p>
        You can now sign in with your account in
        our <Link to="sign-in">web application</Link> or our mobile apps
      </p>
      <DownloadMobileApp />
    </div>
  );
}
