import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  TextField,
} from 'redux-form-material-ui';
import Notification from '../../common/components/Notification';
import FormActions from '../../common/components/FormActions';
import SubmitButton from '../../common/components/SubmitButton';

export default function ResetPassword(props) {
  const { handleSubmit, submitting, error } = props;
  let errorMessage;
  if (error) {
    errorMessage = error.getHumanMessage();
  }
  return (
    <div>
      <Notification type="error" message={errorMessage} />
      <h1>Request password reset</h1>
      <p>Enter your Metabolic account&apos;s e-mail and we&apos;ll send you
        a delicious reset password link</p>
      <form onSubmit={handleSubmit}>
        <div>
          <Field
            name="email" component={TextField}
            hintText="your.email@example.org" floatingLabelText="Your e-mail"
            disabled={submitting} validate={value => (value ? undefined : 'enter something!')}
          />
        </div>
        <FormActions>
          <SubmitButton
            label="I want to reset my password" submitting={submitting} disabled={submitting}
          />
        </FormActions>
      </form>
    </div>
  );
}

ResetPassword.propTypes = {
  error: PropTypes.shape({ getHumanMessage: PropTypes.func.isRequired }),
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

ResetPassword.defaultProps = {
  error: undefined,
};
