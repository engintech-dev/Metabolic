import React from 'react';
import PropTypes from 'prop-types';
import FlatButton from 'material-ui/FlatButton';
import DownloadMobileApp from './DownloadMobileApp';

export default function Dashboard(props) {
  return (<div>
    <p>Soon you&apos;ll find some useful things here.</p>
    <p>In the meantime you can use our mobile app</p>
    <DownloadMobileApp />
    <p>
      <FlatButton label="Sign out" primary onClick={props.onSignOut} />
    </p>
  </div>);
}

Dashboard.propTypes = {
  onSignOut: PropTypes.func.isRequired,
};
