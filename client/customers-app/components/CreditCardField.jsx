import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import classNames from 'classnames';
import theme, { errorStyle, defaultTransition } from '../../common/theme';

const width = '256px';
const styles = {
  container: {
    width,
    height: '72px',
    margin: '0 auto',
    display: 'inline-block',
    position: 'relative',
  },
  fieldContainer: {
    marginTop: '14px',
    height: '100%',
    '& iframe': {
      height: '100% !important',
    },
  },
  field: {
    height: '100%',
    fontSize: '16px',
    lineHeight: '24px',
    margin: '0 auto',
    fontFamily: theme.fontFamily,
    fontWeight: 'normal',
    color: 'red',
    '&.is-empty:not(.is-focused)': {
      opacity: 0,
    },
  },
  errorMessage: errorStyle,
  label: {
    color: theme.textField.floatingLabelColor,
    position: 'absolute',
    lineHeight: '22px',
    top: '38px',
    left: 0,
    transition: defaultTransition,
    zIndex: 1,
    transformOrigin: 'left top 0px',
    pointerEvents: 'none',
    userSelect: 'none',
    transform: 'scale(1) translate(0px, 0px)',
    '.is-focused &, :not(.is-empty) > div > &': {
      transform: 'scale(0.75) translate(0px, -28px)',
    },
    '.is-focused &': {
      color: theme.textField.focusColor,
    },
    '.is-invalid &': {
      color: theme.textField.errorColor,
    },
  },
  underline: {
    position: 'absolute',
    bottom: '8px',
    width: '100%',
    margin: 0,
    border: 'none',
    borderBottom: `1px solid ${theme.textField.borderColor}`,
    '&.togglable': {
      transform: 'scaleX(0)',
      transition: defaultTransition,
      '.is-invalid &, .is-focused &': {
        borderBottomColor: theme.textField.focusColor,
        borderBottomWidth: '2px',
        transform: 'scaleX(1)',
      },
      '.is-invalid &': {
        borderBottomColor: theme.textField.errorColor,
      },
    },
  },
};

export const cardElementConfig = {
  iconStyle: 'solid',
  style: {
    base: {
      color: theme.textField.textColor,
      fontFamily: theme.fontFamily,
      fontSize: '15px',
      lineHeight: '72px',
      iconColor: theme.textField.disabledTextColor,
      '::placeholder': {
        color: theme.textField.disabledTextColor,
      },
    },
    invalid: {
      color: theme.textField.errorColor,
    },
  },
  classes: {
    focus: 'is-focused',
    empty: 'is-empty',
  },
};

function CreditCardField(props) {
  const errorMessage = props.error
    ? (<span className={props.classes.errorMessage}>{props.error.message}</span>)
    : null;
  const hasError = !!errorMessage;
  return (
    <div>
      <div
        className={classNames({
          [props.classes.container]: true,
          'is-invalid': hasError,
          'is-focused': props.focused,
          'is-empty': props.empty,
        })}
      >
        <div className={props.classes.fieldContainer}>
          <label htmlFor={props.id} className={props.classes.label}>Credit Card</label>
          <div id={props.id} ref={props.onRef} className={props.classes.field} style={{ color: 'green' }} />
        </div>
        <hr className={props.classes.underline} />
        <hr className={classNames([props.classes.underline, 'togglable'])} />
        {errorMessage}
      </div>
    </div>
  );
}

CreditCardField.propTypes = {
  classes: PropTypes.shape({
    container: PropTypes.string.isRequired,
    errorMessage: PropTypes.string.isRequired,
    fieldContainer: PropTypes.string.isRequired,
    field: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    underline: PropTypes.string.isRequired,
  }).isRequired,
  empty: PropTypes.bool,
  error: PropTypes.shape({ message: PropTypes.string }),
  focused: PropTypes.bool,
  id: PropTypes.string.isRequired,
  onRef: PropTypes.func.isRequired,
};

CreditCardField.defaultProps = {
  error: undefined,
  empty: true,
  focused: false,
};

export default injectSheet(styles)(CreditCardField);
