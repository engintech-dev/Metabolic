import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Field } from 'redux-form';
import injectSheet from 'react-jss';
import {
  TextField,
} from 'redux-form-material-ui';
import Divider from 'material-ui/Divider';
import CreditCardField from '../containers/CreditCardField';
import Notification from '../../common/components/Notification';
import FormActions from '../../common/components/FormActions';
import SubmitButton from '../../common/components/SubmitButton';
import { required, email, repeatPassword } from '../../common/forms/validators';

const styles = {
  signInUp: {
    '& h2': {
      marginBottom: 0,
    },
  },
};

function alternative(signIn, reset) {
  if (signIn) {
    return (
      <div>Or <Link to="/sign-up" onClick={reset}>create a new account</Link></div>
    );
  }
  return (
    <div>Or <Link to="/sign-in" onClick={reset}>sign in with your existing account</Link></div>
  );
}

function SignInUp(props) {
  const { handleSubmit, submitting, reset, signIn } = props;
  const errorMessage = props.error && props.error.getHumanMessage();
  return (
    <div className={props.classes.signInUp}>
      <Notification type="error" message={errorMessage} />
      <h1>{signIn ? 'Enter your account' : 'Subscribe to Metabolic'}</h1>
      <form onSubmit={handleSubmit}>
        {signIn ? null : <h2>Account</h2>}
        {signIn ? null : <div>
          <Field
            name="name" component={TextField}
            hintText="John Doe" floatingLabelText="Your name"
            disabled={submitting} validate={required}
          />
        </div>}
        <div>
          <Field
            name="email" component={TextField}
            hintText="your.email@example.org" floatingLabelText="Your e-mail"
            disabled={submitting} validate={[required, email]}
          />
        </div>
        <div>
          <Field
            name="password" component={TextField}
            type="password" hintText="******" floatingLabelText="Your password"
            disabled={submitting} validate={required}
          />
        </div>
        {signIn ? null : <div>
          <Field
            name="repeatPassword" component={TextField}
            type="password" hintText="******" floatingLabelText="Repeat password"
            disabled={submitting} validate={repeatPassword}
          />
        </div>}
        {signIn ? null : <div>
          <h2>The details</h2>
          <p>Metabolic is $100 per month.</p>
          <p>
            Before the end of the first month, we’ll give you a heads up.
            You can decide whether it’s awesome and you want to continue or you’d prefer a refund.
          </p>
          <CreditCardField error={props.error} />
          {/* <Field
            name="coupon" component={TextField}
            floatingLabelText="Partner code (optional)?"
            disabled={submitting}
          /> */}
        </div>}
        <FormActions>
          <SubmitButton
            label={signIn ? 'Sign In' : 'Sign Up'}
            submitting={submitting} disabled={submitting}
          />
        </FormActions>
      </form>
      {signIn
        ? (
          <div>
            <div style={{ marginTop: '20px' }}><Link to="/reset-password-request">Forgot password?</Link></div>
            <div style={{ marginTop: '20px' }}><Link to="/verify-email-request">Resend verification e-mail</Link></div>
          </div>
        )
        : undefined}
      <Divider inset={false} style={{ margin: '30px 0', marginTop: '30px' }} />
      {alternative(signIn, reset)}
    </div>
  );
}

SignInUp.propTypes = {
  classes: PropTypes.shape({ signInUp: PropTypes.string.isRequired }),
  error: PropTypes.shape({ getHumanMessage: PropTypes.func.isRequired }),
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  signIn: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

SignInUp.defaultProps = {
  error: undefined,
};

export default injectSheet(styles)(SignInUp);
