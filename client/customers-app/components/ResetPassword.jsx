import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Field } from 'redux-form';
import {
  TextField,
} from 'redux-form-material-ui';
import Notification from '../../common/components/Notification';
import FormActions from '../../common/components/FormActions';
import SubmitButton from '../../common/components/SubmitButton';
import { required, repeatPassword } from '../../common/forms/validators';

export default function ResetPassword(props) {
  const { handleSubmit, submitting, error } = props;
  let errorMessage;
  let tokenExpired;
  if (error) {
    tokenExpired = error.tokenExpired;
    errorMessage = tokenExpired
      ? (<div>
        This reset password link already
        expired. <Link to="/sign-in"> Go to sign in page</Link> to ask for a new one
      </div>)
      : error.getHumanMessage();
  }
  return (
    <div>
      <Notification type="error" message={errorMessage} />
      <h1>Enter your new password</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <Field
            name="password" component={TextField}
            type="password" hintText="******" floatingLabelText="Your password"
            disabled={submitting || tokenExpired} validate={required}
          />
        </div>
        <div>
          <Field
            name="repeatPassword" component={TextField}
            type="password" hintText="******" floatingLabelText="Repeat password"
            disabled={submitting || tokenExpired} validate={repeatPassword}
          />
        </div>
        <FormActions>
          <SubmitButton
            label="Save new password" submitting={submitting} disabled={submitting}
          />
        </FormActions>
      </form>
    </div>
  );
}

ResetPassword.propTypes = {
  error: PropTypes.shape({ getHumanMessage: PropTypes.func.isRequired }),
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

ResetPassword.defaultProps = {
  error: undefined,
};
