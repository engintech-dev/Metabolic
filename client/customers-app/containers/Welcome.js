import { connect } from 'react-redux';
import { goToRoot } from '../store/actions/routes';
import Welcome from '../components/Welcome';
import { signedUpUserName } from '../store/selectors/users';

const mapStateToProps = state => ({
  name: signedUpUserName(state),
});

const mapDispatchToProps = dispatch => ({
  handleNoName: () => dispatch(goToRoot()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
