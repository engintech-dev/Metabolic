import React, { Component } from 'react';
import PropTypes from 'prop-types';
import pick from 'lodash/pick';
import CreditCardField, { cardElementConfig } from '../components/CreditCardField';
import { stripeContextShape } from '../hoc/StripeProvider/stripe-context';

const generateId = (() => {
  const random = Math.round(Math.random() * 100000000000);
  let index = 1;
  return () => {
    index += 1;
    return `card-id-${random}-${index}`;
  };
})();

export default class CreditCardFieldContainer extends Component {
  constructor(props) {
    super(props);
    this.mountStripeElement = this.mountStripeElement.bind(this);
    this.state = {
      error: undefined,
      lastErrorFromPropsShown: undefined,
      empty: true,
      id: generateId(),
    };
  }

  componentWillMount() {
    this.onStripeError = error => this.setState({ error });
    this.onChange = event => this.setState(pick(event, 'error', 'empty'));
    this.onFocus = () => this.setState({ focused: true });
    this.onBlur = () => this.setState({ focused: false });
    this.context.stripeContext.on('error', this.onStripeError);
    this.context.stripeContext.on('change', this.onChange);
    this.context.stripeContext.on('focus', this.onFocus);
    this.context.stripeContext.on('blur', this.onBlur);
  }

  componentWillReceiveProps(nextProps) {
    const { error } = nextProps;
    if (error && error.code === 'StripeCardError') {
      const stripeErrorDetails = error.details;
      if (stripeErrorDetails !== this.state.lastErrorFromProps) {
        this.onStripeError(stripeErrorDetails);
        this.setState({ lastErrorFromProps: stripeErrorDetails });
      }
    }
  }

  componentWillUnmount() {
    if (this.onStripeError) {
      this.context.stripeContext.removeListener('error', this.onStripeError);
      this.context.stripeContext.removeListener('change', this.onChange);
      this.context.stripeContext.removeListener('focus', this.onFocus);
      this.context.stripeContext.removeListener('blur', this.onBlur);
    }
  }

  mountStripeElement(element) {
    const cardElement = this.context.stripeContext.getCardElement();
    if (element) {
      cardElement.update(cardElementConfig);
      cardElement.mount(element);
    } else {
      cardElement.unmount();
    }
  }

  render() {
    return (<CreditCardField
      id={this.state.id}
      onRef={this.mountStripeElement}
      empty={this.state.empty}
      error={this.state.error}
      focused={this.state.focused}
    />);
  }
}

CreditCardFieldContainer.propTypes = {
  error: PropTypes.shape({ code: PropTypes.string, details: PropTypes.object }),
};

CreditCardFieldContainer.defaultProps = {
  error: undefined,
};

CreditCardFieldContainer.contextTypes = {
  stripeContext: stripeContextShape,
};
