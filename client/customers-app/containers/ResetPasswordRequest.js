import { reduxForm } from 'redux-form';
import { routes, notification } from '../store/actions';
import apiErrorsTranslator from '../../common/forms/api-errors';
import ResetPasswordRequest from '../components/ResetPasswordRequest';
import apiService from '../services/api';

export default reduxForm({
  form: 'resetPassword',
  enableReinitialize: true,
  onSubmit: (values, dispatch) => apiService.users.requestPasswordReset(values.email)
    .then(() => dispatch(notification.showNotification({
      message: 'Check your e-mail for further instructions',
      duration: 7000,
    })))
    .then(() => dispatch(routes.goToSignIn()))
    .catch(apiErrorsTranslator),
})(ResetPasswordRequest);
