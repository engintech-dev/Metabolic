import Immutable from 'immutable';
import ACTION_TYPES from '../actions/types';

const API_ACTION_TYPES = ACTION_TYPES.API;
const { Map } = Immutable;

const INITIAL_STATE = Map({
  signUpError: null,
});

export default function userReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case API_ACTION_TYPES.SIGNUP.START:
      return new Map(INITIAL_STATE);
    case API_ACTION_TYPES.SIGNUP.SUCCESS:
      return state.merge({ signedUpUser: action.payload });
    case API_ACTION_TYPES.SIGNUP.FAILURE:
      return state.set('signUpError', action.error);
    default:
      return state;
  }
}
