import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import ui from './ui';
import session from './session';
import users from './users';
import ACTION_TYPES from '../actions/types';

const SIGNOUT_ACTIONS = ACTION_TYPES.API.SIGNOUT;

const appReducer = combineReducers({
  ui,
  session,
  users,
  form: formReducer,
  routing: routerReducer,
});

export default function rootReducer(state, action) {
  if (action.type === SIGNOUT_ACTIONS.SUCCESS || action.type === SIGNOUT_ACTIONS.FAILURE) {
    // eslint-disable-next-line no-param-reassign
    state = undefined;
  }

  return appReducer(state, action);
}
