import { push } from 'react-router-redux';
import apiService from '../../services/api';
import storageService from '../../../common/services/storage';

import {
  startSignIn,
  successSignIn,
  failureSignIn,
  startSignOut,
  successSignOut,
  failureSignOut,
} from '../../../common/store/actions/session';

export function signIn(credentials) {
  return (dispatch, getState) => {
    dispatch(startSignIn());
    return apiService.users.signIn(credentials)
      .then((payload) => {
        dispatch(successSignIn(payload));
        const { session } = getState();
        storageService.set('redux-store', { session });
      })
      .catch((error) => {
        dispatch(failureSignIn(error));
        throw error;
      });
  };
}

export function signOut() {
  return (dispatch) => {
    dispatch(startSignOut());
    return apiService.users.signOut()
      .catch((error) => {
        if (error.statusCode === 401) {
          return undefined;
        }
        throw error;
      })
      .then((payload) => {
        dispatch(successSignOut(payload));
        storageService.set('redux-store', { session: null });
        dispatch(push('/'));
      })
      .catch(error => dispatch(failureSignOut(error)));
  };
}

export default {
  signIn,
  signOut,
};
