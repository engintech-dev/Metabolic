<h1 align="center">Metabolic API</h1>

<p align="center">
master [![Codefresh build status]( https://g.codefresh.io/api/badges/build?repoOwner=metabolicinc&repoName=webapp&branch=master&pipelineName=webapp&accountName=raul_mtblc&key=eyJhbGciOiJIUzI1NiJ9.NThlMmNkNjk3ODE1OTMwMTAwYTUyM2Rj.JiYK7EkD9poGEqRIAa9p8_YKKp-W1fdc-tC4MYQ8NWo&type=cf-1)]( https://g.codefresh.io/repositories/metabolicinc/webapp/builds?filter=trigger:build;branch:master;service:58e2cee1cfa79b0100eb5421~webapp)

develop [![Codefresh build status]( https://g.codefresh.io/api/badges/build?repoOwner=metabolicinc&repoName=webapp&branch=develop&pipelineName=webapp&accountName=raul_mtblc&key=eyJhbGciOiJIUzI1NiJ9.NThlMmNkNjk3ODE1OTMwMTAwYTUyM2Rj.JiYK7EkD9poGEqRIAa9p8_YKKp-W1fdc-tC4MYQ8NWo&type=cf-1)]( https://g.codefresh.io/repositories/metabolicinc/webapp/builds?filter=trigger:build;branch:develop;service:58e2cee1cfa79b0100eb5421~webapp)
</p>

## REST API

The REST API can be found [here](REST_API.md).

## Streaming API

The Streaming API can be found [here](Streaming_API.md).
